(defproject advent-of-code "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [org.clojure/math.combinatorics "0.1.6"]
                 [org.clojure/test.check "1.1.1"]
                 [com.gfredericks/test.chuck "0.2.13"]
                 [instaparse "1.4.12"]
                 [com.rpl/specter "1.1.3"]
                 [pandect "1.0.2"]
                 [net.cgrand/xforms "0.19.4"]
                 [clj-fuzzy "0.4.1"]
                 [com.clojure-goes-fast/clj-java-decompiler "0.3.2"]
                 [parallel "0.10"]
                 [net.cgrand/seqexp "0.6.2"]
                 [prismatic/plumbing "0.6.0"]
                 [com.taoensso/tufte "2.4.5"]
                 [metosin/malli "0.9.2"]
                 [ubergraph "0.8.2"]
                 [criterium "0.4.6"]
                 [org.clojure/core.match "1.0.0"]
                 [net.mikera/core.matrix "0.62.0"]
                 [org.clojure/tools.trace "0.7.11"]]
  :plugins [[org.clojars.benfb/lein-gorilla "0.7.0"]])
