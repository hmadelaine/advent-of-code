(ns advent-of-code-2016.day-7)

(re-find #"([a-z])([a-z])\2\1" "abba")


(re-find #"(.)(.)\2\1" "abba")
(re-find #"\[(.)(.)\2\1\]|((.)(.)\5\4)" "[abba]")

(re-matches #"'Tarzan'|(Tarzan)" "Tarzan")
