(ns advent-of-code-2018.day-10
  (:require [clojure.string :as str]
            [com.rpl.specter :refer :all]
            [cartesian.utils :refer :all]
            [clojure.pprint :refer :all]))


(def raw-example "position=< 9,  1> velocity=< 0,  2>\nposition=< 7,  0> velocity=<-1,  0>\nposition=< 3, -2> velocity=<-1,  1>\nposition=< 6, 10> velocity=<-2, -1>\nposition=< 2, -4> velocity=< 2,  2>\nposition=<-6, 10> velocity=< 2, -2>\nposition=< 1,  8> velocity=< 1, -1>\nposition=< 1,  7> velocity=< 1,  0>\nposition=<-3, 11> velocity=< 1, -2>\nposition=< 7,  6> velocity=<-1, -1>\nposition=<-2,  3> velocity=< 1,  0>\nposition=<-4,  3> velocity=< 2,  0>\nposition=<10, -3> velocity=<-1,  1>\nposition=< 5, 11> velocity=< 1, -2>\nposition=< 4,  7> velocity=< 0, -1>\nposition=< 8, -2> velocity=< 0,  1>\nposition=<15,  0> velocity=<-2,  0>\nposition=< 1,  6> velocity=< 1,  0>\nposition=< 8,  9> velocity=< 0, -1>\nposition=< 3,  3> velocity=<-1,  1>\nposition=< 0,  5> velocity=< 0, -1>\nposition=<-2,  2> velocity=< 2,  0>\nposition=< 5, -2> velocity=< 1,  2>\nposition=< 1,  4> velocity=< 2,  1>\nposition=<-2,  7> velocity=< 2, -2>\nposition=< 3,  6> velocity=<-1, -1>\nposition=< 5,  0> velocity=< 1,  0>\nposition=<-6,  0> velocity=< 2,  0>\nposition=< 5,  9> velocity=< 1, -2>\nposition=<14,  7> velocity=<-2,  0>\nposition=<-3,  6> velocity=< 2, -1>")

(def raw-input "position=<-41150,  41504> velocity=< 4, -4>\nposition=< 31211, -10213> velocity=<-3,  1>\nposition=<-51522, -41248> velocity=< 5,  4>\nposition=< 31227, -51593> velocity=<-3,  5>\nposition=< 31257, -20560> velocity=<-3,  2>\nposition=< 41558,  10468> velocity=<-4, -1>\nposition=< 10539, -30904> velocity=<-1,  3>\nposition=< 51919, -41253> velocity=<-5,  4>\nposition=< 31246,  10473> velocity=<-3, -1>\nposition=<-20471,  20813> velocity=< 2, -2>\nposition=< 51910, -10222> velocity=<-5,  1>\nposition=<-20503,  20816> velocity=< 2, -2>\nposition=< 20901, -41255> velocity=<-2,  4>\nposition=< 31249, -41249> velocity=<-3,  4>\nposition=<-10139,  10477> velocity=< 1, -1>\nposition=< 20888, -30903> velocity=<-2,  3>\nposition=< 31218,  31158> velocity=<-3, -3>\nposition=<-30802,  51848> velocity=< 3, -5>\nposition=< 51923,  51854> velocity=<-5, -5>\nposition=<-51503,  41504> velocity=< 5, -4>\nposition=<-10131,  31165> velocity=< 1, -3>\nposition=<-30809, -10222> velocity=< 3,  1>\nposition=< 10551,  20818> velocity=<-1, -2>\nposition=< 31236,  31158> velocity=<-3, -3>\nposition=<-51534,  20822> velocity=< 5, -2>\nposition=< 20920, -41250> velocity=<-2,  4>\nposition=<-30841,  41504> velocity=< 3, -4>\nposition=< 51932, -41256> velocity=<-5,  4>\nposition=< 31249,  10469> velocity=<-3, -1>\nposition=<-41198,  41505> velocity=< 4, -4>\nposition=<-30833,  41510> velocity=< 3, -4>\nposition=<-51494,  41507> velocity=< 5, -4>\nposition=< 10553,  10472> velocity=<-1, -1>\nposition=<-41174, -20564> velocity=< 4,  2>\nposition=< 41578,  20816> velocity=<-4, -2>\nposition=<-51503, -10214> velocity=< 5,  1>\nposition=<-30824, -30906> velocity=< 3,  3>\nposition=<-41172,  20816> velocity=< 4, -2>\nposition=<-41198,  41507> velocity=< 4, -4>\nposition=< 41578, -30911> velocity=<-4,  3>\nposition=< 20904,  20817> velocity=<-2, -2>\nposition=< 51901,  41503> velocity=<-5, -4>\nposition=< 20872, -30906> velocity=<-2,  3>\nposition=< 31265, -20560> velocity=<-3,  2>\nposition=<-10136, -10217> velocity=< 1,  1>\nposition=< 31246,  20817> velocity=<-3, -2>\nposition=< 51927,  20820> velocity=<-5, -2>\nposition=<-30821, -10222> velocity=< 3,  1>\nposition=<-20492, -10221> velocity=< 2,  1>\nposition=<-20488, -51596> velocity=< 2,  5>\nposition=< 41610, -41249> velocity=<-4,  4>\nposition=< 31257, -30908> velocity=<-3,  3>\nposition=< 41610,  31160> velocity=<-4, -3>\nposition=<-10151,  41503> velocity=< 1, -4>\nposition=<-51527, -10216> velocity=< 5,  1>\nposition=< 20867, -30903> velocity=<-2,  3>\nposition=< 31228,  41503> velocity=<-3, -4>\nposition=<-10160, -41253> velocity=< 1,  4>\nposition=< 41595, -30907> velocity=<-4,  3>\nposition=<-51506,  31158> velocity=< 5, -3>\nposition=<-10142, -10219> velocity=< 1,  1>\nposition=<-10115, -10222> velocity=< 1,  1>\nposition=< 10572, -51601> velocity=<-1,  5>\nposition=< 51899,  20818> velocity=<-5, -2>\nposition=<-20476, -30910> velocity=< 2,  3>\nposition=<-41174,  31162> velocity=< 4, -3>\nposition=< 31246,  41509> velocity=<-3, -4>\nposition=<-30801, -41253> velocity=< 3,  4>\nposition=<-10130,  41508> velocity=< 1, -4>\nposition=<-20473,  10470> velocity=< 2, -1>\nposition=<-41150, -20565> velocity=< 4,  2>\nposition=< 31246,  31165> velocity=<-3, -3>\nposition=< 41610, -41256> velocity=<-4,  4>\nposition=< 41574,  31164> velocity=<-4, -3>\nposition=<-41157, -30903> velocity=< 4,  3>\nposition=< 31241,  10469> velocity=<-3, -1>\nposition=< 31217, -30912> velocity=<-3,  3>\nposition=< 10529,  41503> velocity=<-1, -4>\nposition=< 20901,  51853> velocity=<-2, -5>\nposition=<-51551,  10472> velocity=< 5, -1>\nposition=< 41613,  41503> velocity=<-4, -4>\nposition=<-10134, -41251> velocity=< 1,  4>\nposition=< 41595, -41253> velocity=<-4,  4>\nposition=<-51511,  10477> velocity=< 5, -1>\nposition=<-30837, -10217> velocity=< 3,  1>\nposition=< 51950, -51602> velocity=<-5,  5>\nposition=<-51541,  41503> velocity=< 5, -4>\nposition=< 51907,  51857> velocity=<-5, -5>\nposition=<-41198,  10469> velocity=< 4, -1>\nposition=<-30834,  20817> velocity=< 3, -2>\nposition=< 51947,  31163> velocity=<-5, -3>\nposition=<-30804, -30912> velocity=< 3,  3>\nposition=<-30829, -20560> velocity=< 3,  2>\nposition=< 10547, -30908> velocity=<-1,  3>\nposition=<-51531,  31166> velocity=< 5, -3>\nposition=< 51955,  41510> velocity=<-5, -4>\nposition=<-30861, -30911> velocity=< 3,  3>\nposition=< 20872, -51596> velocity=<-2,  5>\nposition=<-30829,  51854> velocity=< 3, -5>\nposition=<-41150, -10218> velocity=< 4,  1>\nposition=< 31257, -20560> velocity=<-3,  2>\nposition=<-20476,  20816> velocity=< 2, -2>\nposition=<-20468,  10470> velocity=< 2, -1>\nposition=<-51519, -30911> velocity=< 5,  3>\nposition=< 51959,  10468> velocity=<-5, -1>\nposition=< 20888, -51593> velocity=<-2,  5>\nposition=< 51947,  10475> velocity=<-5, -1>\nposition=< 31246,  10468> velocity=<-3, -1>\nposition=<-20516, -41249> velocity=< 2,  4>\nposition=<-51511, -41248> velocity=< 5,  4>\nposition=<-41147,  20817> velocity=< 4, -2>\nposition=<-41150, -51594> velocity=< 4,  5>\nposition=<-10163, -30908> velocity=< 1,  3>\nposition=<-10152, -10213> velocity=< 1,  1>\nposition=<-20460, -30903> velocity=< 2,  3>\nposition=<-41169, -10219> velocity=< 4,  1>\nposition=<-51531, -30907> velocity=< 5,  3>\nposition=<-30812,  41503> velocity=< 3, -4>\nposition=<-41169,  10477> velocity=< 4, -1>\nposition=< 10546,  51848> velocity=<-1, -5>\nposition=<-10139,  20813> velocity=< 1, -2>\nposition=<-30861,  51851> velocity=< 3, -5>\nposition=<-20497, -51593> velocity=< 2,  5>\nposition=< 51949, -20558> velocity=<-5,  2>\nposition=< 41597, -51595> velocity=<-4,  5>\nposition=< 51940,  31163> velocity=<-5, -3>\nposition=< 41589,  10473> velocity=<-4, -1>\nposition=<-51495, -41257> velocity=< 5,  4>\nposition=< 31265, -41254> velocity=<-3,  4>\nposition=< 10527,  10473> velocity=<-1, -1>\nposition=< 51927,  41509> velocity=<-5, -4>\nposition=< 20884,  10471> velocity=<-2, -1>\nposition=<-51493,  41507> velocity=< 5, -4>\nposition=<-41182, -30912> velocity=< 4,  3>\nposition=< 10575,  41509> velocity=<-1, -4>\nposition=< 20884, -20567> velocity=<-2,  2>\nposition=< 10535, -20560> velocity=<-1,  2>\nposition=<-51543,  41512> velocity=< 5, -4>\nposition=<-30825, -10214> velocity=< 3,  1>\nposition=<-51535, -20559> velocity=< 5,  2>\nposition=< 10528, -10218> velocity=<-1,  1>\nposition=<-51538,  41506> velocity=< 5, -4>\nposition=< 20892, -30905> velocity=<-2,  3>\nposition=< 31249, -51596> velocity=<-3,  5>\nposition=< 10539,  20815> velocity=<-1, -2>\nposition=<-20464, -41248> velocity=< 2,  4>\nposition=<-41186, -20567> velocity=< 4,  2>\nposition=<-20492,  20820> velocity=< 2, -2>\nposition=<-30818,  31165> velocity=< 3, -3>\nposition=<-10131, -51596> velocity=< 1,  5>\nposition=< 51919,  20814> velocity=<-5, -2>\nposition=<-41166, -30905> velocity=< 4,  3>\nposition=<-10151,  31163> velocity=< 1, -3>\nposition=< 10522,  10468> velocity=<-1, -1>\nposition=< 51911, -30912> velocity=<-5,  3>\nposition=<-41173,  31160> velocity=< 4, -3>\nposition=< 31260, -41248> velocity=<-3,  4>\nposition=<-10139, -10220> velocity=< 1,  1>\nposition=<-30857, -20558> velocity=< 3,  2>\nposition=< 10531, -10222> velocity=<-1,  1>\nposition=<-30813, -20566> velocity=< 3,  2>\nposition=< 51944, -30903> velocity=<-5,  3>\nposition=<-41198,  20820> velocity=< 4, -2>\nposition=<-41150, -41252> velocity=< 4,  4>\nposition=<-20508,  20814> velocity=< 2, -2>\nposition=< 41554, -41256> velocity=<-4,  4>\nposition=< 31238, -41248> velocity=<-3,  4>\nposition=< 20906, -10219> velocity=<-2,  1>\nposition=< 51912,  41505> velocity=<-5, -4>\nposition=< 20866, -51602> velocity=<-2,  5>\nposition=< 10519,  31164> velocity=<-1, -3>\nposition=<-20479,  51856> velocity=< 2, -5>\nposition=<-30829, -51596> velocity=< 3,  5>\nposition=< 51943, -10221> velocity=<-5,  1>\nposition=< 20917, -41249> velocity=<-2,  4>\nposition=< 20888,  41507> velocity=<-2, -4>\nposition=< 51957,  41507> velocity=<-5, -4>\nposition=<-20499, -10213> velocity=< 2,  1>\nposition=< 51911,  41503> velocity=<-5, -4>\nposition=<-51551, -51595> velocity=< 5,  5>\nposition=<-10139,  51849> velocity=< 1, -5>\nposition=< 41582, -10222> velocity=<-4,  1>\nposition=<-41150,  31159> velocity=< 4, -3>\nposition=< 20888,  41504> velocity=<-2, -4>\nposition=< 10567,  20821> velocity=<-1, -2>\nposition=< 51900, -10222> velocity=<-5,  1>\nposition=<-51503,  20815> velocity=< 5, -2>\nposition=<-20488, -51596> velocity=< 2,  5>\nposition=< 51939, -10221> velocity=<-5,  1>\nposition=< 31270,  10468> velocity=<-3, -1>\nposition=<-51527,  20816> velocity=< 5, -2>\nposition=< 51926,  51853> velocity=<-5, -5>\nposition=<-51543, -20564> velocity=< 5,  2>\nposition=< 20883, -20558> velocity=<-2,  2>\nposition=<-41166,  20816> velocity=< 4, -2>\nposition=< 51936,  41511> velocity=<-5, -4>\nposition=<-10115, -10220> velocity=< 1,  1>\nposition=< 31210, -51593> velocity=<-3,  5>\nposition=<-10115, -41248> velocity=< 1,  4>\nposition=< 31233, -20559> velocity=<-3,  2>\nposition=< 20921, -51598> velocity=<-2,  5>\nposition=<-41185, -30912> velocity=< 4,  3>\nposition=<-51531, -51600> velocity=< 5,  5>\nposition=< 10543, -10220> velocity=<-1,  1>\nposition=< 51924, -30912> velocity=<-5,  3>\nposition=<-30821,  20813> velocity=< 3, -2>\nposition=< 10545, -51602> velocity=<-1,  5>\nposition=<-10126, -41248> velocity=< 1,  4>\nposition=<-30829, -30907> velocity=< 3,  3>\nposition=<-51495,  20817> velocity=< 5, -2>\nposition=< 10548, -41256> velocity=<-1,  4>\nposition=< 41578, -51599> velocity=<-4,  5>\nposition=<-41203, -51602> velocity=< 4,  5>\nposition=< 31253, -10214> velocity=<-3,  1>\nposition=<-20457, -30908> velocity=< 2,  3>\nposition=<-10147,  51848> velocity=< 1, -5>\nposition=<-51502, -20558> velocity=< 5,  2>\nposition=< 41615, -10222> velocity=<-4,  1>\nposition=<-41182, -30906> velocity=< 4,  3>\nposition=< 20912, -10216> velocity=<-2,  1>\nposition=< 20869, -20566> velocity=<-2,  2>\nposition=< 10555,  41510> velocity=<-1, -4>\nposition=<-30835,  10472> velocity=< 3, -1>\nposition=<-20487,  41505> velocity=< 2, -4>\nposition=<-20511, -41249> velocity=< 2,  4>\nposition=< 20913,  20822> velocity=<-2, -2>\nposition=<-30824,  10469> velocity=< 3, -1>\nposition=< 10567,  10472> velocity=<-1, -1>\nposition=< 31238,  31166> velocity=<-3, -3>\nposition=<-41186, -10219> velocity=< 4,  1>\nposition=<-41169, -51602> velocity=< 4,  5>\nposition=< 51947, -20564> velocity=<-5,  2>\nposition=< 41562, -10219> velocity=<-4,  1>\nposition=<-10155,  20821> velocity=< 1, -2>\nposition=< 20884, -51598> velocity=<-2,  5>\nposition=< 20907,  41505> velocity=<-2, -4>\nposition=< 10579,  51848> velocity=<-1, -5>\nposition=< 41586, -30905> velocity=<-4,  3>\nposition=<-30859, -41248> velocity=< 3,  4>\nposition=< 20864,  10475> velocity=<-2, -1>\nposition=< 20876, -30908> velocity=<-2,  3>\nposition=< 10543, -41248> velocity=<-1,  4>\nposition=< 31246, -51598> velocity=<-3,  5>\nposition=<-30833,  41503> velocity=< 3, -4>\nposition=<-20473, -51595> velocity=< 2,  5>\nposition=< 31265, -30904> velocity=<-3,  3>\nposition=<-51493,  31158> velocity=< 5, -3>\nposition=< 51947,  20821> velocity=<-5, -2>\nposition=<-41166, -30912> velocity=< 4,  3>\nposition=<-30813,  41508> velocity=< 3, -4>\nposition=<-41198, -51601> velocity=< 4,  5>\nposition=<-10151,  20819> velocity=< 1, -2>\nposition=< 51907,  51854> velocity=<-5, -5>\nposition=<-10158, -41254> velocity=< 1,  4>\nposition=<-41170, -41249> velocity=< 4,  4>\nposition=< 51907,  51856> velocity=<-5, -5>\nposition=< 41586, -10216> velocity=<-4,  1>\nposition=< 31246, -20560> velocity=<-3,  2>\nposition=<-30826, -10217> velocity=< 3,  1>\nposition=< 10569,  51848> velocity=<-1, -5>\nposition=<-20484, -30912> velocity=< 2,  3>\nposition=< 10559,  41510> velocity=<-1, -4>\nposition=< 41555,  31167> velocity=<-4, -3>\nposition=< 20877, -30911> velocity=<-2,  3>\nposition=<-51549,  20822> velocity=< 5, -2>\nposition=<-51551,  51856> velocity=< 5, -5>\nposition=<-10163,  10476> velocity=< 1, -1>\nposition=<-51530, -10222> velocity=< 5,  1>\nposition=< 31241,  20817> velocity=<-3, -2>\nposition=<-51514,  20822> velocity=< 5, -2>\nposition=<-41155,  31158> velocity=< 4, -3>\nposition=< 41565,  10468> velocity=<-4, -1>\nposition=< 51949,  41503> velocity=<-5, -4>\nposition=< 20874,  10468> velocity=<-2, -1>\nposition=< 41611, -41253> velocity=<-4,  4>\nposition=<-30813, -30906> velocity=< 3,  3>\nposition=< 41574,  41511> velocity=<-4, -4>\nposition=< 51931,  51856> velocity=<-5, -5>\nposition=<-20508, -10220> velocity=< 2,  1>\nposition=< 20891, -41253> velocity=<-2,  4>\nposition=< 31249,  31159> velocity=<-3, -3>\nposition=< 41579,  31162> velocity=<-4, -3>\nposition=<-51527,  51853> velocity=< 5, -5>\nposition=<-30826,  20819> velocity=< 3, -2>\nposition=<-20508,  10471> velocity=< 2, -1>\nposition=<-20472,  10476> velocity=< 2, -1>\nposition=< 51924,  41507> velocity=<-5, -4>\nposition=<-30837, -51600> velocity=< 3,  5>\nposition=<-20487,  41512> velocity=< 2, -4>\nposition=<-51499,  41512> velocity=< 5, -4>\nposition=<-51508, -30910> velocity=< 5,  3>\nposition=<-20483, -20566> velocity=< 2,  2>\nposition=<-20516, -51596> velocity=< 2,  5>\nposition=< 20924, -30908> velocity=<-2,  3>\nposition=<-10144, -41257> velocity=< 1,  4>\nposition=<-41154, -41257> velocity=< 4,  4>\nposition=< 51902, -20558> velocity=<-5,  2>\nposition=< 41610, -10216> velocity=<-4,  1>\nposition=< 51917, -41248> velocity=<-5,  4>\nposition=<-20459,  31158> velocity=< 2, -3>\nposition=< 20884,  31162> velocity=<-2, -3>\nposition=<-20506, -30908> velocity=< 2,  3>\nposition=<-51533, -30903> velocity=< 5,  3>\nposition=<-10168, -30912> velocity=< 1,  3>\nposition=< 20904,  41506> velocity=<-2, -4>\nposition=<-51551,  31163> velocity=< 5, -3>\nposition=<-20500, -51595> velocity=< 2,  5>\nposition=< 31251, -20561> velocity=<-3,  2>\nposition=<-41186, -51595> velocity=< 4,  5>\nposition=<-20496,  31163> velocity=< 2, -3>\nposition=<-20458, -41257> velocity=< 2,  4>\nposition=< 41554, -41255> velocity=<-4,  4>\nposition=<-51511,  31163> velocity=< 5, -3>\nposition=<-51535,  20821> velocity=< 5, -2>\nposition=<-30853, -41249> velocity=< 3,  4>\nposition=< 20912, -51597> velocity=<-2,  5>\nposition=< 10527, -30903> velocity=<-1,  3>\nposition=< 41587,  41505> velocity=<-4, -4>")

(defn parse
  [raw]
  (->> raw
       (str/split-lines)
       (map #(let [[_ x y dx dy] (re-matches #"position=<([-\s]?\d+), ([-\s]?\d+)> velocity=<([-\s]?\d+), ([-\s]?\d+)>" %)
                   [x y dx dy] (map read-string [x y dx dy])]
               {:pos   [x y]
                :delta [dx dy]}))))

(def min-max (juxt min max))

(defn dimensions
  [state]
  (let
    [[min-x max-x] (apply min-max (select [ALL :pos FIRST] state))
     [min-y max-y] (apply min-max (select [ALL :pos LAST] state))]
    {:min-x min-x
     :max-x (inc max-x)
     :min-y min-y
     :max-y (inc max-y)}))

(defn maximums
  [state]
  (let [{:keys  [min-x max-x min-y max-y]} (dimensions state)]
    [(- max-x min-x) (- max-y min-y)]))


(defn draw
  [state]
  (println "")
  (let [{:keys [min-x max-x min-y max-y]} (dimensions state)
     board (partition (- max-x min-x)
                      (for [y (range min-y max-y)
                            x (range min-x max-x)]
                        [x y]))]
    (str/join "\n" (for [line board]
                     (apply str (map (fn [coord]
                                       (if (select-first [ALL :pos #(= coord %)] state)
                                         "#"
                                         " ")) line))))))

(defn next-state
  [state]
  (transform [ALL (collect-one :delta) :pos] add state))

(defn size
  [state]
  (apply * (maximums state)))

(defn reached-minimum?
  [state next-state]
  (> (size next-state) (size state)))


(defn solve-part-1
  [raw-input]
  (let [input (parse raw-input)]
    (draw (reduce (fn [state next-state]
                    (if (reached-minimum? state next-state)
                      (reduced state)
                      next-state))
                  (iterate next-state input)))))

(defn solve-part-2
  [raw-input]
  (let [input (parse raw-input)]
    (->> (iterate next-state input)
         (partition 2)
         (take-while #(apply (complement reached-minimum?) %))
         (count)
         (* 2)
         (dec))))

(comment

  (println (solve-part-1 raw-input))
  (solve-part-2 raw-input)

  (count (take-while #(apply (complement reached-minimum?) %) (partition 2 (iterate next-state input))))
  (* 3 10345)

  )