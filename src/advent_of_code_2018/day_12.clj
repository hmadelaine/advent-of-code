(ns advent-of-code-2018.day-12
  (:require [instaparse.core :as insta]
            [com.rpl.specter :refer :all]
            [clojure.string :as str]))

(def raw-input "initial state: ##.###.......#..#.##..#####...#...#######....##.##.##.##..#.#.##########...##.##..##.##...####..####\n\n#.#.# => #\n.##.. => .\n#.#.. => .\n..### => #\n.#..# => #\n..#.. => .\n####. => #\n###.. => #\n#.... => .\n.#.#. => #\n....# => .\n#...# => #\n..#.# => #\n#..#. => #\n.#... => #\n##..# => .\n##... => .\n#..## => .\n.#.## => #\n.##.# => .\n#.##. => #\n.#### => .\n.###. => .\n..##. => .\n##.#. => .\n...## => #\n...#. => .\n..... => .\n##.## => .\n###.# => #\n##### => #\n#.### => .")

(def raw-example "initial state: #..#.#..##......###...###\n\n...## => #\n..#.. => #\n.#... => #\n.#.#. => #\n.#.## => #\n.##.. => #\n.#### => #\n#.#.# => #\n#.### => #\n##.#. => #\n##.## => #\n###.. => #\n###.# => #\n####. => #")

(def ->char {"#" \#
             "." \.})
(defn parse
  [raw]
  (->> raw
       ((insta/parser
          "INPUT ::= <'initial state: '> STATE <CR> <CR> RULES
                RULES ::= RULE+
                STATE ::= #'[\\.#]+'
                RULE ::=  PATTERN <' => '> SUB <CR>?
                <PATTERN> ::= #'[\\.#]{5}'
                <SUB> ::= #'(\\.|#)'
                <CR> ::= '\n'"))
       (insta/transform
         {:INPUT (fn [& args] (apply merge args))
          :RULES (fn [& args] {:rules (apply merge args)})
          :STATE (fn [state] {:state (vec state)})
          :RULE  (fn [pattern sub] {(vec pattern) (->char sub)})})
       (merge {:offset 0})))

(defn expand
  [state]
  (into (into [\. \. \. \.] state)
        [\. \. \. \.]))

(defn debug
  [o]
  (doto o
    prn))


(defn generator
  [rules]
  (fn [input]
    (-> input
       (update :state (fn [state] (->> state
                                       (expand)
                                       (partition 5 1)
                                       (map #(get rules % \.)))))
       (update :offset #(+ 2 %)))))

(defn solve-part-1
  ([gen raw]
   (let [rules (:rules (parse raw))
         generation (generator rules)
         {:keys [state offset]} (->> (parse raw)
                                     (iterate generation)
                                     (drop gen)
                                     (take 1)
                                     (last))]
     (->> (zipmap (range (- offset) (count state)) state)
          (select [ALL (fn [[_ v]] (#{\#} v)) FIRST])
          (reduce +))))
  ([raw]
   (solve-part-1 20 raw)))

(comment
  (get-in [:rules [\. \# \# \# \#]] (parse raw-example))
  (select [:rules [\. \# \# \# \#]] (parse raw-example))

  (time (solve-part-1 raw-example))
  (solve-part-1 raw-input)
  (map #(apply - %) (partition 2 1
                               (select [ALL LAST] [[2000 46457]
                                                   [3000 69457]
                                                   [4000 92457]]
                                       )))
  )

(int (/ 92457N 4000N))
(/ 69457 3000
   )
(* 23 50000000000 )
