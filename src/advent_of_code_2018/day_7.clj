(ns advent-of-code-2018.day-7
  "Your first goal is to determine the order in which the steps should be completed.
  If more than one step is ready,
  choose the step which is first alphabetically.

  In this example, the steps would be completed as follows:

  Only C is available, and so it is done first.
  Next, both A and F are available. A is first alphabetically, so it is done next.
  Then, even though F was available earlier, steps B and D are now also available,
  and B is the first alphabetically of the three.
  After that, only D and F are available.
  E is not available because only some of its prerequisites are complete.
  Therefore, D is completed next.
  F is the only choice, so it is done next.
  Finally, E is completed.
  So, in this example, the correct order is CABDFE.

  In what order should the steps in your instructions be completed?"
  (:require [clojure.string :as str]
            [com.rpl.specter :refer :all]))


(def raw-example "Step C must be finished before step A can begin.\nStep C must be finished before step F can begin.\nStep A must be finished before step B can begin.\nStep A must be finished before step D can begin.\nStep B must be finished before step E can begin.\nStep D must be finished before step E can begin.\nStep F must be finished before step E can begin.")

(def raw-input "Step E must be finished before step H can begin.\nStep Y must be finished before step T can begin.\nStep F must be finished before step S can begin.\nStep U must be finished before step K can begin.\nStep X must be finished before step Z can begin.\nStep Q must be finished before step W can begin.\nStep W must be finished before step O can begin.\nStep G must be finished before step A can begin.\nStep N must be finished before step H can begin.\nStep S must be finished before step H can begin.\nStep K must be finished before step C can begin.\nStep J must be finished before step H can begin.\nStep T must be finished before step B can begin.\nStep C must be finished before step P can begin.\nStep L must be finished before step V can begin.\nStep Z must be finished before step A can begin.\nStep M must be finished before step A can begin.\nStep A must be finished before step P can begin.\nStep V must be finished before step O can begin.\nStep I must be finished before step O can begin.\nStep P must be finished before step H can begin.\nStep O must be finished before step D can begin.\nStep R must be finished before step B can begin.\nStep D must be finished before step B can begin.\nStep B must be finished before step H can begin.\nStep J must be finished before step L can begin.\nStep T must be finished before step V can begin.\nStep Z must be finished before step M can begin.\nStep G must be finished before step B can begin.\nStep K must be finished before step L can begin.\nStep Z must be finished before step H can begin.\nStep L must be finished before step M can begin.\nStep X must be finished before step A can begin.\nStep N must be finished before step M can begin.\nStep G must be finished before step M can begin.\nStep A must be finished before step V can begin.\nStep G must be finished before step S can begin.\nStep G must be finished before step J can begin.\nStep L must be finished before step A can begin.\nStep A must be finished before step H can begin.\nStep T must be finished before step M can begin.\nStep X must be finished before step N can begin.\nStep P must be finished before step O can begin.\nStep Y must be finished before step F can begin.\nStep U must be finished before step G can begin.\nStep G must be finished before step O can begin.\nStep P must be finished before step D can begin.\nStep G must be finished before step L can begin.\nStep Z must be finished before step P can begin.\nStep C must be finished before step L can begin.\nStep E must be finished before step B can begin.\nStep T must be finished before step Z can begin.\nStep D must be finished before step H can begin.\nStep U must be finished before step N can begin.\nStep E must be finished before step V can begin.\nStep L must be finished before step D can begin.\nStep K must be finished before step Z can begin.\nStep O must be finished before step R can begin.\nStep V must be finished before step R can begin.\nStep L must be finished before step O can begin.\nStep T must be finished before step H can begin.\nStep E must be finished before step Q can begin.\nStep S must be finished before step T can begin.\nStep U must be finished before step M can begin.\nStep Q must be finished before step V can begin.\nStep I must be finished before step B can begin.\nStep L must be finished before step Z can begin.\nStep Y must be finished before step B can begin.\nStep J must be finished before step C can begin.\nStep F must be finished before step Q can begin.\nStep J must be finished before step D can begin.\nStep Q must be finished before step L can begin.\nStep I must be finished before step D can begin.\nStep N must be finished before step V can begin.\nStep U must be finished before step H can begin.\nStep J must be finished before step R can begin.\nStep K must be finished before step V can begin.\nStep G must be finished before step P can begin.\nStep Y must be finished before step X can begin.\nStep L must be finished before step H can begin.\nStep R must be finished before step D can begin.\nStep S must be finished before step C can begin.\nStep Q must be finished before step A can begin.\nStep U must be finished before step X can begin.\nStep V must be finished before step B can begin.\nStep U must be finished before step Z can begin.\nStep F must be finished before step P can begin.\nStep G must be finished before step D can begin.\nStep O must be finished before step H can begin.\nStep C must be finished before step D can begin.\nStep L must be finished before step P can begin.\nStep N must be finished before step I can begin.\nStep Q must be finished before step O can begin.\nStep Q must be finished before step D can begin.\nStep Z must be finished before step D can begin.\nStep Y must be finished before step N can begin.\nStep M must be finished before step O can begin.\nStep W must be finished before step R can begin.\nStep S must be finished before step D can begin.\nStep O must be finished before step B can begin.\nStep I must be finished before step P can begin.")

(defn parent->children
  [input]
  (apply merge-with into
         (for [line (str/split-lines input)]
           (let [[_ parent child] (re-matches #"Step ([A-Z]) must be finished before step ([A-Z]) can begin."
                                              line)]
             {(keyword parent) #{(keyword child)}}))))

(defn find-root
  "Find the node that do not appear in dependances"
  [graph]
  (sort (let [nodes (set (keys graph))
              depends (set (select [MAP-VALS ALL] graph))]
          (clojure.set/difference nodes depends))))

(defn find-steps-order
  [instructions step constraints]
  (prn instructions step constraints)
  (if step
    (let [dependances (constraints step)
          f (first (sort dependances))
          depends (remove #{f} dependances)
          next-graph (-> constraints
                         (dissoc step)
                         (update f #(into (sorted-set) (concat depends %))))]
      (recur (conj instructions step) f next-graph))
    instructions))


(defn solve-part-1
  [input]
  (let [graph (parent->children input)
       [f & others] (find-root graph)
       others (into (sorted-set) others)]
    (str/join (map name (find-steps-order [] f (update graph f into others))))

   ))

(comment
  (solve-part-1 raw-example))