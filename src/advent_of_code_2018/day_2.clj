(ns advent-of-code-2018.day-2
  (:require [com.rpl.specter :refer :all]
            [clojure.string :as str]
            [clj-fuzzy.metrics :refer [levenshtein]]
            [clojure.math.combinatorics :as combo :refer [combinations]]))



;┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
;┃           ____  ___    ____  ______   ___       ┃
;┃          / __ \/   |  / __ \/_  __/  <  /       ┃
;┃         / /_/ / /| | / /_/ / / /     / /        ┃
;┃        / ____/ ___ |/ _, _/ / /     / /         ┃
;┃       /_/   /_/  |_/_/ |_| /_/     /_/          ┃
;┃                                                 ┃
;┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛

(defn letters-freq>2
  [id]
  (set (select [MAP-VALS (pred>= 2)] (frequencies id))))

(defn checksum-part-1
  [input]
  (->> input
       (mapcat letters-freq>2)
       (frequencies)
       (vals)
       (apply *)))

(defn solve-part-1
  [raw-input]
  (let [input (str/split-lines raw-input)]
    (checksum-part-1 input)))


;┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
;┃           ____  ___    ____  ______   ___       ┃
;┃          / __ \/   |  / __ \/_  __/  |__ \      ┃
;┃         / /_/ / /| | / /_/ / / /     __/ /      ┃
;┃        / ____/ ___ |/ _, _/ / /     / __/       ┃
;┃       /_/   /_/  |_/_/ |_| /_/     /____/       ┃
;┃                                                 ┃
;┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛


(defn same-characters
  "Returns the sequence of the common characters in a collections of strings.
   characters that differ are replaced by nil"
  [ids]
  (apply map (fn [& cs] (when (apply = cs) (first cs))) ids))

(defn differ-by-one-character?
  "do IDs differ by exactly one character at the same position in both strings ?"
  [ids]
  (when (= 1 (count (filter nil? (same-characters ids))))
    ids))

(defn all-pairs
  "Generate all possible pairs of IDs"
  [ids]
  (combinations ids 2))

(defn solve-part-2
  [input]
  (->> input
       (all-pairs)
       (some differ-by-one-character?)
       (same-characters)
       (apply str)))

(def raw-input "dghfbsyiznoumojleevappwqtr\ndghfbsyiznoumkjljevacpmqer\ndghfbbyizioumkjlxevacpwdtr\ndghfbsesznoumkjlxevacpwqkr\ndghfbsynznoumkjlxziacpwqtr\ncghfbsyiznjumkjlxevacprqtr\ndghfjsyizwoumkjlxevactwqtr\ndghfdsyfinoumkjlxevacpwqtr\nhghfbsyiznoumkjlxivacpwqtj\ndgcfbsyiznoumkjlxevacbuqtr\ndghfbsyiznoymnjlxevacpwvtr\ndfhfbsyiznoumkulxevacptqtr\ndghfasyiznovmkjlxevacpwqnr\ndghfbsyihnouikjlxevackwqtr\ndghfbayiznolmkjlyevacpwqtr\njghfbsyiznoumnjldevacpwqtr\ndhhfbsyuznoumkjlxevakpwqtr\nnehfrsyiznoumkjlxevacpwqtr\ndghfbsyiznxdmkolxevacpwqtr\ndgpfbsyizwlumkjlxevacpwqtr\nyghfbsyiznoumkjlsevacpwqtm\ndghfssyiznoumkjlxevvcpwqjr\ndahfbsyiznoumkjlfevacpwqto\nduhfcsyiznouvkjlxevacpwqtr\ndghfbvyiznoumkjlrevacpwvtr\ndghfgsyiznoumknlxgvacpwqtr\njghfbeyiznkumkjlxevacpwqtr\ndaofbsyiznoumkjlxevampwqtr\ndghfbsyiznojmkjlxeracpcqtr\ndghnbsyiznouokjlxevaclwqtr\ndgifbsyiznoumkjlxevnspwqtr\ndgkfpsziznoumkjlxevacpwqtr\ndghfxsyijnoumkjlxevaccwqtr\ndghfbsyiznolmkjlwevzcpwqtr\ndkhfbsaiznoumkjlxevacpwqtg\ndghfbsygknoumkjlaevacpwqtr\ndghfbsyizizumkjlxevacpxqtr\nighfbbyijnoumxjlxevacpwqtr\ndghfbsyizrouekjlxevacpwktr\ndghobsyiznoujkjlxevacnwqtr\ndghpbsyizyoumkjlxeaacpwqtr\ndghffsyiznoymkjlxevacewqtr\ndghkbssiznoumzjlxevacpwqtr\ndghfbsyawnoumkjlxevacpwjtr\ndrhfbsyiznormkjlfevacpwqtr\ndghfbsoiznouwkjlxevacpwqtn\ndghfmsyiznoumkjlxlvecpwqtr\ndxhfbsyiznoumkjlxeeacvwqtr\ndghnbsyiznoumkjsxevacpwqur\ndghfbsyiznrujkjlxevacpwqtc\ndghfbstoznoumhjlxevacpwqtr\ndghfboyiznzumkjlvevacpwqtr\ndghfbsyiznjumkjlxevgcpwmtr\ndghfbsnizaoumkjlxevacpwetr\ndghfbsyirnoumkjoxevacplqtr\ndghfbsyiznoumkjlxavvckwqtr\ndghfbjdiznoumkjlxevacpwptr\ndghfbsywznoumkjlxeiacpwqcr\ndjhfbsyizuoumkjlxelacpwqtr\ndghffsniznoumkjlxpvacpwqtr\ndghebsyizuoumkjlxevecpwqtr\nrghfbsyiznourkjcxevacpwqtr\ndghfbsyignoumkwlxenacpwqtr\ndghfbsyiznrufkjlxevacpwqth\ndgifbsyiznoumkjlxevacpjqnr\ndghfbsyiznoumkjbxevaxpwqtw\ndsufbsyizncumkjlxevacpwqtr\ndihfbsyiznoumujlxecacpwqtr\ndghfbiyiznoumkjlxevajpwqtn\ndghqbsyixnoumkjlrevacpwqtr\ndghfbsyiznouukjlxeuacpwqtx\ndghfbsyizyoumksfxevacpwqtr\ndghfbsiiznopfkjlxevacpwqtr\neghfbsyidnoumkjlxexacpwqtr\ndghfbgyiznouwkjlwevacpwqtr\ndghfbsyyznoumkjlxevacwwqtf\nbghfbtypznoumkjlxevacpwqtr\ndghfbsyiznoumtjlxebacpwetr\ndghfbsgiznonmkplxevacpwqtr\ndghfbsyiznoumxjlxevanpwqpr\ndghfbsyiznwumujuxevacpwqtr\ndghxbsyiznoumkjzxevaypwqtr\ndghfbsyhznoumkjlxlvacpiqtr\ndghfbsyiznoumkjlxevzcnwqrr\ndvhfbsyiznoumkjluevacpzqtr\ndghcbsyiznoumkjlxmvacpwetr\ndghfbsyiznohmkjvxbvacpwqtr\ndghfzsyiznouokjlxevacpwqpr\ndghfbsyiznoumkjlxevachtqth\ndghfbsyiznoumkjlxjvacpfutr\ndghfbsyiznoumkjlxevsppwqtt\ndghfusyiznouakhlxevacpwqtr\ndghfbsyizcoumkjlxrvaipwqtr\ndghebsyipnoumfjlxevacpwqtr\ndgdfbsyiznoumkjlwevacpkqtr\ndghfbsyiznoumkjlcffacpwqtr\ndghfbsypznfumkjlxevacpwqar\ndghfbsyiznojmkjlxevgcpkqtr\ndghfbsyiznoumkjlaevlcpwstr\ndgafrsyiunoumkjlxevacpwqtr\ndghfbsyiznouqljlxevacrwqtr\ndyhkbsyiznokmkjlxevacpwqtr\npghfbsciznoumkjlxevacpwvtr\ndghfbxyiznonmkjllevacpwqtr\nighfbsyizxoumkjlxevacpzqtr\ndgffbsyoznoumkjlxevacpwqto\nhghfbsyiznoumkjlpevachwqtr\ndlhfosyiznoumkjldevacpwqtr\ndghfbsvizkoumkjlxvvacpwqtr\ndbafbsyiznozmkjlxevacpwqtr\ndghfbsyidnoumkjlxrvjcpwqtr\ndghfbsyiznfumkjlxeqacpwqta\ndghfbsfiznoumkjvxevacjwqtr\ndghfbsyimnoumrjlhevacpwqtr\ndghflsyiznoumkjlxevacpvqmr\ndghfbmfiznoumkjlxevacpdqtr\ndghfbsyizsouzkjlxevscpwqtr\ndghfksyiznoumimlxevacpwqtr\ndghfbsyiznoumkjlxevbwpwqur\nwghcbsyiznoumkjlkevacpwqtr\nkghfbioiznoumkjlxevacpwqtr\ndghfbsiizeoumkjlxmvacpwqtr\ndglfbsyilnoumkjlxevpcpwqtr\ndgqfbsylznoumkjlxevacpwqcr\ndglfhsyiznoumkjlxevacpwqdr\ndghfbsymznoumkjlxbvacpwqtb\nhghfbsyizhoumkjlxtvacpwqtr\ndghdbsyiznoumkjlxeiacpyqtr\ndohfbsyiznoumkjmxlvacpwqtr\nxhhfbsyiznoumkjlxegacpwqtr\ndlhfbsyiznoumkjlxnvahpwqtr\ndghfbsyiznovdpjlxevacpwqtr\ndgcfbsyiznoumkjlxevactwqdr\ndghfksyiknoumkjlxevacpwqcr\nughfqsyiznoumkjlxevacpwctr\ndghfbjyiznoumkjlxsvacnwqtr\ndgwfbagiznoumkjlxevacpwqtr\ndghfbsyiznoumknlxevtcpwqdr\njghfksyiznoumkjlxeoacpwqtr\ndghfbsyiznoimkjlwezacpwqtr\ndghfbsyiunoumkjlxeqacpwstr\ndghfbsyizjoumkwlxevaypwqtr\ndghfysriznoumkjlxevucpwqtr\ndghfbsygzjoumkjfxevacpwqtr\ndghfbhviznoumkjlxevacpwqtq\ndghfbsyiznoumkjvwevacpwqur\ndghfbsyiznoumtjlxevacplqnr\nyghfbsysznouykjlxevacpwqtr\ndgwfbsiiznoumkjlxevacfwqtr\ndghfbsyizooumkjlxevampiqtr\ndshfbsyiznoumkjlxevawpoqtr\ndghtbsyxznuumkjlxevacpwqtr\ndkhfblyiznoumkjlxevacpaqtr\ndgkfbsyiinoumkjlxegacpwqtr\ndghfbtxiznouhkjlxevacpwqtr\ndghfbsyiznoumkjlxkvmcpeqtr\ndghfbsyiznoumkjlghvacpwqmr\ndghfbsbizioumkjlcevacpwqtr\ndphfbsyizhoumkjwxevacpwqtr\ndghfbsyiznqumkjlugvacpwqtr\ndghfbsjinnoumkjlxevacpwetr\nmghfbsyiznoumkjlxfvacpjqtr\ndghfbsxiznoumkjlxetacwwqtr\ndghmbsyiznoumbjlxevacpwqyr\ndghfbsyiznwumkjlwevacmwqtr\ndgkfbsyiznotmkjlxevacpwstr\ndghfbsyiznouykjlxeiacuwqtr\ndghfbsynznbhmkjlxevacpwqtr\ndgyfbsyiznoumtjlbevacpwqtr\ndghfbftiznoumkjlxevacpwatr\ndghfvsyiznouikjlievacpwqtr\ndghfbsyiznodmkjlxevncpwqtz\nyfhfbsyiznoumkjluevacpwqtr\ndghfbzyiznoumhflxevacpwqtr\ndphfbsyizncumkjlxevacpwqtf\ndghfasyiznoumkjlxeaicpwqtr\ndgffbsyiznoumkjlzevacpwqsr\ndghfbsyiznoumkmxxcvacpwqtr\ndghffsyiznoumkjlxevacpwqre\ndghfbsyizndmmkjlxemacpwqtr\ndghfbsviznoamkjlxevappwqtr\ndghfbsyiznouckrlxevacpdqtr\ndgwfbsyiznyumkjlxevacpqqtr\ndujfbsyiznoumgjlxevacpwqtr\ndghobsailnoumkjlxevacpwqtr\ndghfkqyiznoumknlxevacpwqtr\ndghfbyypznoumkjlxevacpwatr\nwqhfbsyiznoumkjlxevzcpwqtr\ndghfbsyiznoumwjlxrvacppqtr\ndghfbsymznoumkflxevacplqtr\ndghfbsyiznounkjpgevacpwqtr\nighfbsyijnoumxjlxevacpwqtr\ndghfbsyizroumkjllevncpwqtr\ndghfbsliznokmkjlxevacpwqtb\ndgefbsyiznoumkqlxevpcpwqtr\ndghfbtypznouzkjlxevacpwqtr\ndmhfbsyiznoumkjlxeyactwqtr\nvohfbsyiznoumkjlqevacpwqtr\ndgsfpsyiznodmkjlxevacpwqtr\ndghfzsyijnoumkjnxevacpwqtr\ndghfbayijroumkjlxevacpwqtr\ndghfbsyiznodmxjlxgvacpwqtr\ndghfbsyiznocmkjlxhvaipwqtr\ndghmbsyignoumkjlxevacpoqtr\ndghfbsyiznosmkjlncvacpwqtr\ndggfbsyiznuumkjlxevacpwqrr\ndghibsyilnoumkjlxevacowqtr\ndghfbsyiznoumkjluevbcowqtr\ndghfbsaiznyuvkjlxevacpwqtr\ndgnfxsyiznommkjlxevacpwqtr\ndghfbnyiznoumkjlsnvacpwqtr\ndghfssiiznoumkjlxavacpwqtr\ndghfbsyizneumajlxevacfwqtr\ndghfbsyiznoumkjlxevycpvptr\nqghfbsyizgoumkjlxevacpwttr\nvghfbsyiznoumkjlievaepwqtr\ndghfbsyiznoumejlxjvacpwdtr\ndghfbsyispoumkjlxevacpwqtg\nduhfbsyizpoumkjlxenacpwqtr\ndghfbsyifnoumkblxevacpnqtr\nbghfbsyxznoumkjleevacpwqtr\ndgtfbsyzpnoumkjlxevacpwqtr\ndghfbsyiznoumkjlsecacpwqth\ndghfqsyiznjumkjlxevawpwqtr\ndgcfbsyizboumkjlxevacqwqtr\ndghfbqyiznoumkjkxevacpwqtj\ndgyfbsyfznoumkjlievacpwqtr\ndghfdsyiznoumkplxevacpwdtr\ndphfbsyuznkumkjlxevacpwqtr\ndghfbsyiznoupkjitevacpwqtr\ndghfisyiznoamkjlxevacpwqwr\ndgufbsyiznoumkjlxivvcpwqtr\ndghfbvyiznoumkjlxevacvwqtz\ndghfbsyiqnxumkjlxbvacpwqtr\ndghubsyiznqumkflxevacpwqtr\ndghfbsyiznzumkjlxevacpdbtr\ndghfbsyiznoumkjlxehacpwwrr\nmghfbsyiznoumkjlxevacpwqbp\ndvhfbryiznoumkclxevacpwqtr\ndghbbsyiznotmkjlxevacpwqhr\ndghfrsyiznoomkjlxevacpwqto\ndghfbkyiznoumkjlxeracpxqtr\ndghfbfyizfoumkjlxevacpwjtr\ndghfbsyizqoulkjlxevacpwqtt\ndghfbsyiwnoumkjlxevacxwgtr\ndghfbsyiznormkjlgxvacpwqtr\ndghybsyizioumkjoxevacpwqtr\ndchfbsyiznoumkjlxyvacpwqtc\ndgyfbsyiznouckjlxewacpwqtr\ndakfbsyeznoumkjlxevacpwqtr")

(def input (str/split-lines raw-input))



;Solution using Levenshtein distance

#_(defn differ-by-one-character-levenshtein?
    "do IDs differ by exactly one character at the same position in both strings ?
     Using the Levenshtein distance.
     this solution is slow."
    [[id id']]
    (when (= 1 (levenshtein id id'))
      [id id']))







