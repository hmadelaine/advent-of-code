(ns advent-of-code-2018.day-6
  "Using only the Manhattan distance,
   determine the area around each coordinate by counting the number of integer X,Y locations
   that are closest to that coordinate (and aren't tied in distance to any other coordinate).
   Your goal is to find the size of the largest area that isn't infinite."
  (:require [clojure.string :as str]
            [clojure.math.combinatorics :refer [combinations]]
            [cartesian.utils :refer [manhattan-distance sparse-draw]]
            [net.cgrand.xforms :as x]))


(def raw-example "1, 1\n1, 6\n8, 3\n3, 4\n5, 5\n8, 9")
(def raw-input "292, 73\n204, 176\n106, 197\n155, 265\n195, 59\n185, 136\n54, 82\n209, 149\n298, 209\n274, 157\n349, 196\n168, 353\n193, 129\n94, 137\n177, 143\n196, 357\n272, 312\n351, 340\n253, 115\n109, 183\n252, 232\n193, 258\n242, 151\n220, 345\n336, 348\n196, 203\n122, 245\n265, 189\n124, 57\n276, 204\n309, 125\n46, 324\n345, 228\n251, 134\n231, 117\n88, 112\n256, 229\n49, 201\n142, 108\n150, 337\n134, 109\n288, 67\n297, 231\n310, 131\n208, 255\n246, 132\n232, 45\n356, 93\n356, 207\n83, 97")

(def ->int #(Integer/parseInt %))

(defn parse
  [raw]
  (->> raw
       (str/split-lines)
       (map #(str/split % #",\s"))
       (map #(map ->int %))
       (map (fn [id coords]
              {:id     (char id)
               :coords coords}) (into (range 97 (+ 97 26)) (range 65 (+ 65 26))))))

(defn pre-draw
  [area]
  (into {} (map (fn [{:keys [id coords]}] {coords id})) area))

(defn find-min-max
  [coords]
  (let [sup (fnil > Integer/MAX_VALUE)
        inf (fnil < Integer/MIN_VALUE)]
    (reduce (fn [{:keys [min-x max-x max-y min-y] :as acc}
                 [x y]]
              (cond-> acc
                      (inf max-x x) (assoc :max-x x)
                      (sup min-x x) (assoc :min-x x)
                      (inf max-y y) (assoc :max-y y)
                      (sup min-y y) (assoc :min-y y)))
            {}
            coords)))

(defn find-infinite-ids
  "Infinite area have coords located at the min and max"
  [board min-max]
  (reduce (fn [acc location]
            (let [{:keys [min-x max-x max-y min-y]} min-max
                  {[x y] :coords id :id} location]
              (if (acc id)
                acc
                (if (or (<= x min-x)
                        (>= x max-x)
                        (<= y min-y)
                        (>= y max-y))
                  (conj acc id)
                  acc)))) #{} board))

(defn all-empty-locations
  [board]
  (let [all-coordinates (set (map :coords board))
        {:keys [min-x max-x max-y min-y] :as min-max} (find-min-max all-coordinates)
        all-locations (for [y (range min-y (inc max-y))
                            x (range min-x (inc max-x))
                            :when (not (all-coordinates [x y]))]
                        [[x y] (sort-by :distance
                                        (map (fn [location]
                                               (assoc location :distance (manhattan-distance [x y] (:coords location)))) board))])]
    {:min-max       min-max
     :all-locations all-locations}))

(defn find-neighboors
  "Locations that are closest to that coordinate
   and aren't tied in distance to any other coordinate."
  [board]
  (let [{:keys [all-locations min-max]} (all-empty-locations board)
        closest (for [[location distances] all-locations
                      :let [[d1 d2] (map :distance distances)]
                      :when (not= d1 d2)]
                  {:id (:id (first distances)) :coords location})
        infinite-ids (find-infinite-ids closest min-max)]
    (into board (remove #(infinite-ids (:id %)) closest))))

(defn find-max
  "Your goal is to find the size of the largest area that isn't infinite."
  [locations-and-neighboors]
  (apply max-key last (into {} (x/by-key :id x/count) locations-and-neighboors)))

(defn solve-part-1
  [raw]
  (-> raw
      (parse)
      (find-neighboors)
      (find-max)))

;┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
;┃           ____  ___    ____  ______   ___       ┃
;┃          / __ \/   |  / __ \/_  __/  |__ \      ┃
;┃         / /_/ / /| | / /_/ / / /     __/ /      ┃
;┃        / ____/ ___ |/ _, _/ / /     / __/       ┃
;┃       /_/   /_/  |_/_/ |_| /_/     /____/       ┃
;┃                                                 ┃
;┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛



(comment

  (println (sparse-draw "." (pre-draw (find-neighboors (parse raw-input)))))

  (time (solve-part-1 raw-example))
  (time (solve-part-1 raw-input))

  (count (parse raw-input))
  (into {} (x/by-key :id x/count) (find-neighboors (parse raw-input)))


  (apply max-key last reponse)

  (find-max (find-neighboors (parse raw-example)))


  (println (sparse-draw "." (pre-draw (parse raw-input))))
  (println (sparse-draw "." (pre-draw (parse raw-example))))

  )

