(ns advent-of-code-2015.day-5-The-Ideal-Stocking-Stuffer
  (:require [pandect.algo.md5 :refer [md5]]
            [clojure.string :as str]))


(defn solve
  [clef num]
  (let [search (str/join (repeat num 0))]
    (loop [num 0]
      (if (str/starts-with? (md5 (str clef num)) search)
        num
        (recur (inc num))))))

