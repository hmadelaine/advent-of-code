(ns cartesian.utils
  (:require
    [clojure.set]
    [clojure.spec.alpha :as s]
    [clojure.string :as str]
    [com.rpl.specter :as sp]))

(s/def :cartesian/point int?)

(s/def :cartesian/coords (s/tuple :cartesian/point :cartesian/point))


(defn right
  [[x y]]
  [y (- x)])

(defn opposite
  [[x y]]
  [(- x) (- y)])

(defn left
  [coord]
  (opposite (right coord)))


#_(defn add
    [from delta]
    (let [[x y] from
          [dx dy] delta]
      [(+ x dx) (+ y dy)]))

(defn add
  [& coords]
  (apply mapv + coords))

(s/fdef add
        :args (s/cat :point :cartesian/coords :delta :cartesian/coords)
        :ret :cartesian/coords)

(defn minus
  [& coords]
  (apply mapv - coords))

(defn manhattan-distance
  [& coords]
  (reduce + (mapv abs (apply minus coords))))


(def moves->coords
  "
             y
   ┌─────────────────▶
   │
   │         ▲ up
 x │         │
   │ left ◀──┼──▶ righ
   │         │
   │         ▼
   ▼        down
 "
  {:up    [-1 0]
   :down  [1 0]
   :right [0 1]
   :left  [0 -1]})

(def coords->moves (clojure.set/map-invert moves->coords))

(def headings (set (keys moves->coords)))



(defn parse->coords
  ([dico raw]
   (let [lines (str/split-lines raw)]
     (apply merge (flatten (map-indexed (fn [y line]
                                          (map-indexed (fn [x c]
                                                         {[x y] (dico c)}) line)) lines)))))
  ([raw]
   (parse->coords identity raw)))

(defn coords->raw
  ([reverse-dico area]
   (let [coords (keys area)
         dim-x (inc (apply max (map first coords)))
         dim-y (inc (apply max (map last coords)))]
     (str/join "\n"
               (for [y (range dim-y)]
                 (str/join (for [x (range dim-x)]
                             (reverse-dico (area [x y]))))))))
  ([area]
   (coords->raw identity area)))


(defn sparse-draw
  ([translation missing board]
   (let [[min-x max-x] (apply (juxt min max) (map ffirst board))
         [min-y max-y] (apply (juxt min max) (map (comp second first) board))]
     (str/join "\n"
               (let [lines (into (sorted-map) (group-by (comp second first) board))]
                 (for [y (range min-y (inc max-y))
                       :let [line (lines y)]]
                   (str/join (for [x (range min-x (inc max-x))
                                   :let [line (into {} line)]]
                               (or (translation (get line [x y])) missing))))))))
  ([missing board]
   (sparse-draw identity missing board))
  ([board]
   (sparse-draw identity " " board)))


(defn draw-regular-axis
  " y  ▲
       │
       │
       │
       │
       └──────────▶
                  x"
  ([translation missing board]
   (let [[min-x max-x] (apply (juxt min max) (map ffirst board))
         [min-y max-y] (apply (juxt min max) (map (comp second first) board))]
     (str/join "\n"
               (let [lines (into (sorted-map) (group-by (comp second first) board))]
                 (for [y (range max-y (dec min-y) -1)
                       :let [line (lines y)]]
                   (str/join (for [x (range min-x (inc max-x))
                                   :let [line (into {} line)]]
                               (or (translation (get line [x y])) missing))))))))
  ([missing board]
   (draw-regular-axis identity missing board))
  ([board]
   (draw-regular-axis identity " " board)))

(defn adjacents
  [[x y]]
  (set
    (filter
      (fn [[x y]]
        (and (>= x 0) (>= y 0)))
      [[(inc x) y]
       [x (dec y)]
       [(dec x) y]
       [x (inc y)]])))

(defn all-adjacents
  [board coord]
  (let [adjacent-coords (for [x [-1 0 1]
                              y [-1 0 1]
                              :when (not (and (= x 0) (= y 0)))]
                          (add coord [x y]))]
    (filter board adjacent-coords)))

(defn neighboors
  [coord board]
  (select-keys board (adjacents coord)))


(def min-max (juxt min max))

(defn dimensions
  [board]
  (let
    [[min-x max-x] (apply min-max (sp/select [sp/MAP-KEYS sp/FIRST] board))
     [min-y max-y] (apply min-max (sp/select [sp/MAP-KEYS sp/LAST] board))]
    {:min-x min-x
     :max-x max-x
     :min-y min-y
     :max-y max-y}))

(defn fully-contains?
  "[[0 6] [2 3]] => true
   [[0 6] [5 7]] => false"
  [pair]
  (let [[[ff ft] [sf st]] pair]
    (or (and (<= ff sf)
             (>= ft st))
        (and (<= sf ff)
             (>= st ft)))))

(defn overlap?
  "[[0 6] [2 3]] => true
   [[0 6] [5 7]] => true
   [[0 6] [7 9]] => false"
  [pair]
  (let [[[ff ft] [sf st]] pair]
    (or (and (>= ft sf)
             (<= ff sf))
        (and (<= ff st)
             (<= sf ff)))))

(defn distance
  [q p]
  (let [[qx qy] q
        [px py] p]
    (Math/sqrt
      (+ (* (- qx px) (- qx px))
         (* (- qy py) (- qy py))))))

(comment

  (manhattan-distance [-1 0] [2 0])
  (s/exercise-fn `add)

  (let [init [0 0]
        identite (comp left right)]
    (= init (identite init)))

  (let [init [0 0]
        left-left (comp left left)]
    (= (opposite init) (left-left init)))

  (let [init [0 0]
        right-right (comp right right)]
    (= (opposite init) (right-right init))))


