(ns advent-of-code-2022.day-12-Hill-Climbing-Algorithm
  (:require [cartesian.utils :as coords]
            [clojure.set :as set]
            [com.rpl.specter :as sp]
            [ubergraph.core :as uber]
            [ubergraph.alg :as alg]))


(def example "Sabqponm\nabcryxxl\naccszExk\nacctuvwj\nabdefghi")

(def input "abaccccccccccccccaaaccccaaaaaaaaaaaaaccccccaacccccccccccccccccccccccccccccaaaaaa\nabaaccaacccccccccaaaaaccccaaaaaaaaaaaaaccccaaacccccccccccccccccccccccccccccaaaaa\nabaaccaaacccccccaaaaaacccaaaaaaaaaaaaaacaaaaaaaaccccccccaacccccccccccccccccccaaa\nabcaaaaaaaacccccaaaaaacccaaaaaaaaaaaaaacaaaaaaaacccccccaaaacccccccccccccccccaaaa\nabcaaaaaaaaccccccaaaaaccaaaaaaaaccaaaaaccaaaaaaccccccccaaaaccaaaccccccccccccaaac\nabccaaaaaacccccccaaaaccaaaaaaaaaacaaaacccaaaaaacccccccccakkaaaaaacccccccccccaacc\nabccaaaaaacccccccccccccaaaaaaaaaaccccccccaaaaaaccccccckkkkkkkaaacccccccccccccccc\nabccaaaaaaccccccccccccccccaaaaaaaaaccccccaacaaacccccckkkkkkkkkaccccccaccaaaccccc\nabccaacaaacccccaaccccccccaaacacaaaacaaccccccccccccccakkkoppkkkkicccccaaaaaaccccc\nabccccccccccccaaaccccccccaacccccaaaaaaccccccccccccccjkkooppppkiicccccccaaaaccccc\nabccccccccccaaaaaaaaccccccccccaaaaaaaccccccccccccccjjjooopppppiiiicccccccaaacccc\nabaaacccccccaaaaaaaacccccccaacaaaaaaccccccccccccccjjjjooouuppppiiiiiicccccaacccc\nabaaaccccccccaaaaaaccccccccaaaccaaaaacccccccccccjjjjjooouuuupppiiiiiiiiccccacccc\nabaaaaaacccccaaaaaacccccaaaaaaaaaacaaaccccccccjjjjjjooouuuuuupppppiiiiiicccccccc\nabaaaaaacccccaaaaaacccccaaaaaaaaaacccccccccccjjjjjooooouuxxuupppppqqqijjjccccccc\nabaaaacccccaaaaccaaccccccaaaaaaccccccccccccciijjnooooouuuxxxuuupqqqqqqjjjdddcccc\nabaaaaaccaaaaaaccacccccccaaaaaaccccccccccaaiiiinnootttuuxxxxuuvvvvvqqqjjjdddcccc\nabaaaaaccaaaaaacaaaccaaccaaaaaaccccccccccaaiiinnnntttttuxxxxxvvvvvvqqqjjjdddcccc\nabaaccacccaaaaacaaaaaaaccaaccaaccccccccccaaiiinnnttttxxxxxxxyyyyyvvqqqjjjdddcccc\nabcccccccaaaaacccaaaaaaccccccaaaaacccccccaaiiinnntttxxxxxxxyyyyyvvvqqqjjjddccccc\nSbcccccccaaaaacaaaaaaaaccccccaaaaaccccccccciiinnntttxxxEzzzzyyyyvvqqqjjjdddccccc\nabcccccccccccccaaaaaaaaaccccaaaaaaccccccccciiinnnntttxxxxyyyyyvvvvqqjjjdddcccccc\nabcccccccccccccaaaaaaaaaacccaaaaaacccccccccciiinnnttttxxxyyyyyvvvqqqjjjdddcccccc\nabccccccccccccccccaaaaaaacccaaaaaaccccccccccciiinnnntttwyyywyyyvvrrrkkjdddcccccc\nabcccccccccccccccaaaaaaaaccccaaaccccccccccccciiihnnnttwwwywwyyywvrrrkkkeeccccccc\nabcccccccccccccccaaaaaaaaccccccccccccccccccccchhhmmmsswwwwwwwwwwwvrrkkkeeccccccc\nabcccccccaacccccccacaaacccccccccccccccccccaacchhhhmmsswwwwwswwwwwrrrkkkeeccccccc\nabcccccccaaaccacccccaaacccccccccccccccaaccaaccchhhmmssswwwssrrwwwrrrkkkeeccccccc\nabcccccccaaaaaaacccccccccccaaaccccccccaaaaaaccchhhmmssssssssrrrrrrrrkkkeeaaacccc\nabcccccaaaaaaaaccccccccccccaaaaccccccccaaaaaaachhhmmmssssssllrrrrrrkkkeeeaaacccc\nabccccaaaaaaaaaccccccccccccaaaacccccccccaaaaacchhhmmmmsssllllllllkkkkkeeeaaacccc\nabccccaaaaaaaaaccccccccccccaaacccccccccaaaaacccchhhmmmmmlllllllllkkkkeeeeaaccccc\nabcccccccaaaaaaccccccccccaacccccccaaccaaacaacccchhhmmmmmlllgfflllkkffeeeaaaacccc\nabccccccaaaaaaaccccccccccaaaaaaaaaaaaaccccaacccchhhggmmmggggffffffffffeaaaaacccc\nabccaacccaacccaaaaccaccccaaaaaaaaaaaaacccccccccccgggggggggggffffffffffaacccccccc\nabaaaaccaaaccccaaaaaaccccaaaaaacaaaaaaccccccccccccgggggggggaaaaccffccccccccccccc\nabaaaacccccccccaaaaaaccaaaaaaaaaaaaaacccccccccccccccgggaaaaaaaacccccccccccccccca\nabaaaaacccccccaaaaaaaccaaaaaaaaaaaaaacccccccccccccccccaaacccaaaaccccccccccccccaa\nabaaaaacaaaaccaaaaaaaacaaaaaaaaaaaccccccccccccccccccccaaaccccaaaccccccccccaaacaa\nabaaaaacaaaaccaaaaaaaaaaaaaaaaaaacccccccccccccccccccccccccccccccccccccccccaaaaaa\nabaaacccaaaaccccaaaccccaaaaaaaaaaacccccccccccccccccccccccccccccccccccccccccaaaaa")

(defn prepare-graph
  [input]
  (let [parsed (coords/parse->coords input)
        inverted (set/map-invert parsed)
        area (->> parsed
                  (sp/setval [sp/MAP-VALS #{\S}] \a)
                  (sp/setval [sp/MAP-VALS #{\E}] \z)
                  (sp/transform [sp/MAP-VALS] int))]
    {:area       area
     :start-node (inverted \S)
     :end-node   (inverted \E)}))

(defn make-graph
  [area]
  (let [graph (reduce
                (fn [graph node]
                  (let [[coord height] node
                        candidats (coords/neighboors coord area)]
                    (into graph
                          (reduce
                            (fn [graph candidat]
                              (let [[coordc heightc] candidat
                                    diff (- heightc height)]
                                (cond
                                  (>= height heightc) (conj graph [coord coordc])
                                  (and (< height heightc)
                                       (== 1 diff)) (conj graph [coord coordc])
                                  (and (< height heightc)
                                       (< 1 diff)) (conj graph [coordc coord]))))
                            graph
                            candidats))))
                #{}
                area)]
    (apply uber/digraph graph)))

(defn find-shortest-steps
  [graph from to]
  (count
    (alg/edges-in-path
      (alg/shortest-path
        graph
        {:start-node from
         :end-node   to}))))

(defn solve-part-1
  [input]
  (let [{:keys [area start-node end-node]} (prepare-graph input)
        graph (make-graph area)]
    (find-shortest-steps graph start-node end-node)))

(defn find-letter
  [area letter]
  (keep
    (fn [[coord height]]
      (when (== (int letter) height) coord))
    area))

(defn solve-part-2
  [input]
  (let [{:keys [area end-node]} (prepare-graph input)
        graph (make-graph area)
        as (find-letter area \a)]
    (->> as
         (map #(find-shortest-steps graph % end-node))
         (filter pos?)
         (apply min))))


(comment
  (solve-part-1 example)
  (solve-part-2 example)
  (assert (= 391 (solve-part-1 input)))
  (assert (= 386 (solve-part-2 input)))

  )