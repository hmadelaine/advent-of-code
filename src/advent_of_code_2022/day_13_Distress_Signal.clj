(ns advent-of-code-2022.day-13-Distress-Signal
  "When comparing two values, the first value is called left and the second value is called right. Then:

  If both values are integers,
    the lower integer should come first.
    If the left integer is lower than the right integer, the inputs are in the right order.
    If the left integer is higher than the right integer, the inputs are not in the right order.
    Otherwise, the inputs are the same integer; continue checking the next part of the input.

  If both values are lists, compare the first value of each list, then the second value, and so on.
    If the left list runs out of items first, the inputs are in the right order.
    If the right list runs out of items first, the inputs are not in the right order.
    If the lists are the same length and no comparison makes a decision about the order, continue checking the next part of the input.

  If exactly one value is an integer, convert the integer to a list which contains that integer as its only value,
  then retry the comparison. For example, if comparing [0,0,0] and 2, convert the right value to [2] (a list containing 2); the result is then found by instead comparing [0,0,0] and [2].
"
  (:require
    [clojure.edn :as edn]
    [clojure.string :as str]))

(def example "[1,1,3,1,1]\n[1,1,5,1,1]\n\n[[1],[2,3,4]]\n[[1],4]\n\n[9]\n[[8,7,6]]\n\n[[4,4],4,4]\n[[4,4],4,4,4]\n\n[7,7,7,7]\n[7,7,7]\n\n[]\n[3]\n\n[[[]]]\n[[]]\n\n[1,[2,[3,[4,[5,6,7]]]],8,9]\n[1,[2,[3,[4,[5,6,0]]]],8,9]")

(defn parse-packets
  [input]
  (->> (str/split input #"\n\n")
       (map str/split-lines)
       (map #(mapv edn/read-string %))))

(defn check
  [l r]
  (let [check-all (fn check-all
                    [lcoll rcoll]
                    (loop [[l & ls] lcoll
                           [r & rs] rcoll
                           res nil]
                      (if (some? res) res
                        (cond
                         (and (nil? l) (some? r)) true
                         (and (some? l) (nil? r)) false
                         ;(and (nil? l) (nil? r)) false
                         :else (recur ls rs (check l r))))))]
    (cond
      (every? vector? [l r]) (check-all l r)
      (and (every? number? [l r]) (< l r)) true
      (and (every? number? [l r]) (= l r)) nil
      (and (every? number? [l r]) (> l r)) false
      (and (number? l) (vector? r)) (check (vector l) r)
      (and (vector? l) (number? r)) (check l (vector r))
      (every? nil? [l r]) nil
      :else false)))

(comment
  (assert (true? (check [1 1 3 1 1] [1 1 5 1 1])))
  (assert (true? (check [[1], [2, 3, 4]]
                        [[1], 4])))
  (assert (true? (check [2 3 4] [4])))
  (assert (false? (check [9] [8 7 6])))
  (assert (false? (check [7 7 7 7] [7 7 7])))
  (assert (true? (check [] [3])))
  (assert (false? (check [[[]]] [[]])))
  (assert (false? (check [1, [2, [3, [4, [5, 6, 7]]]], 8, 9] [1, [2, [3, [4, [5, 6, 0]]]], 8, 9])))



  (do
    (require '[clojure.tools.trace :as trace])
    (trace/trace-vars check))
  )

(check [2 3] [4])
(check 1 2)
(check 3 2)
(check 1 1)
(check [1] [1])

(get [4] 1)


(assert (true? (check [[1], [2, 3, 4]] [[1], 4])))
(assert (false? (check [9] [[8, 7, 6]])))

(apply check [[1 [2 [3 [4 [5 6 7]]]] 8 9] [1 [2 [3 [4 [5 6 0]]]] 8 9]])

