(ns advent-of-code-2022.day-9-Rope-Bridge
  "H and T must always be touching.
   Touching : diagonally adjacent and even overlapping

   If the head is 2 steps directly : up, down, left, or right from the tail :
   the tail must also move one step in that direction

   If the head and tail aren't touching and aren't in the same row or column :
   the tail always moves one step diagonally to keep up

   "
  (:require
    [clojure.string :as str]
    [cartesian.utils :as coords]))

(def example "R 4\nU 4\nL 3\nD 1\nR 4\nD 1\nL 5\nR 2")
(def input "D 2\nR 2\nU 2\nD 2\nL 1\nD 1\nL 1\nD 1\nU 1\nR 2\nU 2\nD 2\nR 2\nD 2\nR 2\nU 2\nD 1\nR 2\nL 2\nR 2\nL 2\nD 2\nR 1\nU 1\nL 2\nD 1\nR 1\nD 2\nU 1\nL 2\nR 1\nU 1\nR 1\nU 1\nL 2\nU 1\nL 2\nR 2\nU 1\nD 2\nR 2\nU 2\nL 1\nR 1\nU 1\nD 1\nU 1\nD 2\nU 1\nR 2\nL 1\nD 1\nU 2\nD 2\nU 1\nR 2\nU 1\nL 2\nR 2\nD 1\nR 1\nU 1\nD 1\nR 1\nU 2\nD 2\nR 2\nD 2\nR 1\nL 2\nD 1\nL 2\nD 2\nU 2\nD 1\nU 1\nL 2\nU 2\nR 2\nD 1\nL 1\nD 2\nL 1\nU 2\nD 2\nR 1\nU 1\nD 2\nR 1\nU 1\nR 2\nL 2\nU 1\nL 2\nR 1\nL 1\nD 2\nR 1\nD 2\nL 1\nR 1\nL 1\nU 2\nR 1\nL 1\nU 2\nD 2\nR 2\nD 1\nL 1\nD 2\nL 3\nR 2\nU 2\nR 1\nU 1\nR 2\nL 2\nD 2\nR 3\nL 2\nR 3\nU 3\nD 3\nU 2\nR 1\nU 3\nD 1\nU 3\nD 1\nU 1\nD 1\nR 1\nL 2\nD 3\nR 2\nU 1\nR 1\nU 2\nD 3\nL 2\nD 2\nR 1\nU 2\nD 1\nR 2\nL 3\nD 2\nL 3\nU 2\nR 2\nU 3\nL 2\nR 1\nU 3\nL 1\nD 2\nR 3\nU 3\nD 3\nR 3\nD 3\nL 3\nD 2\nL 1\nU 1\nR 2\nU 3\nL 2\nR 1\nL 2\nR 3\nL 1\nR 2\nL 1\nD 3\nU 3\nL 1\nR 3\nD 3\nL 2\nU 1\nD 1\nL 3\nD 3\nL 2\nD 1\nU 2\nD 2\nL 1\nU 2\nL 3\nD 1\nU 1\nR 3\nU 2\nL 3\nU 3\nL 2\nR 1\nU 2\nD 1\nR 3\nD 1\nU 1\nL 2\nD 1\nR 3\nD 3\nU 2\nD 3\nU 3\nR 2\nL 2\nR 3\nL 2\nR 2\nD 3\nR 2\nU 1\nD 1\nR 3\nU 3\nL 2\nU 1\nR 3\nU 4\nL 4\nD 4\nU 2\nL 2\nD 3\nL 2\nR 2\nL 4\nU 3\nR 1\nU 2\nR 4\nL 2\nU 3\nR 3\nL 1\nR 1\nU 4\nL 2\nR 1\nU 4\nL 4\nU 1\nD 1\nU 1\nR 2\nL 2\nD 1\nU 3\nD 2\nU 4\nR 4\nL 2\nU 4\nR 4\nD 1\nR 1\nU 2\nL 3\nR 3\nD 2\nL 1\nU 4\nR 3\nD 1\nU 2\nD 3\nR 4\nU 3\nD 3\nR 1\nL 2\nD 4\nL 2\nR 2\nD 4\nU 4\nD 4\nL 4\nU 4\nD 4\nL 2\nR 3\nL 3\nR 3\nL 4\nR 1\nL 1\nU 2\nD 4\nL 4\nR 1\nD 1\nR 3\nL 2\nU 3\nR 4\nU 4\nR 2\nD 4\nR 3\nU 4\nR 4\nL 1\nD 4\nU 3\nR 3\nD 3\nU 3\nR 3\nU 3\nL 3\nD 2\nR 3\nL 1\nR 4\nD 1\nR 3\nD 4\nL 4\nU 4\nR 1\nD 3\nU 4\nR 1\nD 2\nR 4\nD 5\nU 2\nL 2\nU 3\nR 4\nL 2\nU 3\nL 4\nU 3\nD 3\nR 3\nD 3\nL 3\nU 1\nR 3\nL 4\nR 1\nU 3\nR 1\nL 4\nD 5\nU 3\nD 2\nR 2\nU 2\nD 4\nR 4\nU 2\nR 1\nL 3\nD 1\nL 3\nR 4\nL 5\nD 4\nR 2\nL 5\nR 5\nU 1\nD 4\nR 2\nD 5\nR 1\nL 3\nR 1\nU 3\nD 2\nR 1\nL 2\nR 1\nD 5\nU 3\nL 3\nU 4\nL 3\nD 2\nL 4\nU 2\nD 4\nU 2\nL 4\nR 1\nD 1\nU 1\nD 1\nU 2\nL 3\nR 1\nU 1\nL 2\nD 2\nR 3\nL 3\nR 1\nU 4\nR 2\nL 1\nD 2\nR 4\nL 5\nD 1\nR 2\nU 1\nL 2\nU 5\nD 3\nU 3\nD 1\nU 2\nD 4\nU 1\nL 2\nR 4\nL 5\nU 4\nD 3\nU 1\nD 2\nL 5\nU 5\nD 2\nL 2\nD 2\nU 4\nR 5\nD 1\nR 2\nL 2\nD 1\nR 2\nL 6\nU 3\nR 1\nD 6\nU 1\nD 4\nR 6\nU 3\nL 6\nU 3\nL 2\nU 3\nR 4\nU 5\nD 4\nR 5\nD 6\nU 1\nR 2\nU 1\nD 5\nR 4\nL 3\nD 4\nU 5\nL 5\nU 5\nL 4\nU 1\nD 6\nR 6\nL 4\nR 4\nD 1\nR 1\nL 5\nU 2\nD 3\nU 5\nD 6\nR 3\nL 6\nU 6\nD 4\nU 4\nD 3\nL 5\nD 6\nU 5\nR 3\nU 6\nD 4\nU 1\nR 2\nD 1\nL 3\nR 2\nU 5\nR 2\nL 6\nD 3\nR 2\nU 2\nR 5\nU 6\nR 4\nU 4\nD 4\nU 2\nR 6\nU 3\nL 3\nD 1\nR 2\nL 5\nD 5\nU 6\nD 5\nR 1\nD 1\nU 6\nD 4\nR 3\nU 6\nL 5\nU 5\nD 1\nU 6\nL 2\nD 2\nU 6\nL 5\nU 6\nL 2\nD 1\nR 5\nU 4\nR 6\nD 5\nR 1\nL 1\nU 2\nL 1\nR 1\nD 6\nL 6\nR 4\nD 4\nL 3\nD 1\nU 2\nL 3\nR 4\nD 6\nU 5\nD 3\nL 4\nR 6\nL 7\nU 4\nR 4\nL 6\nU 1\nR 4\nL 5\nD 2\nL 2\nD 3\nR 6\nU 6\nR 5\nD 2\nU 5\nL 7\nR 6\nL 3\nU 7\nR 4\nD 7\nR 5\nU 7\nR 6\nU 5\nR 7\nU 4\nD 4\nL 1\nR 2\nD 5\nL 3\nR 6\nD 3\nR 6\nD 4\nU 4\nL 6\nU 5\nD 3\nU 5\nD 5\nR 2\nL 4\nD 1\nL 4\nU 4\nL 4\nD 2\nR 7\nD 2\nL 6\nR 5\nL 7\nU 5\nR 4\nL 2\nR 1\nU 7\nR 6\nD 7\nL 6\nD 2\nL 5\nD 1\nU 6\nL 2\nD 4\nL 6\nD 1\nL 5\nR 6\nD 6\nL 4\nD 4\nR 5\nL 4\nD 2\nU 5\nD 2\nL 7\nR 1\nD 5\nR 3\nL 4\nD 7\nU 2\nR 3\nU 1\nL 1\nU 3\nD 7\nL 7\nD 2\nU 5\nD 2\nU 3\nL 3\nU 5\nL 1\nU 4\nL 1\nD 2\nU 1\nD 7\nR 5\nU 4\nL 2\nD 7\nU 6\nD 3\nU 8\nL 6\nR 1\nU 7\nD 8\nR 5\nD 2\nL 3\nD 7\nU 3\nD 2\nR 4\nL 1\nU 6\nD 4\nR 2\nU 7\nL 6\nU 3\nL 6\nD 3\nL 5\nU 6\nL 2\nR 1\nD 7\nR 7\nL 1\nD 6\nU 2\nR 8\nD 3\nR 1\nU 7\nD 4\nU 8\nR 6\nL 1\nD 4\nU 3\nL 8\nD 1\nR 4\nD 2\nU 8\nD 6\nL 2\nU 6\nR 2\nU 8\nD 4\nR 8\nD 5\nU 4\nD 7\nU 8\nL 2\nU 6\nD 5\nR 5\nD 4\nR 8\nU 5\nR 4\nL 8\nU 7\nR 5\nD 3\nR 7\nL 6\nD 1\nL 5\nU 7\nR 5\nU 4\nR 3\nL 5\nU 1\nR 7\nD 1\nR 8\nU 2\nL 5\nU 5\nR 1\nL 7\nU 2\nD 3\nU 3\nR 8\nU 6\nR 2\nU 1\nR 4\nD 5\nU 5\nR 1\nU 2\nL 1\nR 3\nL 4\nR 4\nD 6\nR 5\nU 3\nD 4\nR 7\nL 7\nD 7\nR 4\nU 5\nR 4\nL 6\nD 1\nR 6\nL 8\nU 7\nL 6\nR 1\nD 2\nU 6\nR 5\nU 8\nD 8\nU 9\nD 3\nR 8\nL 9\nD 7\nR 2\nL 6\nU 8\nR 4\nD 8\nU 8\nL 2\nR 4\nU 1\nD 2\nU 3\nR 8\nL 2\nR 6\nL 4\nU 9\nL 1\nR 1\nD 6\nR 1\nD 2\nR 8\nL 5\nD 8\nU 2\nR 3\nD 7\nL 1\nU 6\nD 4\nU 2\nR 3\nD 5\nU 1\nL 3\nD 5\nU 5\nD 8\nU 4\nL 2\nR 5\nU 7\nR 5\nU 8\nR 6\nL 1\nR 7\nD 3\nR 3\nD 5\nU 1\nR 2\nL 3\nU 8\nR 4\nL 5\nU 7\nD 3\nU 4\nL 9\nU 4\nD 6\nR 9\nL 6\nD 6\nR 6\nU 1\nR 2\nD 2\nU 9\nL 1\nD 6\nU 3\nD 5\nU 7\nR 9\nL 2\nU 8\nL 3\nU 5\nR 8\nD 6\nR 4\nL 8\nR 8\nL 5\nR 7\nU 7\nL 9\nR 4\nU 3\nR 7\nU 2\nL 2\nD 5\nU 10\nD 4\nR 4\nU 6\nL 3\nR 5\nU 7\nD 10\nU 7\nD 8\nL 1\nR 6\nL 3\nU 8\nL 7\nR 5\nL 9\nD 5\nR 3\nL 6\nR 8\nD 4\nL 4\nU 5\nL 7\nR 1\nU 8\nR 3\nD 3\nR 2\nD 3\nU 3\nR 3\nU 6\nR 5\nU 8\nL 7\nU 2\nD 2\nL 3\nD 9\nL 2\nD 3\nR 5\nL 6\nU 1\nL 7\nU 10\nR 2\nL 5\nD 5\nU 1\nL 2\nR 6\nD 8\nU 7\nL 4\nR 6\nU 8\nD 1\nR 5\nD 8\nU 3\nR 1\nD 6\nR 4\nL 10\nD 4\nU 4\nD 4\nR 3\nU 2\nD 1\nR 4\nL 1\nD 4\nL 7\nD 2\nU 7\nD 8\nL 10\nR 7\nL 9\nD 2\nR 4\nD 7\nL 9\nU 9\nR 1\nL 4\nU 1\nD 8\nU 2\nL 7\nR 1\nL 3\nU 3\nD 3\nL 4\nU 10\nL 4\nU 1\nR 4\nD 5\nL 4\nR 5\nD 8\nL 6\nD 1\nU 3\nR 8\nL 7\nD 9\nL 4\nD 4\nU 6\nD 6\nL 4\nR 7\nU 8\nD 10\nU 7\nL 5\nR 2\nL 8\nD 7\nU 8\nL 6\nR 5\nL 7\nR 11\nU 1\nR 4\nD 6\nL 4\nR 5\nL 4\nD 3\nL 7\nR 3\nD 2\nU 10\nR 6\nD 3\nU 4\nR 1\nD 11\nU 3\nD 2\nU 10\nL 7\nU 1\nR 3\nL 1\nR 1\nD 5\nU 3\nL 5\nD 3\nU 8\nL 2\nD 7\nU 5\nL 3\nD 6\nL 6\nD 9\nU 1\nR 11\nU 11\nR 6\nU 1\nR 9\nU 6\nR 6\nD 9\nL 6\nD 8\nU 6\nD 10\nR 10\nD 11\nR 2\nL 10\nD 4\nU 6\nD 6\nR 9\nD 6\nU 7\nL 2\nD 9\nU 10\nD 5\nL 11\nR 4\nU 5\nL 10\nR 5\nU 9\nR 3\nU 6\nL 8\nR 8\nD 6\nL 5\nU 4\nD 8\nL 11\nD 5\nR 8\nU 8\nR 1\nU 3\nR 5\nU 4\nD 8\nR 8\nU 2\nR 2\nU 6\nL 2\nD 1\nR 1\nD 5\nR 11\nU 7\nR 10\nL 4\nU 10\nR 3\nU 1\nD 11\nR 9\nU 6\nL 3\nD 12\nL 7\nD 4\nL 5\nU 7\nD 11\nR 11\nU 1\nR 2\nU 6\nR 7\nL 9\nU 5\nL 7\nD 9\nR 12\nD 12\nR 8\nU 8\nD 6\nR 10\nD 3\nU 1\nD 1\nL 10\nR 4\nD 5\nR 10\nD 3\nU 7\nD 6\nL 9\nR 10\nD 3\nR 9\nL 3\nD 5\nL 5\nR 1\nD 10\nU 11\nR 1\nD 1\nR 11\nD 4\nL 11\nR 6\nL 2\nR 4\nD 4\nR 9\nD 9\nL 3\nU 2\nR 5\nL 1\nR 3\nD 12\nL 9\nD 8\nU 7\nD 2\nR 4\nL 1\nD 9\nU 11\nD 9\nL 6\nD 4\nU 6\nR 8\nD 1\nL 4\nU 8\nD 4\nL 2\nR 1\nL 11\nR 4\nD 5\nL 9\nR 11\nU 1\nD 12\nU 9\nL 8\nR 5\nD 1\nR 9\nL 3\nR 5\nD 3\nU 4\nR 11\nL 4\nR 3\nU 12\nD 1\nU 8\nR 5\nU 10\nL 9\nR 10\nD 4\nL 9\nU 6\nD 4\nU 7\nL 12\nD 7\nR 9\nU 6\nL 4\nD 1\nU 11\nL 3\nD 4\nU 9\nD 10\nU 5\nD 13\nR 9\nD 8\nL 7\nD 11\nU 12\nR 2\nL 12\nR 10\nD 13\nL 6\nU 8\nR 9\nL 7\nR 7\nU 2\nL 5\nR 4\nD 10\nL 4\nD 5\nU 4\nD 13\nR 4\nD 4\nU 9\nR 3\nD 12\nR 2\nU 2\nR 9\nD 10\nU 3\nL 12\nU 9\nL 11\nU 6\nL 3\nU 7\nR 10\nU 13\nL 12\nD 1\nR 6\nD 13\nU 5\nR 2\nL 11\nD 7\nU 1\nD 9\nL 11\nD 6\nR 3\nD 2\nU 5\nL 1\nR 9\nU 7\nD 11\nU 6\nR 2\nU 6\nL 13\nR 4\nL 11\nD 10\nL 1\nU 7\nL 10\nD 1\nU 12\nL 2\nU 5\nD 10\nU 12\nR 13\nU 12\nR 10\nD 13\nR 8\nU 6\nD 11\nL 4\nR 7\nD 6\nL 12\nU 6\nL 8\nR 7\nD 5\nR 7\nD 8\nL 14\nR 10\nU 14\nL 6\nU 14\nR 11\nU 5\nR 1\nL 8\nU 7\nL 3\nD 13\nR 11\nL 14\nU 14\nR 5\nD 9\nR 11\nU 12\nR 13\nL 12\nD 6\nU 7\nL 3\nU 10\nR 13\nD 13\nU 10\nR 12\nD 12\nU 4\nD 12\nU 9\nL 9\nU 13\nD 9\nR 13\nD 13\nR 12\nD 7\nR 4\nL 2\nR 12\nL 8\nU 8\nL 1\nU 4\nR 9\nU 9\nD 13\nR 9\nL 10\nR 12\nD 14\nR 6\nD 9\nL 11\nR 4\nD 6\nR 3\nL 11\nU 9\nR 3\nU 6\nL 2\nD 3\nL 9\nD 9\nR 5\nD 13\nU 14\nR 12\nU 8\nL 12\nD 4\nL 8\nR 13\nD 13\nL 11\nR 6\nU 10\nD 7\nU 12\nR 7\nL 1\nU 2\nR 4\nL 11\nR 6\nL 11\nR 14\nD 7\nR 6\nL 13\nR 8\nD 8\nR 1\nL 3\nR 10\nU 3\nL 5\nU 5\nD 3\nL 4\nD 7\nR 5\nL 6\nU 14\nD 11\nL 13\nR 9\nD 12\nL 2\nD 11\nU 6\nD 14\nR 7\nU 5\nR 5\nU 2\nL 4\nU 12\nD 12\nL 3\nR 6\nL 14\nR 10\nD 13\nU 5\nR 7\nL 6\nU 2\nD 1\nU 5\nR 11\nU 8\nR 2\nL 5\nR 8\nU 5\nR 15\nD 14\nL 7\nR 4\nU 14\nL 13\nU 8\nL 8\nU 4\nD 5\nL 7\nR 9\nU 5\nL 11\nU 3\nR 2\nU 1\nD 13\nL 4\nU 5\nL 13\nR 8\nU 13\nD 15\nL 5\nU 9\nD 1\nL 7\nU 5\nR 9\nL 9\nD 14\nR 7\nU 14\nR 8\nD 1\nR 14\nD 13\nL 15\nD 6\nU 10\nL 10\nR 8\nU 13\nR 12\nU 4\nR 13\nD 2\nR 13\nU 13\nL 6\nD 5\nR 10\nL 5\nR 3\nD 3\nR 15\nD 9\nU 2\nR 7\nU 12\nD 3\nR 8\nD 12\nU 1\nL 13\nD 15\nL 9\nR 15\nU 10\nR 16\nD 5\nR 8\nL 1\nR 7\nD 11\nU 4\nR 10\nL 4\nD 8\nU 8\nD 12\nU 12\nL 7\nU 2\nR 13\nL 14\nD 7\nU 15\nR 2\nD 15\nL 15\nD 10\nU 1\nR 5\nL 9\nR 6\nL 15\nD 6\nU 11\nL 14\nU 10\nR 2\nL 9\nR 4\nU 12\nD 5\nR 7\nU 15\nD 12\nR 1\nU 8\nL 14\nU 8\nD 13\nU 1\nR 3\nU 10\nL 12\nD 9\nL 7\nD 5\nR 7\nD 15\nL 13\nU 2\nD 11\nL 13\nU 14\nL 14\nD 10\nU 9\nL 13\nU 15\nD 5\nU 14\nR 8\nU 8\nR 4\nL 10\nU 13\nR 10\nU 5\nR 12\nD 1\nL 10\nR 11\nD 9\nL 14\nU 15\nD 16\nR 10\nD 14\nU 8\nD 5\nU 7\nD 1\nR 12\nD 10\nL 10\nD 15\nU 8\nL 5\nR 16\nU 13\nR 3\nL 8\nD 11\nL 15\nU 2\nR 12\nL 15\nD 8\nU 14\nL 13\nU 10\nL 14\nR 11\nU 4\nL 1\nR 1\nU 10\nD 8\nU 4\nD 5\nL 8\nR 12\nU 6\nD 16\nL 11\nU 14\nL 5\nD 4\nR 11\nD 5\nR 5\nL 3\nD 5\nR 4\nD 10\nU 13\nL 1\nR 1\nD 5\nR 2\nU 10\nR 12\nL 15\nR 3\nL 13\nU 11\nD 16\nR 6\nD 10\nL 16\nR 9\nU 12\nD 6\nR 16\nU 6\nL 9\nD 10\nL 2\nU 11\nL 5\nD 14\nR 17\nD 9\nR 15\nU 17\nR 12\nL 17\nR 9\nL 14\nR 6\nD 3\nU 12\nD 17\nU 16\nD 7\nU 11\nD 3\nL 5\nU 13\nR 6\nU 16\nD 12\nR 6\nU 1\nL 4\nR 15\nL 13\nD 16\nL 17\nU 1\nD 4\nR 12\nU 1\nL 5\nU 13\nL 12\nU 11\nD 5\nR 15\nL 4\nR 7\nU 6\nD 17\nR 14\nL 16\nD 15\nR 15\nU 7\nR 5\nU 10\nR 5\nL 3\nU 9\nL 6\nU 17\nL 15\nR 14\nD 6\nU 13\nR 11\nL 12\nR 16\nU 4\nD 2\nU 4\nR 17\nD 9\nR 9\nD 7\nR 12\nL 9\nR 16\nD 14\nU 1\nR 12\nU 11\nD 10\nL 10\nD 12\nR 12\nU 7\nD 12\nU 9\nD 15\nR 7\nU 1\nR 4\nU 11\nR 18\nL 6\nD 12\nU 14\nR 12\nD 2\nR 6\nD 9\nL 4\nU 11\nL 13\nD 14\nL 5\nR 13\nL 12\nU 17\nL 11\nR 8\nL 3\nD 6\nU 4\nR 16\nD 14\nU 10\nR 12\nL 7\nR 6\nL 9\nD 13\nU 6\nD 15\nR 17\nL 1\nD 7\nL 6\nR 16\nD 13\nL 5\nR 16\nU 5\nD 2\nR 17\nL 17\nU 18\nL 10\nD 14\nU 9\nD 9\nU 1\nL 8\nR 5\nD 5\nU 12\nD 1\nL 8\nU 13\nR 6\nD 12\nR 6\nD 2\nL 8\nU 11\nL 14\nR 6\nL 17\nD 1\nR 1\nD 3\nU 11\nD 14\nR 8\nU 2\nR 1\nU 17\nL 14\nU 15\nD 17\nL 15\nU 11\nL 7\nD 2\nL 16\nD 7\nL 14\nU 14\nD 5\nR 4\nU 7\nD 4\nR 12\nL 11\nD 4\nU 19\nL 16\nR 13\nL 1\nR 13\nU 12\nR 12\nU 10\nL 7\nR 9\nU 15\nD 3\nR 12\nU 8\nL 5\nR 14\nU 4\nD 2\nL 2\nU 8\nL 6\nD 10\nL 18\nU 4\nD 8\nR 15\nD 11\nR 14\nD 16\nR 14\nD 4\nR 9\nU 14\nL 2\nR 2\nU 16\nR 2\nU 18\nD 6\nR 16\nU 2\nL 7\nU 6\nD 4\nL 9\nU 18\nR 10\nL 17\nR 12\nU 7\nL 10\nU 5\nD 15\nU 6\nL 14\nU 17\nR 11\nU 2\nR 6\nL 5\nD 2\nU 15\nD 6\nU 4\nR 16\nD 7\nL 15\nR 9\nD 6\nR 15\nL 1\nR 2\nL 5\nU 19\nL 6\nU 15\nR 16\nL 12\nD 9\nU 15\nD 7\nL 10\nU 9\nD 15\nL 13\nR 1\nL 17\nR 14\nD 14\nU 19\nD 4\nU 15\nD 1\nR 12\nL 1\nD 14\nU 13\nL 4\nR 15\nL 1\nR 17\nL 9\nD 10\nL 9\nU 2\nL 17\nU 4\nD 7\nR 3")

(def moves->coords
  {:right [1 0]
   :left  [-1 0]
   :up    [0 1]
   :down  [0 -1]})

(defn towards-one
  [n]
  (cond
    (zero? n) 0
    (pos? n) 1
    (neg? n) -1))

(defn parse-moves
  [input]
  (reduce (fn [moves s]
            (let [[m n] (str/split s #"\s")]
              (into moves
                    (repeat (parse-long n)
                            (moves->coords
                              ({"R" :right
                                "U" :up
                                "L" :left
                                "D" :down} m))))))
          []
          (str/split-lines input)))

(defn minus
  [{:keys [H T]}]
  (coords/minus H T))

(def all-directions
  (set (for [x (range -1 2)
             y (range -1 2)]
         [x y])))

(defn adjacent?
  [{:keys [H T]}]
  (let [dist (coords/distance H T)]
    (< dist 1.42))
  #_(some #{T} (map #(coords/add H %) all-directions)))

(defn move-tail
  [world]
  (let [step (mapv towards-one (minus world))]
    (update world :T coords/add step)))

(defn move-head
  [world move]
  (update world :H coords/add move))

(defn step
  [world move]
  (let [world' (move-head world move)]
    (if (adjacent? world')
      world'
      (move-tail world'))))


(defn solve-part-1
  [input]
  (let [moves (reductions
                step
                {:H [0 0]
                 :T [0 0]}
                (parse-moves input))]
    (count (into #{} (map :T) moves))))

(defn adjacent-2?
  [H T]
  (< (coords/distance H T) 1.42))

(defn step-towards-head
  [H T]
  (mapv towards-one (coords/minus H T)))

(defn draw-moves
  [moves who]
  (coords/draw-regular-axis {who "#"} "." (zipmap (map who moves) (repeat who))))

(defn step-2
  [world-2 move]
  (loop [world (update world-2 0 coords/add move)
         i 0]
    (if (< i 9)
      (let [tail-index (inc i)
            H (get world i)
            T (get world tail-index)]
        (recur (if (adjacent-2? H T)
                 world
                 (update world tail-index coords/add (step-towards-head H T)))
               tail-index))
      world)))

(defn solve-part-2
  [input]
  (let [moves (reductions
                step-2
                (vec (take 10 (repeat [0 0])))
                (parse-moves input))]
    (count (into #{} (map #(get % 9) moves)))))

(def example-2 "R 5\nU 8\nL 8\nD 3\nR 17\nD 10\nL 25\nU 20")

(comment
  (solve-part-2 example-2)
  (solve-part-2 input)
  (time (assert (= 6175 (solve-part-1 input))))
  (time (assert (= 2578 (solve-part-2 input))))
  (assert (= 13 (solve-part-1 example)))
  (parse-moves example)
  )