(ns advent-of-code-2022.day-5-Supply-Stacks
  (:require [clojure.string :as str]))

(def stacks "[P]     [C]         [M]            \n[D]     [P] [B]     [V] [S]        \n[Q] [V] [R] [V]     [G] [B]        \n[R] [W] [G] [J]     [T] [M]     [V]\n[V] [Q] [Q] [F] [C] [N] [V]     [W]\n[B] [Z] [Z] [H] [L] [P] [L] [J] [N]\n[H] [D] [L] [D] [W] [R] [R] [P] [C]\n[F] [L] [H] [R] [Z] [J] [J] [D] [D]\n 1   2   3   4   5   6   7   8   9 ")
(def moves "move 4 from 9 to 1\nmove 6 from 3 to 1\nmove 7 from 4 to 1\nmove 2 from 8 to 5\nmove 1 from 9 to 7\nmove 1 from 8 to 5\nmove 3 from 6 to 4\nmove 6 from 1 to 5\nmove 14 from 1 to 2\nmove 1 from 6 to 1\nmove 2 from 6 to 2\nmove 9 from 5 to 9\nmove 2 from 4 to 5\nmove 2 from 5 to 3\nmove 6 from 9 to 6\nmove 4 from 1 to 2\nmove 2 from 1 to 2\nmove 5 from 6 to 1\nmove 1 from 4 to 9\nmove 4 from 9 to 4\nmove 2 from 3 to 7\nmove 2 from 4 to 9\nmove 2 from 9 to 6\nmove 5 from 2 to 9\nmove 1 from 4 to 9\nmove 1 from 4 to 3\nmove 5 from 9 to 8\nmove 1 from 6 to 5\nmove 3 from 7 to 5\nmove 2 from 1 to 6\nmove 5 from 6 to 8\nmove 1 from 9 to 4\nmove 1 from 6 to 5\nmove 9 from 2 to 7\nmove 1 from 2 to 3\nmove 1 from 4 to 6\nmove 8 from 5 to 4\nmove 1 from 6 to 1\nmove 2 from 8 to 6\nmove 1 from 6 to 4\nmove 7 from 4 to 6\nmove 1 from 3 to 1\nmove 1 from 3 to 4\nmove 3 from 4 to 1\nmove 2 from 3 to 4\nmove 2 from 4 to 5\nmove 3 from 5 to 7\nmove 7 from 8 to 2\nmove 5 from 1 to 2\nmove 12 from 7 to 6\nmove 2 from 1 to 9\nmove 2 from 9 to 1\nmove 1 from 7 to 5\nmove 6 from 2 to 3\nmove 5 from 2 to 6\nmove 6 from 2 to 6\nmove 4 from 3 to 1\nmove 3 from 2 to 1\nmove 1 from 5 to 4\nmove 7 from 1 to 2\nmove 1 from 4 to 8\nmove 7 from 2 to 9\nmove 5 from 2 to 8\nmove 2 from 6 to 8\nmove 21 from 6 to 9\nmove 8 from 9 to 1\nmove 2 from 6 to 1\nmove 3 from 8 to 7\nmove 6 from 6 to 4\nmove 7 from 1 to 8\nmove 1 from 9 to 1\nmove 7 from 7 to 3\nmove 1 from 7 to 4\nmove 1 from 7 to 4\nmove 7 from 8 to 1\nmove 5 from 4 to 8\nmove 10 from 1 to 2\nmove 3 from 1 to 4\nmove 3 from 2 to 9\nmove 1 from 4 to 5\nmove 3 from 3 to 6\nmove 1 from 6 to 4\nmove 1 from 6 to 7\nmove 1 from 7 to 8\nmove 7 from 2 to 4\nmove 10 from 9 to 1\nmove 10 from 4 to 5\nmove 2 from 5 to 2\nmove 2 from 2 to 1\nmove 11 from 8 to 9\nmove 7 from 1 to 4\nmove 1 from 6 to 1\nmove 1 from 8 to 3\nmove 1 from 4 to 6\nmove 6 from 4 to 5\nmove 1 from 5 to 7\nmove 1 from 6 to 8\nmove 6 from 1 to 6\nmove 19 from 9 to 2\nmove 1 from 1 to 8\nmove 1 from 4 to 7\nmove 9 from 2 to 6\nmove 1 from 9 to 2\nmove 2 from 8 to 1\nmove 1 from 1 to 9\nmove 7 from 3 to 6\nmove 3 from 9 to 2\nmove 5 from 2 to 6\nmove 1 from 9 to 3\nmove 15 from 6 to 7\nmove 6 from 6 to 7\nmove 1 from 1 to 9\nmove 5 from 6 to 2\nmove 1 from 6 to 1\nmove 6 from 5 to 8\nmove 1 from 3 to 4\nmove 1 from 9 to 7\nmove 6 from 8 to 1\nmove 3 from 4 to 6\nmove 1 from 6 to 1\nmove 3 from 5 to 2\nmove 1 from 5 to 7\nmove 5 from 1 to 5\nmove 2 from 6 to 9\nmove 2 from 9 to 2\nmove 7 from 5 to 1\nmove 1 from 5 to 7\nmove 1 from 5 to 9\nmove 20 from 7 to 1\nmove 23 from 1 to 7\nmove 1 from 1 to 2\nmove 4 from 7 to 9\nmove 4 from 9 to 8\nmove 1 from 9 to 2\nmove 16 from 7 to 6\nmove 4 from 1 to 5\nmove 9 from 7 to 6\nmove 11 from 2 to 6\nmove 1 from 1 to 9\nmove 1 from 1 to 7\nmove 1 from 8 to 2\nmove 1 from 9 to 7\nmove 4 from 5 to 2\nmove 3 from 8 to 3\nmove 2 from 2 to 4\nmove 2 from 7 to 4\nmove 4 from 4 to 9\nmove 28 from 6 to 9\nmove 5 from 2 to 7\nmove 8 from 6 to 5\nmove 6 from 2 to 6\nmove 2 from 7 to 3\nmove 5 from 5 to 7\nmove 1 from 5 to 9\nmove 14 from 9 to 4\nmove 18 from 9 to 8\nmove 5 from 6 to 4\nmove 6 from 7 to 8\nmove 1 from 2 to 6\nmove 19 from 4 to 7\nmove 1 from 2 to 5\nmove 1 from 9 to 3\nmove 2 from 5 to 2\nmove 14 from 7 to 3\nmove 1 from 5 to 3\nmove 12 from 8 to 6\nmove 6 from 6 to 5\nmove 4 from 5 to 4\nmove 21 from 3 to 4\nmove 10 from 8 to 3\nmove 2 from 3 to 2\nmove 7 from 4 to 6\nmove 2 from 8 to 1\nmove 2 from 2 to 3\nmove 5 from 7 to 2\nmove 2 from 1 to 4\nmove 3 from 3 to 7\nmove 2 from 5 to 7\nmove 2 from 2 to 7\nmove 2 from 2 to 3\nmove 7 from 4 to 1\nmove 3 from 1 to 4\nmove 3 from 2 to 5\nmove 2 from 1 to 5\nmove 7 from 4 to 3\nmove 15 from 6 to 2\nmove 1 from 1 to 4\nmove 1 from 5 to 1\nmove 14 from 3 to 1\nmove 9 from 4 to 1\nmove 5 from 7 to 1\nmove 1 from 3 to 5\nmove 1 from 4 to 2\nmove 20 from 1 to 2\nmove 17 from 2 to 5\nmove 1 from 3 to 7\nmove 5 from 7 to 3\nmove 6 from 5 to 1\nmove 3 from 3 to 2\nmove 10 from 1 to 9\nmove 3 from 5 to 6\nmove 12 from 5 to 6\nmove 1 from 5 to 1\nmove 15 from 6 to 5\nmove 13 from 5 to 3\nmove 1 from 5 to 1\nmove 10 from 3 to 2\nmove 3 from 3 to 2\nmove 1 from 5 to 3\nmove 2 from 3 to 6\nmove 1 from 3 to 4\nmove 2 from 6 to 4\nmove 3 from 4 to 2\nmove 8 from 9 to 4\nmove 8 from 4 to 8\nmove 7 from 2 to 1\nmove 5 from 8 to 7\nmove 2 from 2 to 3\nmove 13 from 1 to 2\nmove 2 from 3 to 8\nmove 2 from 9 to 7\nmove 3 from 8 to 1\nmove 2 from 1 to 2\nmove 2 from 8 to 4\nmove 6 from 7 to 2\nmove 3 from 1 to 8\nmove 1 from 7 to 5\nmove 24 from 2 to 1\nmove 2 from 8 to 5\nmove 15 from 1 to 4\nmove 1 from 5 to 8\nmove 9 from 1 to 4\nmove 2 from 8 to 5\nmove 26 from 2 to 4\nmove 1 from 5 to 8\nmove 1 from 5 to 8\nmove 50 from 4 to 1\nmove 1 from 8 to 9\nmove 1 from 4 to 6\nmove 1 from 4 to 9\nmove 22 from 1 to 5\nmove 1 from 6 to 2\nmove 1 from 5 to 8\nmove 1 from 2 to 4\nmove 1 from 8 to 1\nmove 28 from 1 to 3\nmove 2 from 9 to 4\nmove 21 from 5 to 8\nmove 1 from 1 to 8\nmove 1 from 5 to 8\nmove 1 from 5 to 7\nmove 3 from 4 to 8\nmove 1 from 7 to 9\nmove 1 from 9 to 7\nmove 20 from 8 to 4\nmove 2 from 8 to 1\nmove 1 from 7 to 6\nmove 2 from 1 to 4\nmove 27 from 3 to 1\nmove 4 from 8 to 4\nmove 1 from 6 to 9\nmove 19 from 4 to 2\nmove 5 from 2 to 5\nmove 1 from 4 to 1\nmove 1 from 9 to 2\nmove 17 from 1 to 9\nmove 1 from 3 to 8\nmove 15 from 9 to 2\nmove 2 from 4 to 8\nmove 2 from 5 to 8\nmove 2 from 5 to 9\nmove 3 from 9 to 8\nmove 9 from 1 to 2\nmove 2 from 1 to 3\nmove 4 from 4 to 5\nmove 2 from 5 to 7\nmove 1 from 8 to 5\nmove 2 from 3 to 8\nmove 4 from 5 to 2\nmove 1 from 9 to 6\nmove 5 from 8 to 5\nmove 1 from 7 to 9\nmove 29 from 2 to 3\nmove 1 from 8 to 6\nmove 1 from 9 to 7\nmove 2 from 2 to 8\nmove 2 from 5 to 2\nmove 2 from 7 to 5\nmove 4 from 5 to 9\nmove 1 from 5 to 9\nmove 10 from 3 to 4\nmove 10 from 4 to 7\nmove 1 from 3 to 4\nmove 5 from 2 to 9\nmove 5 from 8 to 6\nmove 1 from 6 to 5\nmove 2 from 6 to 3\nmove 4 from 6 to 7\nmove 1 from 5 to 2\nmove 2 from 2 to 7\nmove 5 from 7 to 8\nmove 8 from 7 to 2\nmove 6 from 8 to 7\nmove 14 from 2 to 5\nmove 3 from 7 to 3\nmove 1 from 4 to 7\nmove 2 from 7 to 2\nmove 3 from 2 to 8\nmove 3 from 8 to 5\nmove 8 from 9 to 1\nmove 3 from 7 to 2\nmove 2 from 7 to 4\nmove 17 from 3 to 6\nmove 8 from 1 to 6\nmove 16 from 5 to 2\nmove 1 from 5 to 2\nmove 1 from 3 to 1\nmove 21 from 6 to 7\nmove 1 from 4 to 8\nmove 7 from 7 to 8\nmove 1 from 1 to 3\nmove 11 from 7 to 2\nmove 7 from 2 to 6\nmove 8 from 8 to 5\nmove 2 from 7 to 4\nmove 4 from 5 to 6\nmove 8 from 2 to 8\nmove 17 from 2 to 3\nmove 4 from 5 to 3\nmove 7 from 6 to 9\nmove 2 from 6 to 9\nmove 1 from 4 to 1\nmove 1 from 4 to 2\nmove 3 from 6 to 2\nmove 1 from 6 to 8\nmove 1 from 4 to 1\nmove 1 from 7 to 5\nmove 10 from 9 to 2\nmove 1 from 5 to 6\nmove 1 from 8 to 2\nmove 1 from 1 to 4\nmove 12 from 3 to 4\nmove 1 from 6 to 2\nmove 2 from 8 to 6\nmove 1 from 1 to 2\nmove 1 from 9 to 8\nmove 2 from 8 to 7\nmove 6 from 3 to 2\nmove 1 from 3 to 5\nmove 8 from 4 to 9\nmove 22 from 2 to 9\nmove 7 from 3 to 5\nmove 3 from 8 to 2\nmove 2 from 7 to 8\nmove 3 from 6 to 9\nmove 1 from 2 to 9\nmove 1 from 6 to 2\nmove 4 from 8 to 5\nmove 5 from 5 to 9\nmove 1 from 3 to 6\nmove 1 from 5 to 6\nmove 2 from 4 to 1\nmove 2 from 2 to 4\nmove 4 from 4 to 6\nmove 1 from 1 to 5\nmove 5 from 6 to 3\nmove 35 from 9 to 1\nmove 4 from 9 to 1\nmove 1 from 4 to 7\nmove 3 from 3 to 7\nmove 37 from 1 to 7\nmove 2 from 2 to 3\nmove 3 from 3 to 7\nmove 1 from 5 to 8\nmove 2 from 1 to 8\nmove 2 from 5 to 2\nmove 1 from 6 to 9\nmove 16 from 7 to 1\nmove 5 from 1 to 5\nmove 3 from 8 to 2\nmove 10 from 7 to 9\nmove 6 from 7 to 9\nmove 3 from 2 to 1\nmove 4 from 5 to 3\nmove 2 from 1 to 2\nmove 5 from 7 to 9\nmove 5 from 7 to 9\nmove 5 from 5 to 3\nmove 8 from 3 to 7\nmove 6 from 9 to 4\nmove 8 from 7 to 3\nmove 2 from 3 to 6\nmove 1 from 6 to 7\nmove 1 from 6 to 7\nmove 5 from 4 to 9\nmove 3 from 7 to 1\nmove 2 from 2 to 8\nmove 1 from 8 to 6\nmove 6 from 1 to 8\nmove 1 from 7 to 9\nmove 1 from 3 to 9\nmove 4 from 3 to 2\nmove 8 from 1 to 6\nmove 1 from 3 to 9\nmove 5 from 8 to 4\nmove 2 from 3 to 1\nmove 1 from 8 to 2\nmove 4 from 9 to 1\nmove 2 from 1 to 5\nmove 1 from 8 to 5\nmove 11 from 9 to 5\nmove 1 from 2 to 8\nmove 10 from 5 to 4\nmove 1 from 1 to 9\nmove 3 from 5 to 4\nmove 5 from 2 to 3\nmove 1 from 5 to 1\nmove 9 from 9 to 4\nmove 1 from 6 to 7\nmove 1 from 3 to 9\nmove 4 from 3 to 1\nmove 1 from 2 to 4\nmove 1 from 1 to 4\nmove 1 from 4 to 7\nmove 5 from 1 to 3\nmove 1 from 3 to 2\nmove 1 from 8 to 3\nmove 3 from 9 to 5\nmove 1 from 2 to 9\nmove 4 from 1 to 4\nmove 1 from 7 to 4\nmove 2 from 5 to 8\nmove 1 from 7 to 6\nmove 4 from 3 to 1\nmove 1 from 5 to 8\nmove 1 from 3 to 4\nmove 22 from 4 to 1\nmove 11 from 1 to 9\nmove 2 from 1 to 4\nmove 11 from 1 to 6\nmove 8 from 6 to 7\nmove 1 from 8 to 7\nmove 7 from 9 to 2\nmove 6 from 7 to 6\nmove 2 from 4 to 9\nmove 2 from 7 to 1\nmove 14 from 6 to 3\nmove 2 from 3 to 1\nmove 3 from 6 to 7\nmove 6 from 1 to 3\nmove 8 from 9 to 6\nmove 7 from 4 to 6\nmove 7 from 6 to 8\nmove 1 from 9 to 1\nmove 2 from 9 to 8\nmove 4 from 3 to 4\nmove 1 from 8 to 4\nmove 1 from 4 to 3\nmove 6 from 3 to 7\nmove 7 from 2 to 5\nmove 8 from 4 to 6\nmove 1 from 7 to 2\nmove 1 from 5 to 7\nmove 6 from 7 to 3\nmove 1 from 7 to 1\nmove 8 from 8 to 4\nmove 8 from 4 to 2\nmove 3 from 7 to 3\nmove 6 from 5 to 6\nmove 15 from 3 to 1\nmove 21 from 6 to 1\nmove 4 from 2 to 6\nmove 5 from 6 to 5\nmove 1 from 2 to 6\nmove 1 from 4 to 5\nmove 1 from 4 to 3\nmove 1 from 8 to 6\nmove 4 from 5 to 7\nmove 18 from 1 to 4\nmove 2 from 5 to 7\nmove 6 from 7 to 6\nmove 1 from 3 to 2\nmove 6 from 1 to 2\nmove 3 from 3 to 9\nmove 3 from 9 to 4\nmove 1 from 8 to 3\nmove 1 from 6 to 5\nmove 6 from 2 to 5\nmove 1 from 5 to 9\nmove 1 from 3 to 5\nmove 2 from 6 to 8\nmove 2 from 1 to 4\nmove 5 from 4 to 6\nmove 15 from 4 to 9\nmove 5 from 9 to 1\nmove 2 from 6 to 2\nmove 6 from 6 to 3\nmove 1 from 8 to 6\nmove 6 from 5 to 9\nmove 3 from 6 to 5\nmove 2 from 4 to 7")

(def example-stacks "    [D]    \n[N] [C]    \n[Z] [M] [P]\n 1   2   3 ")

(def example-moves "move 1 from 2 to 1\nmove 3 from 1 to 3\nmove 2 from 2 to 1\nmove 1 from 1 to 2")


(defn parse-stacks
  [stacks]
  (->> (str/split-lines stacks)
       (map #(take-nth 4 (drop 1 %)))))

(defn parse-moves
  [moves-str]
  (map #(let [[_ qty from to] (re-matches #"move (\d+) from (\d+) to (\d+)" %)]
          (zipmap
            [:from :to :qty]
            (map parse-long [from to qty]))) (str/split-lines moves-str)))

(defn raw-stacks->model
  [raw-stacks]
  (let [raws (reverse (butlast raw-stacks))]
    (reduce
      (fn [stack line]
        (loop [stack stack
               [crate & crates] line
               index 1]
          (if crate
            (recur (if (= \space crate)
                     stack
                     (update stack index (fnil conj []) crate))
                   crates
                   (inc index))
            stack)))
      (sorted-map)
      raws)))

(defn move-crate
  [stacks move]
  (let [{:keys [from to]} move
        elem (peek (stacks from))]
    (-> stacks
        (update from pop)
        (update to conj elem))))

(defn move-crate-9000
  [stacks move]
  (let [qty (:qty move)]
    (->> stacks
         (iterate #(move-crate % move))
         (take (inc qty))
         (last))))

(defn solve
  [mover input-moves input-stacks]
  (let [moves (parse-moves input-moves)
        stacks (raw-stacks->model (parse-stacks input-stacks))
        moved (reduce mover stacks moves)]
    (apply str (map peek (vals moved)))))

(defn solve-part-1
  [input-moves input-stacks]
  (solve move-crate-9000 input-moves input-stacks))

(defn move-crate-9001
  [stacks move]
  (let [{:keys [from to qty]} move
        stack (stacks from)
        cnt (count stack)
        move-from (- cnt qty)
        [still moved] (map vec (split-at move-from stack))]
    (-> stacks
        (assoc from still)
        (update to into moved))))

(defn solve-part-2
  [input-moves input-stacks]
  (solve move-crate-9001 input-moves input-stacks))

(comment
  (solve-part-1 example-moves example-stacks)
  (solve-part-2 example-moves example-stacks)
  (assert (= "JDTMRWCQJ" (solve-part-1 moves stacks) ))
  (assert (= "VHJDDCWRD" (solve-part-2 moves stacks)))
  )