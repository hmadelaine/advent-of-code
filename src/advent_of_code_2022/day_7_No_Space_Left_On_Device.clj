(ns advent-of-code-2022.day-7-No-Space-Left-On-Device
  "cd means change directory.
   This changes which directory is the current directory,
   but the specific result depends on the argument:

   cd x moves in one level: it looks in the current directory for the directory named x and makes it the current directory.
   cd .. moves out one level: it finds the directory that contains the current directory, then makes that directory the current directory.
   cd / switches the current directory to the outermost directory, /.

   ls means list.
   It prints out all of the files and directories immediately contained by the current directory:

   123 abc means that the current directory contains a file named abc with size 123.
   dir xyz means that the current directory contains a directory named xyz.
   Since the disk is full, your first step should probably be to find directories that are good candidates for deletion. To do this, you need to determine the total size of each directory. The total size of a directory is the sum of the sizes of the files it contains, directly or indirectly. (Directories themselves do not count as having any intrinsic size.)

   The total sizes of the directories above can be found as follows:

   The total size of directory e is 584 because it contains a single file i of size 584 and no other directories.
   The directory a has total size 94853 because it contains files f (size 29116), g (size 2557), and h.lst (size 62596), plus file i indirectly (a contains e which contains i).
   Directory d has total size 24933642.
   As the outermost directory, / contains every file. Its total size is 48381165, the sum of the size of every file.
   To begin, find all of the directories with a total size of at most 100000, then calculate the sum of their total sizes. In the example above, these directories are a and e; the sum of their total sizes is 95437 (94853 + 584). (As in this example, this process can count files more than once!)

   Find all of the directories with a total size of at most 100000. What is the sum of the total sizes of those directories?"
  (:require [clojure.string :as str]))

(def example "$ cd /\n$ ls\ndir a\n14848514 b.txt\n8504156 c.dat\ndir d\n$ cd a\n$ ls\ndir e\n29116 f\n2557 g\n62596 h.lst\n$ cd e\n$ ls\n584 i\n$ cd ..\n$ cd ..\n$ cd d\n$ ls\n4060174 j\n8033020 d.log\n5626152 d.ext\n7214296 k")

(def input "$ cd /\n$ ls\ndir drblq\n133789 fjf\ndir jpfrhmw\ndir jqfwd\ndir ncgffsr\n12962 ntnr.lrq\ndir qnbq\ndir rqdngnrq\ndir shcvnqq\ndir vsd\ndir vtzvf\n$ cd drblq\n$ ls\n133843 bglzqdd\ndir brfnfhj\n268201 fbqjmp.jzv\n80676 shcvnqq\n$ cd brfnfhj\n$ ls\n150447 jlcg.dsg\ndir nhvgrzs\n$ cd nhvgrzs\n$ ls\n282889 jlcg.dsg\n19004 ncgffsr.gwr\ndir vbzr\n6338 vpsgdph.gbh\ndir wdcn\n$ cd vbzr\n$ ls\n225101 fbqjmp\n243277 vbzr\n$ cd ..\n$ cd wdcn\n$ ls\n154089 dlmpbbf.psv\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd jpfrhmw\n$ ls\n87622 cffdsj.jzf\n26165 qnbq.sbm\ndir vbzr\n$ cd vbzr\n$ ls\ndir blhstw\n16919 nttftcts\ndir rgdp\n116477 shcvnqq\n242592 tmjrnqbz.chq\ndir vbzr\ndir wmct\n$ cd blhstw\n$ ls\n98023 jwdv.qct\n$ cd ..\n$ cd rgdp\n$ ls\ndir gcb\n141507 shcvnqq\ndir ssvzm\n$ cd gcb\n$ ls\n189016 ncgffsr.rbq\n$ cd ..\n$ cd ssvzm\n$ ls\n82667 shcvnqq.zjq\n$ cd ..\n$ cd ..\n$ cd vbzr\n$ ls\n120202 jlcg.dsg\n86205 vbzr.jtr\n$ cd ..\n$ cd wmct\n$ ls\ndir fbsfcgph\n155709 hpsftv\n13636 lztgs\n273353 ncgffsr.jsg\ndir pvwhpfp\n$ cd fbsfcgph\n$ ls\n139944 ncgffsr.gpf\n$ cd ..\n$ cd pvwhpfp\n$ ls\n111230 bscrjpzh.glp\ndir dgjsddgq\n37234 lwd\n107139 lztgs\n258111 mgtwwvwz\n117638 qpdvnfb.gnf\ndir szrplcdw\ndir vzsl\ndir wsmf\n$ cd dgjsddgq\n$ ls\ndir qnbq\n$ cd qnbq\n$ ls\n199119 jlcg.dsg\n$ cd ..\n$ cd ..\n$ cd szrplcdw\n$ ls\n122236 qclr.cpf\n269638 qnbq\n$ cd ..\n$ cd vzsl\n$ ls\n233006 twpz.tdm\n$ cd ..\n$ cd wsmf\n$ ls\ndir wcnptvtz\n$ cd wcnptvtz\n$ ls\n183952 shcvnqq.lwt\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd jqfwd\n$ ls\ndir hqb\n285121 jqffsjbs.jrm\ndir nhpqpdn\ndir qnbq\ndir qtrv\ndir wspztvjr\n$ cd hqb\n$ ls\n253786 jwdv.qct\ndir vbzr\n$ cd vbzr\n$ ls\n153 gbh\ndir gqpqqrgl\ndir jzncgd\n36914 nvdnsnls.mpd\n$ cd gqpqqrgl\n$ ls\n206691 dmdgcwm.bgh\n$ cd ..\n$ cd jzncgd\n$ ls\n122640 vrgmf.tnp\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd nhpqpdn\n$ ls\n86329 ntnr.lrq\n$ cd ..\n$ cd qnbq\n$ ls\n76269 fbqjmp.lbd\n118968 fbqjmp.msg\n190416 gfwhsb.dpc\ndir lhgjrmj\ndir pbv\n173541 pfl\n141842 srrmt.ssj\n$ cd lhgjrmj\n$ ls\ndir ghccnw\n180420 ldzcj.rwz\n149356 lztgs\n61792 ncgffsr\ndir spmbcjhc\n$ cd ghccnw\n$ ls\n253233 lztgs\n56439 ntnr.lrq\n19225 ntrmjf.gdb\n31628 pdhhzjhm.lbd\n$ cd ..\n$ cd spmbcjhc\n$ ls\ndir shcvnqq\n$ cd shcvnqq\n$ ls\n122334 drjbh\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd pbv\n$ ls\n69436 cctsjqh.wqr\n285573 ljtqddz\n$ cd ..\n$ cd ..\n$ cd qtrv\n$ ls\n234568 dmwqfbwd\ndir pwwsrjc\n245046 qmcr\n159151 qtvdjncm.rdb\ndir swhzds\n178915 vbzr.vgn\ndir vcgv\n$ cd pwwsrjc\n$ ls\n173975 bgdj.jnw\n202714 jwdv.qct\n270702 wggrgcvw.rtp\n$ cd ..\n$ cd swhzds\n$ ls\n114686 jwdv.qct\n$ cd ..\n$ cd vcgv\n$ ls\ndir fbqjmp\ndir qlsgtfhf\ndir vbzr\n$ cd fbqjmp\n$ ls\n73065 fbqjmp.jfb\ndir shcvnqq\n$ cd shcvnqq\n$ ls\n231428 shcvnqq\n$ cd ..\n$ cd ..\n$ cd qlsgtfhf\n$ ls\n75227 ntnr.lrq\n$ cd ..\n$ cd vbzr\n$ ls\n128050 ncgffsr.gsj\n187649 vbzr\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd wspztvjr\n$ ls\ndir pntrhtwh\ndir qnbq\ndir zfdzvv\n$ cd pntrhtwh\n$ ls\n237258 cffhtr\n$ cd ..\n$ cd qnbq\n$ ls\ndir qnbq\n$ cd qnbq\n$ ls\ndir ccwmftsj\n$ cd ccwmftsj\n$ ls\ndir mfc\ndir shcvnqq\n12262 smpjmn\n$ cd mfc\n$ ls\n198047 fbqjmp.cgh\ndir gghsht\n205411 wlclz\n$ cd gghsht\n$ ls\n31767 vbzr.lmb\n$ cd ..\n$ cd ..\n$ cd shcvnqq\n$ ls\ndir lgrghwf\n$ cd lgrghwf\n$ ls\n114786 shcvnqq.vrz\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd zfdzvv\n$ ls\n54298 sjp\n60303 tcmhrll.htm\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd ncgffsr\n$ ls\ndir fqqsqmpr\ndir gfznw\ndir ncdft\ndir pwmppt\ndir shcvnqq\n196969 vbzr\n214841 vzgvr\n$ cd fqqsqmpr\n$ ls\ndir mcdjcntr\n$ cd mcdjcntr\n$ ls\n281856 ncgffsr.lbm\n$ cd ..\n$ cd ..\n$ cd gfznw\n$ ls\n255657 fzrctbsj.lgf\ndir ltfsndpd\n175434 qnbq\n31794 qnbq.zhd\n13366 shcvnqq.wld\ndir vcspqgn\n235199 wmnjjd.bnh\ndir wqpnp\n$ cd ltfsndpd\n$ ls\ndir ncgffsr\ndir zpzvdhb\n$ cd ncgffsr\n$ ls\n9898 jjbsnj.gcg\n$ cd ..\n$ cd zpzvdhb\n$ ls\n106139 lnp\n$ cd ..\n$ cd ..\n$ cd vcspqgn\n$ ls\n25386 dgsmmqj\n$ cd ..\n$ cd wqpnp\n$ ls\n65905 wjtbfvjp.fmd\n$ cd ..\n$ cd ..\n$ cd ncdft\n$ ls\n34616 bzlpmsqc\n59863 jlcg.dsg\n64629 zpzjcl.fmp\n$ cd ..\n$ cd pwmppt\n$ ls\ndir dwnqgrzm\n80901 vbzr.vsg\n89557 vbzr.zlz\n$ cd dwnqgrzm\n$ ls\n184770 jwdv.qct\ndir vbzr\n$ cd vbzr\n$ ls\n210329 jlcg.dsg\n62272 jwdv.qct\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd shcvnqq\n$ ls\n128433 gbh\n30208 hjbw\n200071 jlcg.dsg\ndir sgcz\n25045 tbhlwfqg.hts\n$ cd sgcz\n$ ls\n193481 gbh\n96461 jwdv.qct\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd qnbq\n$ ls\n236171 shcvnqq\n$ cd ..\n$ cd rqdngnrq\n$ ls\ndir cprnb\n280135 hshsfqwm\ndir hwhm\n245626 qnbq\n145502 qspgdz\n114231 rctg.tgt\ndir zgn\n$ cd cprnb\n$ ls\n115025 twwgmmp.wbb\n$ cd ..\n$ cd hwhm\n$ ls\n229849 cvm\n190622 jwdv.qct\ndir mscztz\ndir ncgffsr\n$ cd mscztz\n$ ls\n59743 bzgpzn.bds\n75184 pbdgv\n181089 shcvnqq.dhq\ndir zqgtr\n$ cd zqgtr\n$ ls\n189142 ffnznfs.nct\n$ cd ..\n$ cd ..\n$ cd ncgffsr\n$ ls\ndir dphrnjl\ndir zzfztql\n$ cd dphrnjl\n$ ls\n117317 vbzr\n$ cd ..\n$ cd zzfztql\n$ ls\n51096 lztgs\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd zgn\n$ ls\ndir bpbzwgz\ndir gqnw\n75631 ljptj\n283351 ljzhsw.rbs\n131158 lztgs\ndir ncgffsr\n3136 nnpl.swf\ndir shcvnqq\ndir vbzr\n$ cd bpbzwgz\n$ ls\n29659 jlcg.dsg\n15547 shcvnqq\n117389 zprhsdfv\n$ cd ..\n$ cd gqnw\n$ ls\n117091 brqwhst.jgb\n88406 nzjmbrrm.hmh\n$ cd ..\n$ cd ncgffsr\n$ ls\n195821 gbh\ndir lbzgc\n226692 llqqr.spq\n247989 lztgs\n231909 vnctc\n157973 wqnggh\n$ cd lbzgc\n$ ls\n251414 ffmsbscc.dqg\n46840 lztgs\n$ cd ..\n$ cd ..\n$ cd shcvnqq\n$ ls\ndir dvvmhzcq\ndir ncgffsr\ndir sqzzllv\n$ cd dvvmhzcq\n$ ls\ndir qnbq\n70226 qvvm.rpp\ndir shcvnqq\n$ cd qnbq\n$ ls\n103994 bfcjrmvr.ltq\ndir fbqjmp\ndir fcs\n177152 gjghvvw.bzg\ndir lbfjqh\n78412 ntnr.lrq\ndir sgjtm\n286995 shcvnqq\n51750 wmq.vjj\n$ cd fbqjmp\n$ ls\n267212 qhhb.zvg\n$ cd ..\n$ cd fcs\n$ ls\n272051 znhsswwh.mjj\n$ cd ..\n$ cd lbfjqh\n$ ls\n261487 jlcg.dsg\n$ cd ..\n$ cd sgjtm\n$ ls\ndir dnznpj\ndir jzsntnbs\ndir nqgcbd\ndir vdg\n$ cd dnznpj\n$ ls\n173938 hrp.cjq\n180485 qnbq.thj\n215400 ztvt.wnt\n$ cd ..\n$ cd jzsntnbs\n$ ls\n67448 gpvgh.psg\n$ cd ..\n$ cd nqgcbd\n$ ls\n196250 fbqjmp.qcv\n198482 jlcg.dsg\n$ cd ..\n$ cd vdg\n$ ls\n257343 jwdv.qct\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd shcvnqq\n$ ls\n156769 fbqjmp.hdb\n$ cd ..\n$ cd ..\n$ cd ncgffsr\n$ ls\n205473 fbqjmp\n113067 gsvznzz.qtv\n$ cd ..\n$ cd sqzzllv\n$ ls\n146018 ddvjgswr.gsq\n$ cd ..\n$ cd ..\n$ cd vbzr\n$ ls\ndir vbzr\n$ cd vbzr\n$ ls\n266721 mhlfqpbs.pwr\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd shcvnqq\n$ ls\ndir slnvdd\n$ cd slnvdd\n$ ls\n90875 pzqv.gnv\n207484 rbrj.vcr\n$ cd ..\n$ cd ..\n$ cd vsd\n$ ls\ndir dfb\ndir fqqnsph\ndir gbwdhjr\n18837 jwdv.qct\ndir ncgffsr\ndir qnbq\ndir rjzjrbvs\n$ cd dfb\n$ ls\ndir bpst\n66174 jwdv.qct\ndir lcwhfzjw\n$ cd bpst\n$ ls\ndir nqftnn\ndir pcvgnvnp\n$ cd nqftnn\n$ ls\ndir bbrsg\ndir gjfc\ndir hfql\ndir shcvnqq\n139226 shcvnqq.sbd\ndir ssnjqbg\n$ cd bbrsg\n$ ls\n73382 vjcf\n$ cd ..\n$ cd gjfc\n$ ls\n164310 gbh\n126316 mmqnrc\n133899 ntnr.lrq\n102615 rgfhrt\n$ cd ..\n$ cd hfql\n$ ls\n14685 jwdv.qct\n$ cd ..\n$ cd shcvnqq\n$ ls\n119597 lztgs\n34165 shcvnqq.zcg\n$ cd ..\n$ cd ssnjqbg\n$ ls\n77678 gqdfbqj.tmj\n$ cd ..\n$ cd ..\n$ cd pcvgnvnp\n$ ls\n21250 lhq\n266619 qps.crp\n$ cd ..\n$ cd ..\n$ cd lcwhfzjw\n$ ls\ndir bhdnnbvm\ndir fdnsvfh\n12002 jlcg.dsg\ndir lfdbzfl\n46488 ncgffsr\n233704 nthcv.pnc\n204660 ntnr.lrq\n172482 shcvnqq\ndir tlw\n$ cd bhdnnbvm\n$ ls\n37204 fwrdjw.zvv\n3248 ntnr.lrq\n$ cd ..\n$ cd fdnsvfh\n$ ls\n20765 jlfgnwb.szl\n$ cd ..\n$ cd lfdbzfl\n$ ls\ndir fspntmld\n183925 jlcg.dsg\n$ cd fspntmld\n$ ls\n251568 lztgs\n146785 ncgffsr.mmj\n$ cd ..\n$ cd ..\n$ cd tlw\n$ ls\ndir qqn\n$ cd qqn\n$ ls\n39232 lprqfwf\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd fqqnsph\n$ ls\n132318 lztgs\n103863 ntnr.lrq\n18793 tngbs\n$ cd ..\n$ cd gbwdhjr\n$ ls\n253798 jwdv.qct\n$ cd ..\n$ cd ncgffsr\n$ ls\n110767 blctz.tqz\ndir csfssn\ndir dbbfz\ndir hjgm\ndir hwd\n249139 rgcz.gnz\ndir wgw\n$ cd csfssn\n$ ls\ndir dlcw\ndir jqspd\n119066 mlwlc.mql\ndir ncgffsr\n203475 nwnbsc\n143071 qnbq\n116623 qvw.gjz\n83637 whm.cdg\n$ cd dlcw\n$ ls\n232066 gqllsd.qpl\n1046 mfsh\n$ cd ..\n$ cd jqspd\n$ ls\n251070 mthmm.bmh\n$ cd ..\n$ cd ncgffsr\n$ ls\n83639 ntnr.lrq\n$ cd ..\n$ cd ..\n$ cd dbbfz\n$ ls\n112576 jgqf.qmj\n148549 jlcg.dsg\n144811 jwdv.qct\n23726 ntnr.lrq\n123802 pgdjchrf.vnm\ndir vzfbzbcp\n$ cd vzfbzbcp\n$ ls\n39375 fbqq\n31914 jwdv.qct\n165999 lztgs\n$ cd ..\n$ cd ..\n$ cd hjgm\n$ ls\ndir ljqjtdmf\n100534 mdw\n219057 qnbq\n97164 rzjwmvdw.vlv\ndir shcvnqq\n83034 vbzr\n$ cd ljqjtdmf\n$ ls\n23716 dmslzv.qns\n159519 gbh\ndir hlvbmpg\ndir nlqqshp\n247315 vqt\ndir wlsjnthg\n$ cd hlvbmpg\n$ ls\n54421 jlcg.dsg\n$ cd ..\n$ cd nlqqshp\n$ ls\ndir rvzprwhp\n$ cd rvzprwhp\n$ ls\n35024 lztgs\n$ cd ..\n$ cd ..\n$ cd wlsjnthg\n$ ls\n29178 gnrlgb.bgh\n$ cd ..\n$ cd ..\n$ cd shcvnqq\n$ ls\n150311 nvrd\n$ cd ..\n$ cd ..\n$ cd hwd\n$ ls\ndir jzqtmm\n$ cd jzqtmm\n$ ls\n103547 jtvdt.jtn\n$ cd ..\n$ cd ..\n$ cd wgw\n$ ls\ndir mmhlt\n$ cd mmhlt\n$ ls\ndir cmwjh\n$ cd cmwjh\n$ ls\n243844 qnbq.shn\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd qnbq\n$ ls\ndir dhfng\ndir fbqjmp\n16855 rgrszmrh.lbl\ndir rqjs\ndir shcvnqq\n38322 vhvrmq\n$ cd dhfng\n$ ls\n132537 gwngz.hpt\ndir lbccc\n182221 ntnr.lrq\n$ cd lbccc\n$ ls\n282448 fbqjmp.njj\n267049 gbh\ndir jtj\ndir ntnn\ndir vbfgmmvw\n128500 vbzr\n$ cd jtj\n$ ls\ndir hvmlh\n$ cd hvmlh\n$ ls\n131886 dmww.sqc\n$ cd ..\n$ cd ..\n$ cd ntnn\n$ ls\n109064 lgh.bbf\ndir wfgdd\n53862 wflv.ngc\n$ cd wfgdd\n$ ls\n58756 gbh\ndir lgzlndn\ndir qnbq\n$ cd lgzlndn\n$ ls\n190415 dwsqvczd\n$ cd ..\n$ cd qnbq\n$ ls\n240922 znjhmhp.ngt\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd vbfgmmvw\n$ ls\n271827 vbzr.dfl\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd fbqjmp\n$ ls\n144993 gvpnf\n150786 jwdv.qct\n49025 pdcwwtt.grs\n$ cd ..\n$ cd rqjs\n$ ls\ndir bwnzs\n119390 jlcg.dsg\n172042 vjzg\n$ cd bwnzs\n$ ls\n108537 hzzgm.zrn\n38699 qgfqbfr\ndir vhvcfhvr\n$ cd vhvcfhvr\n$ ls\n2783 jwdv.qct\n209933 mgj.nvj\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd shcvnqq\n$ ls\n257312 fbqjmp\n193792 msdqtrpn.grn\n98165 rgm\n$ cd ..\n$ cd ..\n$ cd rjzjrbvs\n$ ls\ndir ftrlfg\ndir mtrnl\ndir rdpbbd\ndir shcvnqq\ndir vztnr\n$ cd ftrlfg\n$ ls\n196590 cjjvwjb\ndir ffsvh\n70123 ldnbc\ndir lwnfc\n106499 lztgs\ndir ncgffsr\ndir tfdctq\ndir vgthdbf\n80852 zndjt.wtl\n$ cd ffsvh\n$ ls\n20370 dvdftpvb.qcj\n$ cd ..\n$ cd lwnfc\n$ ls\ndir fgmd\ndir gmdjt\n274331 hmgjmq.vbz\n9726 qjfdqbf.dfj\ndir ssnncn\n$ cd fgmd\n$ ls\n280608 jwdv.qct\n201912 rqtbw.shd\n$ cd ..\n$ cd gmdjt\n$ ls\n202107 jwdv.qct\n$ cd ..\n$ cd ssnncn\n$ ls\n140697 jwdv.qct\n$ cd ..\n$ cd ..\n$ cd ncgffsr\n$ ls\n227389 fpdfqp.fzl\n164141 hzhrrvpm.hlf\n$ cd ..\n$ cd tfdctq\n$ ls\ndir cttmzlw\ndir ntvtm\n257094 qnbq.zjm\n284928 shcvnqq\n$ cd cttmzlw\n$ ls\n142651 rptschdv.mgv\n$ cd ..\n$ cd ntvtm\n$ ls\n176269 dhpj\n88278 gbh\n$ cd ..\n$ cd ..\n$ cd vgthdbf\n$ ls\n130998 ncgffsr.mnf\n$ cd ..\n$ cd ..\n$ cd mtrnl\n$ ls\n86144 djwnvdj\n122600 gsdpwh.cmb\n$ cd ..\n$ cd rdpbbd\n$ ls\n177384 gbh\ndir gstfdm\ndir qnbq\ndir qtj\n260302 vbzr.dhq\n$ cd gstfdm\n$ ls\n23734 mnwzrm.hzr\n$ cd ..\n$ cd qnbq\n$ ls\n51705 gmt\n205537 ntnr.lrq\n94469 vbzr.bvj\n$ cd ..\n$ cd qtj\n$ ls\ndir tls\ndir zvpcfhg\n$ cd tls\n$ ls\ndir chvgwnt\ndir jvgnmfjw\n$ cd chvgwnt\n$ ls\ndir rbw\ndir srhj\n$ cd rbw\n$ ls\n174372 btjd.bvv\n272995 cnqqh.dfc\n$ cd ..\n$ cd srhj\n$ ls\n134054 qwzpr\n$ cd ..\n$ cd ..\n$ cd jvgnmfjw\n$ ls\ndir hdcwbwgm\n236775 sdc\n$ cd hdcwbwgm\n$ ls\n113707 ntnr.lrq\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd zvpcfhg\n$ ls\ndir lsq\n$ cd lsq\n$ ls\n220331 jlcg.dsg\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd shcvnqq\n$ ls\ndir cmwrqgfq\n258731 fbqjmp.fvn\n277895 gbh\n64973 jlcg.dsg\n77978 jwdv.qct\ndir lttjrdn\ndir sqgnhc\n$ cd cmwrqgfq\n$ ls\n81199 gbh\n$ cd ..\n$ cd lttjrdn\n$ ls\n23355 gbh\n148263 hcgfqdw\n57338 hjwr\n166510 jbvnmcj\n$ cd ..\n$ cd sqgnhc\n$ ls\ndir glswqrdp\ndir qnbq\n$ cd glswqrdp\n$ ls\n225761 ncgffsr.vct\n$ cd ..\n$ cd qnbq\n$ ls\n62861 pdqz.wzs\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd vztnr\n$ ls\n189943 wvtlfsp\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd vtzvf\n$ ls\n43248 jwdv.qct")

(defn parse
  [input]
  (str/split-lines input))

(defn current
  [fs]
  (into [:fs] (:path fs)))

(defn change-dir
  [fs path dir]
  (let [path' (conj path dir)
        fs' (assoc-in fs path' {})]
    [fs' path']))

(defn back-dir
  [fs path]
  [fs (pop path)])

(defn add-dir
  [fs path dir]
  [(assoc-in fs (conj path dir) {}) path])


(defn add-file
  [fs path file]
  (update-in fs [:fs (current fs) :files] conj file))

(loop [[command & cmds] ["$ cd /"
                         "$ ls"
                         "dir a"
                         ;"14848514 b.txt"
                         ;"8504156 c.dat"
                         "dir d"
                         "$ cd a"
                         "$ ls"
                         "dir e"
                         ;"29116 f"
                         ;"2557 g"
                         ;"62596 h.lst"
                         "$ cd e"
                         "$ ls"
                         ;"584 i"
                         "$ cd .."
                         "$ cd .."
                         "$ cd d"
                         "$ ls"
                         ;"4060174 j"
                         ;"8033020 d.log"
                         ;"5626152 d.ext"
                         ;"7214296 k"
                         ]
       fs {}
       path []]
  (prn command fs path)
  (if command
    (let [[fs' path'] (condp re-matches command
                        #"\$ cd (\.\.)" :>> (fn [_]
                                              (back-dir fs path))
                        #"\$ cd ([a-z/]+)" :>> (fn [[_ dir]]
                                                 (change-dir fs path dir))
                        #"\$ ls" :>> (fn [[_]]
                                       [fs path])
                        #"dir (.+)" :>> (fn [[_ dir]]
                                          (add-dir fs path dir))
                        #"(\d+)\s(.+)" :>> (fn [[_ file-size file-name]]
                                             (add-file fs path {:name file-name
                                                                :size file-size})))]
      (recur cmds fs' path'))
    fs))



[{:name "/"
  :type :dir
  :children [{:name "a"
              :type :dir
              :children [{:name "e"
                          :type :dir
                          :children []}]}
             {:name "d"
              :type :dir
              :children []}]}
 ]