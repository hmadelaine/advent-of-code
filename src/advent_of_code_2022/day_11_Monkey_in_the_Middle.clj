(ns advent-of-code-2022.day-11-Monkey-in-the-Middle
  (:require
    [clojure.string :as str]))

(def example "Monkey 0:\n  Starting items: 79, 98\n  Operation: new = old * 19\n  Test: divisible by 23\n    If true: throw to monkey 2\n    If false: throw to monkey 3\n\nMonkey 1:\n  Starting items: 54, 65, 75, 74\n  Operation: new = old + 6\n  Test: divisible by 19\n    If true: throw to monkey 2\n    If false: throw to monkey 0\n\nMonkey 2:\n  Starting items: 79, 60, 97\n  Operation: new = old * old\n  Test: divisible by 13\n    If true: throw to monkey 1\n    If false: throw to monkey 3\n\nMonkey 3:\n  Starting items: 74\n  Operation: new = old + 3\n  Test: divisible by 17\n    If true: throw to monkey 0\n    If false: throw to monkey 1")
(def input "Monkey 0:\n  Starting items: 98, 89, 52\n  Operation: new = old * 2\n  Test: divisible by 5\n    If true: throw to monkey 6\n    If false: throw to monkey 1\n\nMonkey 1:\n  Starting items: 57, 95, 80, 92, 57, 78\n  Operation: new = old * 13\n  Test: divisible by 2\n    If true: throw to monkey 2\n    If false: throw to monkey 6\n\nMonkey 2:\n  Starting items: 82, 74, 97, 75, 51, 92, 83\n  Operation: new = old + 5\n  Test: divisible by 19\n    If true: throw to monkey 7\n    If false: throw to monkey 5\n\nMonkey 3:\n  Starting items: 97, 88, 51, 68, 76\n  Operation: new = old + 6\n  Test: divisible by 7\n    If true: throw to monkey 0\n    If false: throw to monkey 4\n\nMonkey 4:\n  Starting items: 63\n  Operation: new = old + 1\n  Test: divisible by 17\n    If true: throw to monkey 0\n    If false: throw to monkey 1\n\nMonkey 5:\n  Starting items: 94, 91, 51, 63\n  Operation: new = old + 4\n  Test: divisible by 13\n    If true: throw to monkey 4\n    If false: throw to monkey 3\n\nMonkey 6:\n  Starting items: 61, 54, 94, 71, 74, 68, 98, 83\n  Operation: new = old + 2\n  Test: divisible by 3\n    If true: throw to monkey 2\n    If false: throw to monkey 7\n\nMonkey 7:\n  Starting items: 90, 56\n  Operation: new = old * old\n  Test: divisible by 11\n    If true: throw to monkey 3\n    If false: throw to monkey 5")


(defn parse-monkey
  [monkey-str]
  (let [[id-str items-str operation-str test-str true-str false-str] (str/split-lines monkey-str)
        [_ id] (re-matches #"Monkey (\d):" id-str)
        items (mapv (comp bigint second) (re-seq #"\s(\d+)(?:,|$)" items-str))
        [_ op-str value] (re-matches #"  Operation: new = old (.) (\d+|old)" operation-str)
        ops {"*" *
             "+" +}
        op (ops op-str)
        next-worry (if (= "old" value)
                     #(op % %)
                     #(op % (bigint value)))
        [_ modulo] (re-matches #"  Test: divisible by (\d+)" test-str)
        [_ monkey-true] (re-matches #"    If true: throw to monkey (\d+)" true-str)
        [_ monkey-false] (re-matches #"    If false: throw to monkey (\d+)" false-str)]
    {:id          (parse-long id)
     :items       items
     :modulo      #(zero? (mod % (bigint modulo)))
     :inspects    next-worry
     :throw-true  (parse-long monkey-true)
     :throw-false (parse-long monkey-false)
     :inspected   0}))

(defn parse-monkeys
  [input]
  (mapv parse-monkey (str/split input #"\n\n")))

(defn relief
  [worry]
  (long (/ worry 3)))

(defn turn
  [monkey]
  (let [{:keys [items inspects modulo throw-true throw-false]} monkey]
    (for [worry items]
      (let [next-worry (-> worry
                           (inspects)
                           (relief))
            throw-to (if (modulo next-worry)
                       throw-true
                       throw-false)]
        [throw-to next-worry]))))

(defn throws-to
  [monkeys from throws]
  (reduce
    (fn [state [throw-to next-worry]]
      (-> state
          (update-in [from :items] #(vec (drop 1 %)))
          (update-in [throw-to :items] conj next-worry)
          (update-in [from :inspected] inc)))
    monkeys
    throws))

(defn round
  [monkeys]
  (loop [monkeys monkeys
         id 0]
    (if (< id (count monkeys))
      (recur
        (throws-to monkeys id (turn (monkeys id)))
        (inc id))
      monkeys)))

(def mem-round (memoize round))

(defn solve-part-1
  [input]
  (let [monkeys (parse-monkeys input)]
    (->> monkeys
         (iterate mem-round)
         (drop 20)
         (take 1)
         (first)
         (map :inspected)
         (sort >)
         (take 2)
         (reduce *))))

(defn solve-part-2
  [input]
  (let [monkeys (parse-monkeys input)]
    (->> monkeys
         (iterate mem-round)
         (drop 1000)
         (take 1)
         (first)
         (map :inspected)
         (sort >)
         (take 2)
         (reduce *))))

(comment
  (solve-part-1 example)
  (solve-part-1 input)
  (solve-part-2 input)

  )

(let [dividend 2537523722N
      divisor 4N]
  (bit-and dividend (dec divisor)))


