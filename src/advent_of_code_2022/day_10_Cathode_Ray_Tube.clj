(ns advent-of-code-2022.day-10-Cathode-Ray-Tube
  "The CPU has a single register, X,
   which starts with the value 1.
   It supports only two instructions:
   addx V takes two cycles to complete.

   After two cycles, the X register is increased by the value V. (V can be negative.)

   noop takes one cycle to complete. It has no other effect.

   The CPU uses these instructions in a program (your puzzle input) to, somehow, tell the screen what to draw."
  (:require [clojure.string :as str]))

(def example "noop\naddx 3\naddx -5")
(def example-2 "addx 15\naddx -11\naddx 6\naddx -3\naddx 5\naddx -1\naddx -8\naddx 13\naddx 4\nnoop\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx -35\naddx 1\naddx 24\naddx -19\naddx 1\naddx 16\naddx -11\nnoop\nnoop\naddx 21\naddx -15\nnoop\nnoop\naddx -3\naddx 9\naddx 1\naddx -3\naddx 8\naddx 1\naddx 5\nnoop\nnoop\nnoop\nnoop\nnoop\naddx -36\nnoop\naddx 1\naddx 7\nnoop\nnoop\nnoop\naddx 2\naddx 6\nnoop\nnoop\nnoop\nnoop\nnoop\naddx 1\nnoop\nnoop\naddx 7\naddx 1\nnoop\naddx -13\naddx 13\naddx 7\nnoop\naddx 1\naddx -33\nnoop\nnoop\nnoop\naddx 2\nnoop\nnoop\nnoop\naddx 8\nnoop\naddx -1\naddx 2\naddx 1\nnoop\naddx 17\naddx -9\naddx 1\naddx 1\naddx -3\naddx 11\nnoop\nnoop\naddx 1\nnoop\naddx 1\nnoop\nnoop\naddx -13\naddx -19\naddx 1\naddx 3\naddx 26\naddx -30\naddx 12\naddx -1\naddx 3\naddx 1\nnoop\nnoop\nnoop\naddx -9\naddx 18\naddx 1\naddx 2\nnoop\nnoop\naddx 9\nnoop\nnoop\nnoop\naddx -1\naddx 2\naddx -37\naddx 1\naddx 3\nnoop\naddx 15\naddx -21\naddx 22\naddx -6\naddx 1\nnoop\naddx 2\naddx 1\nnoop\naddx -10\nnoop\nnoop\naddx 20\naddx 1\naddx 2\naddx 2\naddx -6\naddx -11\nnoop\nnoop\nnoop")
(def input "addx 1\naddx 5\naddx -1\naddx 20\naddx -14\naddx -1\naddx 5\naddx 13\naddx -12\naddx 3\naddx 3\naddx 3\naddx 1\naddx 4\nnoop\nnoop\naddx 1\nnoop\nnoop\naddx 4\nnoop\naddx -35\naddx 11\naddx -1\naddx -7\naddx 5\naddx 2\naddx 3\naddx -2\naddx 2\naddx 5\naddx 5\nnoop\nnoop\naddx -2\naddx 2\nnoop\naddx 3\naddx 2\naddx 7\nnoop\nnoop\naddx 3\naddx -2\naddx -36\nnoop\naddx 25\naddx -22\naddx 7\nnoop\naddx -2\nnoop\nnoop\nnoop\naddx 5\naddx 5\naddx 4\nnoop\naddx -2\naddx 5\naddx -4\naddx 5\naddx 4\nnoop\naddx -29\naddx 32\naddx -23\naddx -12\nnoop\naddx 7\nnoop\naddx -2\naddx 4\naddx 3\naddx 20\naddx 3\naddx -20\naddx 5\naddx 16\naddx -15\naddx 6\nnoop\nnoop\nnoop\naddx 5\nnoop\naddx 5\nnoop\nnoop\nnoop\naddx -37\naddx 2\naddx -2\naddx 7\nnoop\naddx -2\naddx 5\naddx 2\naddx 3\naddx -2\naddx 2\naddx 5\naddx 2\naddx -6\naddx -15\naddx 24\naddx 2\nnoop\naddx 3\naddx -8\naddx 15\naddx -14\naddx 15\naddx -38\nnoop\nnoop\naddx 21\naddx -14\naddx 1\naddx 5\nnoop\naddx -2\naddx 7\naddx -1\naddx 5\nnoop\naddx 2\naddx 3\naddx 3\naddx -2\naddx 4\naddx 2\naddx -17\naddx 20\nnoop\nnoop\nnoop\nnoop")

(defn parse-instruction-2
  [instruction]
  (condp re-matches instruction
    #"noop" [0]
    #"addx\s(-?\d+)" :>> (fn [[_ value]] [0 (parse-long value)])))


(defn solve
  [input]
  (vec (->> input
            (str/split-lines)
            (reduce
              (fn [ops instruction]
                (into ops (parse-instruction-2 instruction)))
              [])
            (reductions + 1))))

(defn solve-part-1
  [input]
  (let [values (solve input)
        cycles [20 60 100 140 180 220]]
    (reduce + (map *
                   (map values (map dec cycles))
                   cycles))))

(defn solve-part-2
  [input]
  (doseq [line (partition 40 (map
                               (fn [register crt]
                                 (let [sprite #{(dec register) register (inc register)}]
                                   (sprite crt)))
                               (solve input)
                               (cycle (range 40))))]
    (println (map #(if % "#" ".") line))))

(comment
  (assert (= 13140 (solve-part-1 example-2)))
  (assert (= 17380 (solve-part-1 input)))
  (solve-part-2 input)

  )
