(ns utils.solve-utils
  (:require [clojure.string :as str]))

(def ->int #(Integer/parseInt %))

(defn parse-raw
  [x s]
  (into [] x (str/split-lines s)))

(def parse-nums (partial parse-raw (map #(Long/parseLong %))))

(defn most-frequent
  "TIL : Use max-key with frequencies is great ! "
  [events]
  (apply max-key second (frequencies events)))
