(ns advent-of-code-2019.day-4
  "However, they do remember a few key facts about the password:
  It is a six-digit number.
  The value is within the range given in your puzzle input.
  Two adjacent digits are the same (like 22 in 122345).
  Going from left to right, the digits never decrease;
  they only ever increase or stay the same (like 111123 or 135679). "
  (:require [clojure.string :as str]
            [taoensso.tufte :as tufte :refer [defnp p profile]]
            [clojure.core.reducers :as r]))

(tufte/add-basic-println-handler!                           ;; 👈👇☝ Added these three lines
  {:format-pstats-opts {:columns [:n-calls :max :mean :mad :clock :total]}})

(defn number->digits
  [^Integer n]
  (map #(Integer/parseInt (str %)) (str n)))

(comment
  (profile {}
           (dotimes [n 15000]
             ((p :num->digits number->digits) n)))
)

(defn exactly-two?
  [candidat xn]
  (let [re (re-pattern (str candidat "{3,6}|(" candidat "{2})"))]
    (second (re-find re (str/join xn)))))

(defn two-adjacent-digits-are-the-same
  [xn]
  (let [pairs (seq (filter (fn [[a b]] (= a b)) (partition 2 1 xn)))]
    (when pairs xn)))

(defn two-adjacent-matching-digits-are-not-part-of-a-larger-group
  [xn]
  (when-let [pairs (seq (into #{} (filter (fn [[a b]] (= a b))) (partition 2 1 xn)))]
    (let [exactly-twos (seq (filter (fn [[p]]
                                      (exactly-two? p xn)) pairs))]
      (when exactly-twos xn))))

(defn going-from-left-to-right-the-digits-never-decrease
  [xn]
  (when (apply <= xn)
    xn))

(defn is-password?
  [candidat]
  (some-> candidat
          (number->digits)
          (two-adjacent-digits-are-the-same)
          (going-from-left-to-right-the-digits-never-decrease)))

(def input (vec (range 246540 (inc 787419))))


(defn solution-part-1
  [input]
  (p ::part-1 (count (r/foldcat (r/filter is-password? input)))))

(defnp is-password-part-2?
       [candidat]
       (some-> candidat
               number->digits
               (two-adjacent-matching-digits-are-not-part-of-a-larger-group)
               (going-from-left-to-right-the-digits-never-decrease)))

(defnp solution-part-2
       [input]
       (count (r/foldcat (r/filter is-password-part-2? input))))

(comment
  (profile {} (dotimes [_ 20]
                (solution-part-1 input)))

  (profile {} (dotimes [_ 20]
                (solution-part-2 input)))

  (count input)

  (time (reduce + (map inc (range 1 100000000))))
  (time (r/fold + (r/map inc (range 1 100000000))))
  )