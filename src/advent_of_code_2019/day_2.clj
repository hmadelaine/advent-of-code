(ns advent-of-code-2019.day-2
  (:require [clojure.string :as str]
            [com.rpl.specter :as sp]))

(def raw-input "1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,13,1,19,1,9,19,23,2,23,13,27,1,27,9,31,2,31,6,35,1,5,35,39,1,10,39,43,2,43,6,47,1,10,47,51,2,6,51,55,1,5,55,59,1,59,9,63,1,13,63,67,2,6,67,71,1,5,71,75,2,6,75,79,2,79,6,83,1,13,83,87,1,9,87,91,1,9,91,95,1,5,95,99,1,5,99,103,2,13,103,107,1,6,107,111,1,9,111,115,2,6,115,119,1,13,119,123,1,123,6,127,1,127,5,131,2,10,131,135,2,135,10,139,1,13,139,143,1,10,143,147,1,2,147,151,1,6,151,0,99,2,14,0,0")


(defn initial-state
  [raw-input]
  (partition 4 (map #(Integer/parseInt %) (str/split raw-input #","))))

(defn path
  [n]
  (mapv sp/nthpath [(quot n 4) (rem n 4)]))
(comment
  [(quot 151 4) (rem 151 4)]
  )
(defn address
  [n model]
  (sp/select-one (path n) model))

(defn set-at
  [n v model]
  (sp/setval (path n) v model))

(def ops {1 +
          2 *})

(defn process
  [stop ops memory]
  (reduce (fn [acc pointer]
            (let [[opcode param1 param2 store] (nth acc pointer)]
              (if (= stop opcode)
                (reduced acc)
                (set-at store
                        ((ops opcode)
                         (address param1 acc)
                         (address param2 acc))
                        acc)))) memory
          (range)))

(defn run
  ([stop ops noun verb init]
   (let [memory (->> init
                     (set-at 1 noun)
                     (set-at 2 verb))]
     (process stop ops memory)))
  ([noun verb init]
   (run 99 ops noun verb init)))


(defn solution-1
  []
  (address 0 (run 12 2 (initial-state raw-input))))

(defn solution-2
  [raw-input]
  (let [state (initial-state raw-input)]
   (let [[_ noun verb] (last (take-while
                               (fn [[result]]
                                 (<= result 19690720))
                               (for [noun (range 100)
                                     verb (range 100)]
                                 [(address 0 (run noun verb state)) noun verb])))]
     (+ verb (* 100 noun)))))

(defn process-2
  [stop ops memory]
  (reduce (fn [acc pointer]
            (let [[opcode param1 param2 store] (nth acc pointer)
                  _ (prn opcode param1 param2 store)
                  value ((ops opcode)
                         (address store acc)
                         (address param2 acc)
                         )]

              (if (= stop value)
                (reduced acc)
                (set-at param1
                        value
                        acc))))
          memory
          (range (- (count memory) 2) 0 -1)))

(def input-2 (run 12 2 (initial-state raw-input)))

(->> input-2
     (process-2 3306701 {1 - 2 /})
     )


(comment

  (->> (process 99 ops (initial-state "1,9,10,3,2,3,11,0,99,30,40,50"))
       (process-2 3500 {1 - 2 /}))

  (time (address 0 (solution-1)))
  )