(ns advent-of-code-2020.day-1-report-repair
  (:require [clojure.string :as str]
            [clojure.math.combinatorics :as comb]))

(def raw-input "1946\n1859\n1654\n1806\n1648\n1873\n1216\n1831\n1610\n1779\n1626\n1332\n1713\n1919\n1353\n1720\n1818\n1976\n1993\n1617\n1678\n1655\n1725\n1686\n1737\n1696\n1046\n1814\n1909\n1618\n2006\n1903\n1528\n1635\n1457\n1924\n1734\n1723\n1735\n1984\n1846\n1921\n1587\n2009\n1607\n1987\n1910\n1571\n1898\n1869\n1537\n1446\n1535\n1802\n1847\n1966\n1944\n1793\n1383\n1850\n1274\n347\n1208\n1748\n1906\n1771\n1849\n1773\n1792\n1705\n1538\n1564\n2003\n1994\n1545\n1704\n1657\n1483\n1701\n1724\n1293\n1834\n1712\n1950\n1844\n1290\n1692\n1820\n1585\n1986\n1328\n1841\n1709\n1232\n1945\n1684\n1787\n1991\n1914\n16\n1977\n1620\n1825\n1866\n1615\n1832\n496\n1932\n1819\n1559\n1870\n1677\n1650\n1594\n1664\n1600\n1622\n1862\n1937\n1624\n1580\n1931\n1803\n1839\n1755\n1952\n1473\n1694\n1864\n1178\n1163\n1790\n393\n1776\n1871\n1999\n1923\n1174\n1557\n1646\n1200\n1842\n1432\n1573\n1913\n1954\n1599\n1980\n1948\n1430\n1298\n1835\n1643\n1742\n1609\n1649\n1382\n1343\n1263\n1908\n1703\n1922\n1764\n1603\n1330\n588\n954\n1772\n1553\n975\n1499\n1552\n1214\n1829\n1698\n1797\n1807\n1961\n1947\n1845\n1881\n1821\n1815\n1623\n1675\n1478\n1886\n1951\n1700\n1890\n1876\n1781\n1853\n1983\n1901\n1939\n1292\n853\n1879\n1652")

(defn parse-input
  [raw]
  (->> raw
       (str/split-lines)
       (map parse-long)))

(defn part-1
  [input]
  (loop [[f & r] (parse-input input)]
    (if-let [result (some #(when (= 2020 (+ f %)) %) r)]
      (* f result)
      (recur r))))

(defn part-2
  [input]
  (let [expenses (parse-input input)]
    (some (fn [[f s t]]
            (when (= 2020 (+ f s t))
              (* f s t))) (comb/combinations expenses 3))))

(comment

  (assert (= 842016 (part-1 raw-input)))
  (assert (= 9199664 (part-2 raw-input)))

  )

