(ns advent-of-code-2020.day-12
  " Action N means to move north by the given value.
   Action S means to move south by the given value.
   Action E means to move east by the given value.
   Action W means to move west by the given value.
   Action L means to turn left the given number of degrees.
   Action R means to turn right the given number of degrees.
   Action F means to move forward by the given value in the direction the ship is currently facing."
  (:require [cartesian.utils :as c]
            [utils.solve-utils :as p]
            [clojure.set :as set]))

(def dico {"N" :up
           "S" :down
           "E" :right
           "W" :left
           "L" :turn-left
           "R" :turn-right
           "F" :forward})

(defn parse-12
  [raw]
  (p/parse-raw (comp
                 (map #(re-matches #"(\w)(\d+)" %))
                 (map (juxt second last))
                 (map (fn [[h n]]
                        [(dico h) (Long/parseLong n)]))) raw))

(def raw-example "F10\nN3\nF7\nR90\nF11")
(def example (parse-12 raw-example))


(def raw-input "F77\nE4\nS2\nW1\nL180\nN4\nR180\nS3\nW5\nF86\nL90\nE1\nF16\nR90\nN1\nE1\nF86\nS1\nF36\nE2\nL180\nN5\nF46\nN1\nL90\nF43\nS5\nR90\nF41\nW5\nN1\nF65\nE4\nN1\nW3\nF92\nN5\nF33\nR90\nS5\nL90\nW1\nR180\nL90\nS5\nF27\nR90\nN4\nR90\nF43\nE5\nS2\nF68\nN5\nR90\nF68\nR180\nS2\nE2\nS3\nF41\nL180\nE3\nR90\nF73\nR90\nN1\nL180\nN3\nL180\nW3\nS1\nR180\nN3\nF26\nN5\nF27\nL90\nF30\nR180\nN4\nR90\nE5\nN1\nF70\nE1\nL90\nN3\nF100\nL90\nE5\nL90\nS2\nF85\nW5\nR90\nF85\nE3\nR90\nE5\nF41\nR180\nS1\nL90\nF93\nS1\nF7\nN3\nR270\nW4\nS1\nF47\nR90\nN2\nW4\nF21\nS1\nW1\nF44\nL180\nE5\nN3\nW4\nF11\nL180\nE2\nF36\nW4\nF34\nR90\nF30\nN1\nE1\nF21\nN4\nF59\nE3\nF33\nN1\nL180\nW1\nR90\nE3\nF84\nW3\nR90\nW1\nN1\nR90\nF27\nS3\nL90\nN4\nE3\nF97\nN3\nF30\nW3\nF77\nE5\nF1\nR90\nF96\nE5\nN5\nW2\nL90\nS1\nF46\nN4\nF41\nW5\nL90\nS5\nF79\nR90\nF32\nS3\nR90\nF5\nL90\nE1\nR180\nW2\nN3\nL90\nS1\nR90\nW1\nF78\nW5\nN2\nE2\nR90\nS4\nS2\nE4\nF59\nR270\nW2\nL180\nS3\nR90\nW2\nF41\nN3\nF21\nL270\nF73\nN3\nE4\nL90\nE3\nF97\nE5\nN4\nW4\nF42\nW5\nS3\nR180\nN1\nF56\nE2\nF23\nR90\nF37\nL90\nS5\nW5\nR270\nE4\nF43\nW4\nR90\nE3\nN2\nR90\nS4\nL90\nN5\nF52\nE3\nL90\nF18\nF89\nL90\nW4\nF18\nE1\nL90\nE2\nF40\nF44\nR90\nN5\nR90\nS1\nL90\nF19\nN5\nL180\nN5\nW3\nN4\nF73\nR90\nE5\nR180\nF86\nE5\nS5\nF71\nW4\nF76\nS2\nR180\nS1\nL90\nS2\nF67\nR90\nN5\nE1\nF100\nS3\nW3\nN5\nR90\nF66\nL90\nE1\nL90\nW1\nF93\nS2\nF62\nF31\nL180\nF20\nR180\nF23\nW3\nF53\nW3\nR90\nE5\nR90\nN3\nR90\nE5\nF11\nW4\nR90\nW2\nR180\nE3\nN4\nE3\nF88\nL90\nN2\nW1\nR90\nF13\nN5\nW4\nF7\nS4\nW3\nF34\nE5\nS3\nF32\nF27\nN4\nF49\nS1\nF86\nS1\nF91\nS3\nF80\nR90\nS5\nE2\nL90\nF30\nW1\nR180\nW3\nS1\nR90\nF78\nW5\nN3\nR90\nS4\nF47\nF55\nN5\nL90\nF86\nS3\nE3\nF45\nS1\nF78\nS3\nF37\nE2\nF14\nL180\nE3\nF49\nN1\nL180\nF42\nF3\nN5\nE5\nF96\nS2\nL90\nF27\nE5\nS3\nW3\nS5\nF73\nN1\nW5\nS4\nL90\nW1\nS3\nW3\nR90\nE3\nL90\nF44\nL90\nR180\nF89\nW3\nR180\nF34\nE1\nF35\nN5\nR90\nN5\nF68\nL90\nF82\nS4\nF36\nW2\nS1\nS3\nR90\nN5\nE5\nF18\nR180\nS1\nF87\nR90\nF34\nR180\nN5\nE4\nF12\nE4\nL90\nS3\nE1\nS5\nW2\nF16\nE2\nF15\nN3\nW1\nF17\nS5\nL180\nF60\nN3\nF75\nR90\nF30\nE4\nR90\nF90\nS2\nF13\nE2\nF3\nE1\nF60\nS5\nE2\nF74\nL90\nS3\nW3\nF57\nN4\nE2\nF33\nS4\nF64\nN5\nL90\nF29\nN5\nS2\nR90\nF46\nS4\nE5\nL90\nF68\nW5\nS2\nR180\nF27\nW3\nL90\nS5\nR90\nE1\nR180\nF100\nS4\nW3\nR90\nE1\nS4\nW5\nF20\nW1\nN1\nR90\nE1\nN4\nE1\nF54\nN3\nF77\nL270\nN1\nF26\nN4\nE5\nS5\nN1\nF98\nE4\nF52\nW1\nF6\nR180\nN1\nF31\nW3\nN2\nF100\nL180\nE3\nF43\nR180\nE4\nF8\nL90\nW3\nL270\nN2\nR90\nN2\nE4\nL90\nE5\nE4\nS3\nF89\nL180\nS3\nN5\nR90\nF53\nF43\nR180\nE5\nN5\nF88\nW1\nE4\nL90\nW2\nN5\nF75\nL90\nE1\nS4\nF65\nN3\nW3\nF88\nE2\nS3\nE2\nN2\nR90\nS2\nF98\nN4\nS2\nF13\nR90\nN3\nF74\nR90\nF56\nS2\nE3\nS4\nF72\nN2\nR90\nF21\nE4\nN4\nL90\nF72\nL90\nN1\nN2\nF61\nW2\nL90\nF28\nS3\nW5\nS5\nF81\nS1\nE5\nN3\nF49\nN1\nF4\nN3\nF78\nE1\nF81\nN3\nW4\nF12\nL90\nS3\nE4\nF2\nW2\nR90\nS1\nW2\nF40\nS1\nW1\nW4\nN4\nL90\nN2\nE1\nL180\nN5\nF30\nL90\nN3\nF84\nW1\nF6\nS3\nF72\nN2\nW4\nS4\nW4\nE5\nL90\nL90\nN4\nS2\nF19\nN1\nW5\nN4\nL90\nN2\nF54\nL90\nW4\nF96\nN4\nR180\nS2\nF53\nR90\nS3\nE5\nN5\nL180\nE5\nS4\nL90\nN5\nL90\nE3\nL90\nF63\nS2\nL90\nF35\nN2\nF52\nW1\nL90\nF94\nN5\nE5\nR270\nN2\nR180\nS2\nE2\nN2\nS3\nF86\nN1\nF54\nN1\nL90\nW2\nR90\nE4\nR90\nN3\nR90\nF30\nS4\nF98\nW2\nS5\nR90\nN5\nW2\nS1\nF36\nS3\nR90\nS1\nF84\nS5\nN5\nL180\nF16\nN1\nF55\nL90\nN4\nS2\nS3\nR90\nS5\nW5\nN1\nE4\nS5\nW1\nL180\nF100\nE4\nS3\nE3\nN3\nE1\nL90\nW4\nF60\nW5\nL270\nW1\nS2\nL90\nR90\nF52\nS1\nW3\nN4\nF30\nE2\nF9\nF87\nS5\nR90\nS4\nW5\nR180\nS3\nE4\nS4\nL90\nW3\nF94\nF85\nR90\nE4\nW2\nS2\nL180\nW4\nF28\nE3\nN5\nF53")
(def input (parse-12 raw-input))

(def right-rotations {:up    :right
                      :right :down
                      :down  :left
                      :left  :up})
(def rotations {:turn-right right-rotations
                :turn-left  (set/map-invert right-rotations)})


(def initial-state {:heading  :right
                    :position [0 0]})

(defn factor
  [n move]
  (mapv #(* n %) move))

(defn next-move
  [state heading delta]
  (update state :position c/add (factor delta (c/moves->coords heading))))

(defn turn
  [state sens amount]
  (update state :heading #(let [n (inc (/ amount 90))]
                            (last (take n (iterate (rotations sens) %))))))

(comment
  (turn initial-state :turn-right 90)
  (let [n (/ 90 90)
        sens :right]
    (last (take (inc n) (iterate (rotations sens) :right))))
  )

(defn part-1
  [input]
  (let [last-point (reduce
                     (fn [{:keys [_ heading] :as state} [move delta]]
                       (cond
                         (#{:up :down :right :left} move) (next-move state move delta)
                         (#{:forward} move) (next-move state heading delta)
                         (#{:turn-left :turn-right} move) (turn state move delta)))
                     initial-state
                     input)]
    (c/manhattan-distance [0 0] (:position last-point))))

(comment
  (part-1 example)
  (part-1 input)
  )

