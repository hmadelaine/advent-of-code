(ns advent-of-code-2020.day-13
  (:require [clojure.string :as str]
            [utils.solve-utils :as su :refer [->int]]))

(def parse-buses (fn [s]
                   (map ->int (remove #{"x"} (str/split s #",")))))

(defn parse-13
  [raw]
  (let [[earliest-time buses] (str/split-lines raw)
        earliest-time (->int earliest-time)
        buses (parse-buses buses)]
    [earliest-time buses]))

(defn parse-13-2
  [raw]
  (let [[_ buses] (str/split-lines raw)
        buses (into {} (comp
                         (map-indexed (fn [i b]
                                        (when-not (= "x" b)
                                          [i (->int b)])))
                         (filter identity)) (str/split buses #","))]
    buses))


(def raw-example "939\n7,13,x,x,59,x,31,19")
(def example (parse-13 raw-example))
(def raw-input "1000303\n41,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,37,x,x,x,x,x,541,x,x,x,x,x,x,x,23,x,x,x,x,13,x,x,x,17,x,x,x,x,x,x,x,x,x,x,x,29,x,983,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,19")



(comment

  )


(defn earliest-departure
  [earliest-time bus]
  (loop [time 0]
    (let [t (+ time bus)]
      (if (>= t earliest-time)
        [(- t earliest-time) bus]
        (recur t)))))

(defn part-1
  [raw]
  (let [[earliest-time buses] (parse-13 raw)]
    (apply * (apply min-key first (map (partial earliest-departure earliest-time) buses)))))

(defn part-2
  [raw]
  (let [input (parse-13-2 raw)]
    (loop [t 100000000000000]
      (let [values (map (fn [[^Long d ^Long b]] (mod (+ t d) b)) input)]
        (prn t)
        (if (every? zero? values)
          t
          (recur (inc t)))))))

(comment

  (mod (+ (mod 1068781 7) 0) 7)
  (mod (+ (mod 1068781 13) 1) 13)
  (mod (+ (mod 1068781 59) 4) 59)
  (mod (+ (mod 1068781 31) 4) 59)
  (mod (+ (mod 1068781 19) 4) 59)


  (part-1 raw-example)
  (part-1 raw-input)

  (part-2 raw-example)
  (part-2 raw-input)
  (earliest-departure 59 944)
  (map #(mod % 3) (vals (parse-13-2 raw-input)))

  (map (fn [[f s]]
         [(- s f) s]) (parse-13-2 raw-example))

  (loop [[7 13 59 31 19]]
    )

  )