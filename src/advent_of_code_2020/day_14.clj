(ns advent-of-code-2020.day-14
  (:require [clojure.string :as str]
            [utils.solve-utils :refer [->int]]))


(def mask "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X")
(def raw-input "mask = 11X01X101X10000110110101X100000011XX\nmem[30904] = 804\nmem[25640] = 58672415\nmem[44254] = 829902099\nmask = 0100X100101000X0X01100X011100X000011\nmem[16446] = 3614672\nmem[60345] = 12470188\nmem[56197] = 3362\nmem[16887] = 516\nmem[15481] = 98479236\nmem[35964] = 5253451\nmem[6365] = 7426\nmask = 110X0X00100110101011110X1101100X1000\nmem[8832] = 13005766\nmem[55115] = 950\nmem[41989] = 552\nmem[61913] = 823\nmem[20675] = 22741\nmem[63918] = 733663\nmem[27098] = 12221134\nmask = 1101000X0011100000101XX0000X1100XX00\nmem[6537] = 8038445\nmem[1980] = 10059427\nmem[14520] = 4008996\nmask = 00X11X000000X000X1100011000110001000\nmem[34817] = 444921\nmem[2819] = 5673\nmem[22931] = 1528224\nmem[3026] = 5672\nmask = 1X0000001111X0X1111X1000100X0X0X10X0\nmem[35040] = 132191053\nmem[10721] = 754223\nmem[56671] = 2754806\nmem[30791] = 10367\nmem[19792] = 147\nmem[10716] = 44773\nmem[33657] = 364\nmask = 0X00000XX00X0X01101111000111010X11XX\nmem[54264] = 252\nmem[20924] = 4662\nmem[30011] = 13994357\nmem[32315] = 3582763\nmem[49802] = 55546\nmem[60634] = 12285193\nmem[35945] = 423\nmask = 010000X010000001100X100X1X0001110110\nmem[54639] = 1114347\nmem[14221] = 648\nmem[48913] = 130830803\nmask = 0111100011011X0110000X1X01X101100000\nmem[54240] = 49462136\nmem[30740] = 2366\nmem[65284] = 4006\nmem[6233] = 507389094\nmem[19877] = 141\nmask = 110110X0X001010X1100010XX01101111X10\nmem[42920] = 210585\nmem[1758] = 291573\nmem[11740] = 1753119\nmem[62804] = 1121\nmem[54639] = 872\nmem[35142] = 1916057\nmask = 100X10000001100101000011XX10101000X0\nmem[40548] = 560555\nmem[62482] = 7506653\nmem[49966] = 575\nmem[30575] = 1220439\nmem[28113] = 5563\nmem[41047] = 65293771\nmask = X1010000X1XX01X110100X01101X01010XX0\nmem[1366] = 71278590\nmem[45316] = 873708\nmem[43138] = 2041\nmem[46594] = 607\nmem[15467] = 127558\nmask = X100100000100000101X00011X11X0110001\nmem[549] = 1814393\nmem[59563] = 865\nmem[34570] = 175093\nmem[21815] = 841005500\nmem[46823] = 517\nmem[62134] = 2068575\nmask = XX0000X110000101101X00X1X111X111X110\nmem[9644] = 81473\nmem[55094] = 103187\nmask = 010X00001XXX0X01101XX001101111X1X010\nmem[43476] = 39027003\nmem[25072] = 225\nmem[37182] = 1497390\nmem[22440] = 170498\nmask = 0XXXX0001000001110X1101X111X10101000\nmem[16871] = 92184017\nmem[22567] = 9298973\nmem[38861] = 3583194\nmem[46161] = 3411721\nmem[55420] = 382\nmem[25462] = 174027\nmem[41626] = 275108\nmask = 1X01000X11X0X11110100X11110X0101X001\nmem[21342] = 14984152\nmem[52268] = 435036\nmem[23071] = 13378\nmem[58137] = 63436471\nmem[23072] = 4203436\nmem[38405] = 479843353\nmask = 0000X001100X0001X011X10X010101101011\nmem[52773] = 64752275\nmem[3789] = 898152\nmem[12600] = 7124813\nmask = 100110XX000110X001101X10100XX000000X\nmem[58350] = 693\nmem[2651] = 163278\nmem[24130] = 13797006\nmem[62784] = 23369612\nmem[6912] = 1301\nmask = 11010X00100XX101101111111111X101101X\nmem[53888] = 194671\nmem[3670] = 8817412\nmem[58068] = 16156\nmem[64215] = 11891\nmask = 01X1000XX101XX0110100001X1000X010X0X\nmem[59576] = 237039\nmem[30009] = 957\nmem[60486] = 961\nmem[38241] = 18092946\nmem[32300] = 81527\nmem[43235] = 361326555\nmask = X01110100001110X0000X0X11X0X110X00X1\nmem[34347] = 63169660\nmem[23916] = 2842\nmem[14819] = 2078726\nmem[38982] = 54808\nmem[36227] = 494\nmem[17114] = 7834\nmask = 0XX10XX0000011011111X01111100010XX00\nmem[21874] = 304925\nmem[56696] = 950088\nmask = 1100X0X110111011XX111101X10X110X1111\nmem[59256] = 178304\nmem[10638] = 155668\nmem[6818] = 58192\nmask = 01X1000011110X11X010101110101X000100\nmem[23723] = 7575\nmem[62088] = 17274981\nmem[6864] = 21516071\nmask = 11X10000X101X00X10110X01111110X00111\nmem[31049] = 491767185\nmem[27270] = 7118669\nmem[4580] = 65758\nmem[1039] = 14254\nmem[32658] = 9716\nmem[5162] = 842785\nmem[24455] = 5673467\nmask = 110X0X0X110X1000XX1X0000001010000X11\nmem[46755] = 6502241\nmem[7424] = 20934554\nmem[23337] = 678828183\nmem[64828] = 805263\nmask = X10X00001111X0101011010110X010000100\nmem[5481] = 288565\nmem[10519] = 1209\nmem[5509] = 4081599\nmask = 10X000001XX01101X111XX00101111000111\nmem[31289] = 23840740\nmem[60675] = 2716\nmem[549] = 32627037\nmem[56788] = 47252\nmem[42610] = 1204108\nmask = 00XXXX00100000X110010010101110100111\nmem[38288] = 675950673\nmem[4580] = 3440\nmem[29394] = 633235\nmem[28343] = 8737\nmask = 1101100000001X0010X10110X0011X111101\nmem[56644] = 40733165\nmem[15071] = 32842495\nmem[42317] = 31612899\nmem[11060] = 31791\nmem[32740] = 144488\nmem[59117] = 84496\nmem[48853] = 2789\nmask = 1001X0000X1X10000011010000011XX01001\nmem[41834] = 13008\nmem[44254] = 506032399\nmem[47240] = 104934982\nmem[59603] = 908964\nmem[27127] = 7816195\nmem[9442] = 6967530\nmem[6049] = 130571712\nmask = 11010000101110X0X11X01100XXX11X01001\nmem[55552] = 538\nmem[45294] = 5968\nmem[57770] = 5074932\nmem[14709] = 620\nmask = 1X000X0011001X01X1111XX1111100010010\nmem[22567] = 111589993\nmem[27442] = 7345501\nmem[1366] = 693\nmem[10716] = 67389\nmem[34007] = 7669704\nmem[42994] = 424182225\nmask = 00X1101000011X010X0X1001100000110100\nmem[17366] = 1744816\nmem[49802] = 26865386\nmem[33107] = 315222523\nmem[22972] = 1000\nmem[40792] = 296402778\nmem[15571] = 119687927\nmask = 0111X0X0X10111X110X00010XX0101X0010X\nmem[28584] = 20338342\nmem[60490] = 7353702\nmem[47627] = 238170\nmem[65284] = 98512808\nmem[21880] = 209876\nmem[45316] = 17898\nmem[23937] = 67926\nmask = 110110000011X000X0XX0000X110000XX000\nmem[6049] = 603777\nmem[35598] = 8391793\nmem[36909] = 87535271\nmem[39369] = 15004\nmask = 01010X10110010011111X110100000010X0X\nmem[7560] = 1428145\nmem[60796] = 150097\nmem[7572] = 402\nmem[26354] = 156030\nmem[38982] = 792942\nmem[43594] = 29726\nmem[62747] = 70726731\nmask = 1X01X00000X1X000X01X1X0110X010000001\nmem[21629] = 9076643\nmem[52203] = 6306\nmem[22984] = 248989\nmem[24904] = 5381\nmask = X1X10XX00000011X11010X0101XX1X111001\nmem[46483] = 15635\nmem[59552] = 13183491\nmem[41572] = 5477\nmem[44254] = 20911415\nmem[13067] = 3094\nmem[6827] = 2156\nmem[13117] = 9019\nmask = 1101000X0X0X1001X1110011100000010101\nmem[7097] = 11433841\nmem[64940] = 1502724\nmem[63191] = 27213771\nmask = X1010X000000XXX11X1X111111X10011100X\nmem[65480] = 13438723\nmem[33632] = 81817290\nmem[7966] = 28099266\nmem[52682] = 35609347\nmem[14221] = 73182911\nmask = 0101X000X110X001101000011X111100111X\nmem[41444] = 7685\nmem[2096] = 94796\nmem[18256] = 71874\nmask = 100X00000011XX000X1110011001X1101010\nmem[3789] = 14774133\nmem[62406] = 226\nmem[4451] = 136465\nmask = 010100000X0001XX1XX1X01101011X0010X0\nmem[3660] = 3970041\nmem[52464] = 1608598\nmem[9853] = 221778\nmem[50180] = 200623\nmask = 0101X000X000X00110X111X111X110111011\nmem[36227] = 6953473\nmem[3660] = 207401\nmem[549] = 66817\nmem[59807] = 3577\nmem[13761] = 4685324\nmem[21629] = 53428878\nmem[46431] = 1686\nmask = 01001000XX1X011110111101100X101XX01X\nmem[38241] = 239351742\nmem[30851] = 116\nmem[57771] = 10156\nmem[52691] = 5553451\nmask = 1X01000011X1X0011011001111X100100110\nmem[1980] = 934\nmem[28565] = 22842096\nmem[65272] = 1283\nmem[3239] = 843939\nmem[17041] = 507297277\nmask = 01001000011101X010111011X001X00X1100\nmem[44712] = 2005\nmem[47569] = 627819534\nmem[22757] = 4903910\nmask = 1X01000000001101101101100X1X0101100X\nmem[40808] = 134469\nmem[38359] = 5460\nmem[12034] = 124445441\nmask = 00011000000X0000011010X10XX01000X001\nmem[27442] = 85520780\nmem[39169] = 2706511\nmem[63093] = 65082\nmem[38333] = 21146\nmem[22757] = 9565\nmem[55395] = 575\nmem[30420] = 6655\nmask = 1101000X00X1X00X0011000101X001011XX1\nmem[13633] = 2443\nmem[40637] = 14161841\nmask = 0100X00001100000101XX10101011X001101\nmem[14221] = 763800\nmem[36636] = 74643\nmem[60609] = 942\nmask = 0XX10000100X00XX10011X10101010110X10\nmem[26500] = 14129085\nmem[20357] = 419\nmem[7587] = 53492\nmem[60812] = 914199\nmask = 1101X000X0XX1XXX101111111011110010X0\nmem[21629] = 96321089\nmem[31049] = 5542520\nmem[28565] = 2711108\nmask = 100110X0000110000XX01000100X0X00XXXX\nmem[14509] = 3728576\nmem[34266] = 564529794\nmem[39304] = 476496\nmem[4938] = 41380863\nmask = XX010000010X01X110101X101100X1010010\nmem[24130] = 1934\nmem[58734] = 10309\nmem[40221] = 1598\nmem[15977] = 184066469\nmem[21816] = 11450486\nmask = 0X00X100X0001001101X00X110100X101X10\nmem[34141] = 57556\nmem[30791] = 1291662\nmem[58838] = 7014\nmem[24341] = 3067\nmask = X10000X0100001011XX100X1001011110010\nmem[4394] = 987\nmem[6958] = 251375845\nmem[30011] = 3732389\nmask = 11XX000010011010111X0100XXX011010110\nmem[52203] = 68721\nmem[45364] = 59466717\nmem[31045] = 21598944\nmem[50395] = 4231\nmem[51592] = 8111\nmem[63504] = 8329134\nmem[58734] = 754\nmask = 110100001XX110XX1X1101110XX11X0X0001\nmem[16279] = 811\nmem[10716] = 55019036\nmem[51529] = 24243\nmem[14709] = 3794\nmask = 1X010100XX00110111111X0X101X11101X1X\nmem[46385] = 264566\nmem[15571] = 3070\nmem[36393] = 614536\nmem[47391] = 115102\nmem[16612] = 1449\nmem[39001] = 7341623\nmask = X10X1000X01000001011XX0010X000110001\nmem[31360] = 245394\nmem[10346] = 2844\nmem[59256] = 26640464\nmem[40731] = 9549\nmask = 110X0000XX0XX001111111X11101X00X1000\nmem[20489] = 3679\nmem[36850] = 48368\nmem[43476] = 9536\nmem[42317] = 794736\nmem[64360] = 3041633\nmem[20374] = 182769351\nmask = 11XX000X1X111X1X1X11011110XX11001101\nmem[34207] = 918112464\nmem[30124] = 22887363\nmem[23337] = 5194\nmem[64654] = 2482244\nmem[13633] = 52721017\nmem[53541] = 611\nmask = X0X1101000011X0XX00X100X0X00X1010011\nmem[35029] = 31476\nmem[29797] = 8305\nmem[11060] = 1711\nmem[3509] = 370841\nmask = 1100000X11010X0X111X1X1X0000XX001010\nmem[12166] = 2826105\nmem[13096] = 3515\nmem[10844] = 31293\nmem[47528] = 4666248\nmem[30022] = 2602\nmask = 1101X00100011001X01X1001X0X010001010\nmem[7409] = 228726\nmem[44042] = 5963265\nmem[7966] = 6413\nmem[32315] = 2668\nmask = 110X000X00X1X000X0111001100111000001\nmem[21629] = 372179\nmem[12594] = 3250987\nmem[51528] = 11238\nmem[32964] = 432\nmask = 11X10000001X000X0010X0111X0110001001\nmem[22646] = 1609877\nmem[2120] = 252\nmem[12600] = 90688426\nmem[26126] = 5793298\nmem[52659] = 17234\nmem[59801] = 14287590\nmem[7917] = 16520660\nmask = 11X00000110X10011X110X0111011101X001\nmem[38536] = 60532\nmem[38879] = 61201\nmem[9818] = 9333026\nmem[48276] = 10329920\nmask = 110X1000X01X00001011X0X10X0110001XX1\nmem[39369] = 5666\nmem[12594] = 361449\nmem[35634] = 1157722\nmem[11186] = 3299696\nmem[15380] = 5526081\nmem[4541] = 328\nmask = 010X0X00X0X0X00X10X1000010100011X111\nmem[13134] = 37334\nmem[60703] = 1171\nmem[6504] = 9516\nmem[46431] = 246333598\nmem[26155] = 21043\nmem[55195] = 125930\nmem[58790] = 11080295\nmask = 10011000X0011X0001001X0XX1XXX00110X1\nmem[344] = 499699\nmem[56449] = 20803473\nmem[60345] = 52387\nmem[46126] = 1697507\nmem[19090] = 52858555\nmem[20723] = 26263343\nmem[63191] = 3\nmask = X00110000001X00X01X01001101XX0000XX0\nmem[33281] = 489201\nmem[28482] = 1249916\nmem[44260] = 977965\nmem[45827] = 90243487\nmask = X10100000111X1X11010XX11X001X1X10010\nmem[39556] = 151117\nmem[42947] = 34920\nmem[59025] = 2013682\nmem[57008] = 4913\nmem[6773] = 406\nmask = X10X00001X010XX1111010XX100X00101010\nmem[57004] = 33597\nmem[24038] = 57028199\nmem[8014] = 10576959\nmem[30740] = 118961\nmem[32106] = 2411\nmem[43274] = 3984\nmem[3179] = 716\nmask = 1101000X00001X011X11X01011XX1011110X\nmem[20572] = 8223195\nmem[3670] = 309\nmem[57224] = 3646\nmask = 0101000X00X00X01111X1111110X00100X10\nmem[27896] = 2498\nmem[10519] = 629600\nmem[35133] = 15030807\nmask = 0X01X0X00X0X01111010110X10110X010100\nmem[62482] = 20472364\nmem[64016] = 8001446\nmem[61547] = 12063\nmask = 11010XX011100X0110X00X1110010111X0X0\nmem[59444] = 2341\nmem[10049] = 53787074\nmem[50395] = 3290\nmask = 100110100X01100X001X100110001000010X\nmem[4227] = 24426052\nmem[16502] = 23444792\nmem[22294] = 3709904\nmem[10773] = 12722\nmask = X10010000X1X0X0XX01110110X011100XX01\nmem[7257] = 1613\nmem[7489] = 1856\nmem[24160] = 61998242\nmem[56644] = 8079639\nmem[59111] = 151253\nmask = 11010000000100011XX101111X0100001X01\nmem[52224] = 252594\nmem[10569] = 79481269\nmem[12724] = 627\nmask = 11X0000011X1100111111X0X1XX10110X000\nmem[48476] = 44915498\nmem[28502] = 987462\nmem[28907] = 17654\nmem[30984] = 527202957\nmask = 110X000000X0110110101110XX1101111001\nmem[14737] = 57864548\nmem[63545] = 62156\nmask = 1XX1100000010X00X100XX01X11101100110\nmem[23890] = 27294\nmem[50906] = 46891220\nmem[34574] = 597\nmem[20622] = 2981498\nmem[10272] = 303\nmask = 01X1000XX00010X1101101X1X01110XX1X11\nmem[18342] = 206778755\nmem[10346] = 412\nmem[19158] = 20387791\nmask = 010X00001X100111101X1X1X1X11X10101X0\nmem[1901] = 158999\nmem[41794] = 54125676\nmem[22931] = 89400316\nmem[19522] = 833\nmask = 110XX000001X00001011X001111100010100\nmem[21629] = 1212\nmem[5305] = 9321884\nmem[23237] = 31772642\nmem[15481] = 30790\nmask = 01001000011X01X1XXX10001XXX11X101011\nmem[50281] = 1031850\nmem[55170] = 152566\nmem[62088] = 2825\nmem[55498] = 39448855\nmask = 010X0000XX0XX0011011000100X110111XXX\nmem[55115] = 91\nmem[27127] = 204950\nmem[29412] = 99341996\nmem[30840] = 32617407\nmem[52681] = 17904254\nmem[28755] = 910986\nmask = 010000X0111X0X01X01000011X101X000X10\nmem[61913] = 86487\nmem[23890] = 1409414\nmem[18153] = 122261\nmask = 01X1000010001011X01100X1XX1110101010\nmem[32043] = 1038190\nmem[35909] = 154\nmask = 0111X00XXXX0101110XX010000X11X100001\nmem[37916] = 4881390\nmem[13226] = 443002\nmem[22646] = 64137936\nmem[31097] = 94510556\nmem[3570] = 119428\nmem[3311] = 292624\nmask = X10X1000000XX000X011011110X1000XX101\nmem[25147] = 646727\nmem[7075] = 143058300\nmem[50395] = 661\nmem[12058] = 11478790\nmask = X0011000X0111000X010110100X1X10010X0\nmem[38241] = 752832217\nmem[33284] = 1355491\nmem[32536] = 344526925\nmem[6345] = 989912\nmask = 01001X0001X1010100111111100X110X1101\nmem[16833] = 1778173\nmem[22135] = 3254\nmem[43235] = 84325\nmask = X10X00X01100100111111111110X1XX11XX0\nmem[26414] = 3191\nmem[62592] = 1205580\nmem[24576] = 82621\nmem[65177] = 4041591\nmem[20140] = 2396\nmask = 11X010X0X0100X011X11XX01110000001101\nmem[23934] = 79377022\nmem[22183] = 77353599\nmem[4451] = 229619\nmem[9249] = 2021235\nmem[47560] = 472016181")
(str/split raw-input #"(\n)?mask = ")
(defn parse-mask
  [raw-mask]
  (->
    (reduce
      (fn [acc bit]
        (condp = bit
          \X (-> acc
                 (update :and str "1")
                 (update :or str "0"))
          \1 (-> acc
                 (update :and str "1")
                 (update :or str "1"))
          \0 (-> acc
                 (update :and str "0")
                 (update :or str "0"))
          ))
      {:and ""
       :or  ""}
      raw-mask)
    (update :and #(Long/parseLong % 2))
    (update :or #(Long/parseLong % 2))))

(defn apply-mask
  [mask n]
  (let [{:keys [and or]} mask]
    (->> n
         (bit-and and)
         (bit-or or))))

(defn parse-program
  [prog]
  (let [[raw-mask & instrs] (str/split-lines prog)
        mask (parse-mask raw-mask)
        instructions (map #(map ->int %) (map #(re-seq #"\d+" %) instrs))]
    {:mask mask
     :ops  instructions}))

(defn parse-input
  [raw-input]
  (->> (str/split raw-input #"(\n)?mask = ")
       (remove str/blank?)
       (map parse-program)))

(defn part-1
  [prog]
  (reduce +
          (vals
            (reduce
              (fn [acc {:keys [mask ops]}]
                (reduce (fn run-prog [acc [mem n]]
                          (assoc acc mem (apply-mask mask n)))
                        acc
                        ops))
              {}
              prog))))

(comment

  (part-1 (parse-input raw-input))
  (part-1 (parse-input "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X\nmem[8] = 11\nmem[7] = 101\nmem[8] = 0"))

  (apply-mask (parse-mask "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X") 101)
  (apply-mask (parse-mask "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X") 0)

  (parse-program "11X010X0X0100X011X11XX01110000001101
  mem[23934] = 79377022
  mem[22183] = 77353599
  mem[4451] = 229619
  mem[9249] = 2021235
  mem[47560] = 472016181")

  )





