(ns advent-of-code-2020.day-23
  (:require [com.rpl.specter :as sp]
            [clojure.string :as str]))


(def game {:current-cup       7
           :cups              [7 9 2 8 4 5 1 3 6]
           :pick-up           ()
           :destination-label nil})


(defn current-position
  [game]
  (let [{:keys [current-cup cups]} game]
    (.indexOf cups current-cup)))

(defn insert-pick-up
  [current-cup pick-up cups]
  (sp/setval [(sp/srange (inc current-cup) (inc current-cup))] pick-up cups))


(defn remove-pick-up
  [cups pick-up]
  (remove (set pick-up) cups))

(defn select-pick-up
  [current-cup cups]
  (let [extended-cups (take (* 2 (count cups)) (cycle cups))]
    (sp/select-one [(sp/srange (inc current-cup) (+ 4 current-cup))] extended-cups)))


(defn picks-cup
  "The crab picks up the three cups that are immediately clockwise of the current cup.
   They are removed from the circle;
   cup spacing is adjusted as necessary to maintain the circle."
  [game]
  (let [{:keys [cups]} game
        current-position (current-position game)
        pick-up (select-pick-up current-position cups)
        next-cups (remove-pick-up cups pick-up)]
    (-> game
        (assoc :cups next-cups)
        (assoc :pick-up pick-up))))


(defn destination-cup
  "The crab selects a destination cup :
   The cup with a label equal to the current cup's label minus one.
   If this would select one of the cups that was just picked up,
   the crab will keep subtracting one until it finds a cup that wasn't just picked up.
   If at any point in this process the value goes below the lowest value on any cup's label,
   it wraps around to the highest value on any cup's label instead."
  [game]
  (let [{:keys [cups current-cup]} game
        next-label (dec current-cup)
        next-label (if (apply < next-label (sort cups))
                     (apply max cups)
                     (loop [target next-label
                            [f & rcups] cups]
                       (if f
                         (if (= f target)
                           target
                           (recur target rcups))
                         (recur (dec target) cups))))]
    (assoc game :destination-label next-label)))


(defn places-cups
  "The crab places the cups it just picked up so that they are immediately clockwise of the destination cup.
   They keep the same order as when they were picked up."
  [game]
  (let [{:keys [destination-label cups pick-up]} game
        destination-position (.indexOf cups destination-label)
        next-cups (insert-pick-up destination-position pick-up cups)]
    (-> game
        (assoc :cups next-cups))))

(defn next-current-cup
  "The crab selects a new current cup:
  the cup which is immediately clockwise of the current cup."
  [game]
  (let [position (current-position game)
        next-position (mod (inc position) 9)
        next-cup (nth (:cups game) next-position)]
    (assoc game :current-cup next-cup)))

(defn move
  [game]
  (-> game
      (picks-cup)
      (destination-cup)
      (places-cups)
      (next-current-cup)))

(defn part-1
  [game]
  (let [final-game (first (take 1 (drop 100 (iterate move game))))
        cups (:cups final-game)
        _ (prn cups)
        start (inc (.indexOf cups 1))]
    (str/join (take 8 (drop start (cycle cups))))))

(comment

  (part-1 game)

  )
