(ns advent-of-code-2020.day-19
  (:require [clojure.string :as str]
            [instaparse.core :as insta]
            [utils.solve-utils :as u]
            [instaparse.transform :as trans]))

(def raw-rules-example "0: 4 1 5\n1: 2 3 | 3 2\n2: 4 4 | 5 5\n3: 4 5 | 5 4\n4: \"a\"\n5: \"b\"\n")

(def parser (insta/parser (slurp "resources/2020-day-19.bnf")))

(def transformer {:PUZZLE  (fn [& args]
                             (apply merge args))
                  :RULE    (fn [& args]
                             (let [[id rules] args]
                               {id rules}))
                  :RULE-ID (fn [s] (u/->int s))
                  :RULES   (fn [& args]
                             (let [[l _ r] (partition-by #{"|"} args)]
                               (remove nil? [l r])))})

(trans/transform transformer (parser raw-rules-example))


(def graph '{0 [(4 1 5)],
             1 [(2 3) (3 2)],
             2 [(4 4) (5 5)],
             3 [(4 5) (5 4)],
             4 "a",
             5 "b"})

(defn find-pattern
  [id pattern]
  (let [rule (graph id)]
    (prn id rule pattern)
    (cond
      (#{"a" "b"} rule) (str pattern rule)
      (= 1 (count rule)) (mapv #(find-pattern % pattern) (first rule))
      (= 2 (count rule)) (let [[f s] rule]
                        (str (str/join (mapv #(find-pattern % pattern) f))
                             "|"
                             (str/join (mapv #(find-pattern % pattern) s)))))))

(find-pattern 1 "")

[(["a"]
  [([(["a"] ["a"]) (["b"] ["b"])] [(["a"] ["b"]) (["b"] ["a"])])
   ([(["a"] ["b"]) (["b"] ["a"])] [(["a"] ["a"]) (["b"] ["b"])])]
  ["b"])
 ()]
'[("a"
    (("a" "a") ("b" "b") ("a" "b") ("b" "a"))
    (("a" "b") ("b" "a") ("a" "a") ("b" "b"))
    "b")]

(loop [rule 0
       path []]
  (let [r (graph rule)]
    (cond
      (#{"a" "b"} r) (conj rule r)
      (vector? r) (let [[l r] r]
                    (map graph l))

      ))
  )