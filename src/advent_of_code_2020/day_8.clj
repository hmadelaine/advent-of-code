(ns advent-of-code-2020.day-8
  (:require [clojure.string :as str]))

(def raw-input "jmp +254\njmp +1\nacc +48\njmp +487\njmp +586\nacc -18\njmp +238\nacc +37\nacc -7\nacc +45\njmp +514\njmp +25\nacc -2\nacc +48\njmp +43\nacc +33\nacc -1\njmp +98\nacc +0\nnop +406\nacc +32\nacc +34\njmp -15\njmp -5\nacc +0\nnop +60\nnop +395\njmp -15\njmp +380\nacc -15\njmp +446\nacc +38\nacc +18\nacc -1\nacc +23\njmp +386\nnop +534\nacc +19\nacc -6\nacc +41\njmp +163\nacc +17\njmp +383\nacc -13\njmp +346\nacc +10\nacc -18\nnop +448\nacc +50\njmp +399\nacc +43\nacc +36\njmp +24\nacc -7\nacc +43\nnop +60\njmp +80\nnop +40\nnop +274\nacc -16\nacc +42\njmp +102\nacc +17\njmp +410\nacc -8\nacc +45\nacc +12\nacc +50\njmp +486\nacc +17\njmp +425\nacc +39\njmp +239\nacc +7\nacc +3\njmp +315\nacc +13\njmp +344\njmp +154\nacc +20\nacc +3\njmp +206\nacc -14\nacc +33\njmp +79\nacc +44\njmp +106\nacc +5\njmp +1\nacc -19\nacc +19\njmp +346\nacc +41\nacc +42\njmp +481\nacc -4\njmp +142\nacc +10\nacc -5\nacc +44\nnop +302\njmp +368\nnop +36\nacc +46\nacc +44\nnop +171\njmp +256\nacc +37\nnop -11\nacc +7\nnop -34\njmp -68\nacc +16\nacc -4\njmp +355\nacc +1\nacc +45\nacc -19\njmp +464\nacc +33\njmp +149\njmp +475\nnop -86\nacc -7\nacc -12\nacc +38\njmp +398\nacc +8\nacc +16\nacc +25\njmp +350\nacc +18\nacc +42\nacc +12\nacc +43\njmp +302\njmp +331\njmp +14\nacc -18\nacc +18\nacc +7\njmp +140\nacc +8\njmp +42\nacc +10\nacc +4\nacc +28\nacc +20\njmp +75\nacc -8\njmp +30\nacc +13\njmp +28\nacc -6\njmp -142\nacc +17\njmp -41\njmp +361\nacc +37\njmp +147\nnop +78\njmp +1\njmp -18\nacc +1\nnop +273\nacc +43\nacc +30\njmp +126\nacc +4\nacc +10\nacc +9\njmp -128\nacc -12\nacc -3\njmp +58\nacc +17\nacc +38\nacc +42\nacc -10\njmp +218\nacc -18\njmp +378\nacc -11\nacc +6\njmp -33\nacc -15\njmp +68\nacc -11\nnop +312\nacc +21\nacc +33\njmp -36\njmp +281\nacc +34\nacc +3\nnop -40\nacc -5\njmp +141\nacc +6\nacc -5\njmp +99\nacc -9\njmp +360\nacc -9\njmp +366\nacc -1\nnop -188\nacc +47\nnop -87\njmp +361\njmp -113\nacc +43\nacc +21\nnop +41\nacc +1\njmp -23\nacc +10\nnop -110\nacc -16\njmp +136\nacc +33\nnop +219\njmp -95\njmp +223\nacc -6\njmp +354\nacc +24\nacc +50\nacc +10\nacc +26\njmp +207\njmp -202\njmp -6\nnop +181\nacc -9\nnop +248\nacc +43\njmp +325\nacc +2\nacc +19\nacc +22\njmp +254\nacc +31\njmp +233\nacc -9\nacc +24\nacc +49\nacc +18\njmp +84\nacc -19\nacc +16\nacc +37\nacc +31\njmp +66\nacc +6\njmp +1\njmp +206\njmp +126\nacc +24\njmp +271\nacc +16\njmp +1\nacc -11\nacc -4\nnop +47\njmp +118\nnop +136\nacc +7\njmp +94\nacc +0\nacc +0\njmp +239\nacc -4\nacc +23\nacc +16\njmp +270\nacc +28\njmp -8\nacc +24\nacc -13\njmp +117\nacc +31\nacc -3\nacc +21\nacc -9\njmp +86\njmp +293\nnop -29\nacc -11\njmp -162\nacc +36\nacc -4\njmp +122\nacc -13\nacc -10\njmp -115\nacc +23\nacc +7\njmp -126\nacc +21\njmp -162\nacc +48\nacc +43\nacc +37\nnop -275\njmp -89\nnop +248\njmp +107\nacc +26\nacc -16\njmp +185\nacc +40\nacc +32\njmp +232\nacc +27\njmp +189\nnop +259\njmp +131\njmp +261\njmp +230\nacc -2\nacc +37\njmp +240\nnop +1\nacc -9\nacc +36\njmp -110\nacc +5\nacc +50\nacc +23\nacc -19\njmp +142\nacc +40\nacc -4\nacc +3\nnop +134\njmp -164\njmp +60\nacc +28\nacc +28\nacc +24\nacc -7\njmp +91\nacc +1\njmp -268\nacc +41\njmp -195\nacc -3\njmp +231\nacc +48\nacc +7\njmp -153\nacc +2\nacc -11\njmp +60\nnop -240\nnop -40\njmp -125\nacc -7\nacc +14\nacc +23\njmp -103\njmp +1\nacc +27\nacc +16\nacc -17\njmp -181\nacc +38\nacc -6\nacc +20\njmp -243\nacc +13\nacc +26\nacc +5\nacc +38\njmp -268\nacc -17\njmp -373\nacc -10\nacc -10\nnop +68\njmp -36\njmp -128\nacc -5\nacc -9\nacc +10\nacc +15\njmp +103\nacc -6\njmp +64\nacc +42\nacc +15\nacc -12\nacc -1\njmp -309\nnop +187\njmp -378\njmp -78\njmp +1\nacc +7\nacc +0\njmp -389\nacc -14\njmp -80\nacc -13\nacc +0\nacc +25\njmp -364\nacc -9\nacc -13\nacc +11\njmp +1\njmp -327\njmp +63\nacc +0\nnop -300\nacc +29\njmp -101\nnop -238\nacc +25\njmp -204\njmp -13\nacc +21\nacc +43\njmp -137\nacc +44\nacc +11\nacc -18\njmp -307\nacc -16\nacc +0\nacc -7\njmp +138\nacc +11\nacc +42\njmp -411\njmp -34\nacc +9\njmp -89\njmp +115\njmp -62\nacc -5\nacc +37\nacc +14\nacc +16\njmp +45\njmp +37\njmp -404\njmp -356\nacc -4\nacc +18\nacc -18\njmp -97\nnop +57\nacc -5\nnop -93\nacc +47\njmp -247\nacc +4\nacc -13\nnop -309\njmp -245\nacc +9\nacc -12\nacc +49\njmp -302\njmp +88\njmp -438\njmp -397\nacc -9\nnop -278\njmp -313\njmp +90\njmp -329\nacc +36\njmp -411\nacc +3\nacc +36\njmp -454\njmp -343\nnop -148\njmp -237\njmp -159\nacc +6\nacc -17\njmp -481\nacc +30\nacc -12\nacc +40\njmp -27\nacc -19\nacc +30\njmp -39\nacc -17\njmp -32\nacc +23\njmp -432\nacc -18\nnop -427\njmp +19\nacc -6\nnop +81\njmp +16\njmp -23\nnop +45\nacc +44\nnop -33\nacc +23\njmp -111\njmp -271\nacc +3\nacc +0\nacc +5\nacc -4\njmp +48\nacc +0\nnop -300\njmp -402\nacc +18\nacc +3\nacc +30\njmp -263\nnop -125\njmp +59\njmp -488\nnop -518\nacc +3\nacc -11\nacc +3\njmp -522\nacc +13\njmp +9\nacc +35\nacc +22\njmp -276\njmp +1\nacc -5\njmp -458\nacc -10\njmp -388\nnop -100\njmp -6\nacc -6\nnop -289\njmp -91\nacc +31\nacc +37\njmp -43\njmp -237\njmp -49\nacc +22\nacc +13\nnop -79\njmp -105\njmp +1\nacc -8\njmp -166\nnop -27\njmp -23\nacc -17\njmp -298\njmp -274\nacc +5\nacc +11\njmp -372\nacc +29\nnop -204\nacc -14\nacc +38\njmp -493\nacc +15\njmp -146\nacc -6\nacc +49\njmp -371\njmp -504\nacc +17\nnop -367\njmp -279\nacc +9\njmp -50\njmp -327\nacc +18\nacc +39\nacc +19\nacc +10\njmp -7\nnop -87\nacc +15\njmp -565\njmp -252\nacc -19\nacc +17\nacc +25\nnop -350\njmp -296\nacc +39\nnop -579\nacc +23\nacc +43\njmp +1")

(defn parse-raw-input
  [raw-input]
  (->> raw-input
       (str/split-lines)
       (map #(str/split % #"\s"))
       (mapv (fn [[op arg]]
               [(keyword op) (Integer/parseInt arg)]))))


(def input (parse-raw-input raw-input))

(def ops->fn
  {:acc (fn [acc arg index]
          [(+ acc arg) (inc index)])
   :jmp (fn [acc arg index]
          [acc (+ index arg)])
   :nop (fn [acc arg index]
          [acc (inc index)])})


(defn part-1
  [input]
  (loop [visited? #{}
         index 0
         accumulator 0]
    (cond
      (visited? index) [:infinite accumulator]
      (= index (count input)) [:answer accumulator]
      :else (let [[op arg] (get input index)
                  [acc next-index] ((ops->fn op) accumulator arg index)]
              (recur (conj visited? index) next-index acc)))))


(defn find-nops-jmps
  [input]
  (into []
        (comp
          (map-indexed
            (fn [i [op _]]
              (if (#{:jmp :nop} op)
                i)))
          (remove nil?))
        input))

(defn toggle-op-at
  [input at]
  (update input at (fn [[op arg]]
                     [({:nop :jmp
                        :jmp :nop} op) arg])))

(comment
  (part-1 input)

  (loop [[i & rest] (find-nops-jmps input)]
    (when i
      (if (part-1 (toggle-op-at input i))
        (recur rest)
        i)))



  (defn part-2
    [input]
    (reduce
      (fn [acc i]
        (let [input' (toggle-op-at input i)
              result (part-1 input')]
          (if result
            acc
            (reduced input'))))
      []
      (find-nops-jmps input)))
  )

(part-1 (toggle-op-at input 394))
(count input)

(part-2 (parse-raw-input "nop +0\nacc +1\njmp +4\nacc +3\njmp -3\nacc -99\nacc +1\njmp -4\nacc +6"))
