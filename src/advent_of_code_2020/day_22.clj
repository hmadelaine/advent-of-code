(ns advent-of-code-2020.day-22
  (:require [clojure.string :as str]
            [utils.solve-utils :as u])
  (:import (clojure.lang PersistentQueue)))

(def raw-example "Player 1:\n9\n2\n6\n3\n1\n\nPlayer 2:\n5\n8\n4\n7\n10")
(def raw-input "Player 1:\n21\n22\n33\n29\n43\n35\n8\n30\n50\n44\n9\n42\n45\n16\n12\n4\n15\n27\n20\n31\n25\n47\n5\n24\n19\n\nPlayer 2:\n3\n40\n37\n14\n1\n13\n49\n41\n28\n48\n18\n7\n23\n38\n32\n34\n46\n39\n17\n2\n11\n6\n10\n36\n26")
(defn parse-cards
  [cs]
  (map u/->int (remove str/blank? (str/split-lines cs))))

(defn ->deck
  [cards]
  (apply conj (PersistentQueue/EMPTY) cards))

(defn parse-player
  [ps]
  (let [[n cards] (str/split ps #":")]
    [(u/->int n) (->deck (parse-cards cards))]))

(defn parse-22
  [raw]
  (let [[_ p1 p2] (str/split raw #"Player\s")]
    [(parse-player p1)
     (parse-player p2)]))

(defn part-1
  [raw-game]
  (let [[player-1 player-2] (parse-22 raw-game)
        num-cards (* 2 (count (second player-1)))
        winner (loop [[p1 deck1] player-1
                      [p2 deck2] player-2
                      round 0]
                 (let [c1 (peek deck1)
                       c2 (peek deck2)]
                   (if (and c1 c2)
                     (if (> c1 c2)
                       (recur [p1 (conj (pop deck1) c1 c2)]
                              [p2 (pop deck2)]
                              (inc round))
                       (recur [p1 (pop deck1)]
                              [p2 (conj (pop deck2) c2 c1)]
                              (inc round)))
                     (or (seq deck1) (seq deck2)))))]
    (reduce + (map * winner (range num-cards 0 -1)))))

(defn sub-deck
  [deck n]
  (->deck (take n (seq deck))))

(defn print-deck
  [p deck]
  (println (str "Player " p "'s deck:" (seq deck))))

(defn print-round-winner
  [round game]
  (println (str "Player 1 wins round " round " of game " game)))


(defn recursive-combat
  [player-1 player-2 game]
  (loop [[p1 deck1] player-1
         [p2 deck2] player-2
         history #{}
         round 1
         game game]
    (println (str "Round : " round " of game " game))
    ;(print-deck p1 deck1)
    ;(print-deck p2 deck2)
    ;(println history)
    (let [deck (seq deck1)]
      (if (history deck)                                    ;RULE 1
        (do
          (println (str "Player 1 wins game"))
          p1)
        (let [c1 (peek deck1)
              c2 (peek deck2)]
          ;(println (str "Player " p1 "'s plays: " c1))
          ;(println (str "Player " p2 "'s plays: " c2))
          (if (and c1 c2)
            (let [next-history (conj history deck)
                  next-round (inc round)]
              (let [winner (or (and (<= c1 (count (pop deck1))) ;RULE 2
                                    (<= c2 (count (pop deck2)))
                                    (do
                                      (println "Recursive combat")
                                      (recursive-combat
                                        [p1 (sub-deck (pop deck1) c1)]
                                        [p2 (sub-deck (pop deck2) c2)]
                                        (inc game))))
                               (if (> c1 c2) 1 2))]
                (case winner
                  1 (let [next-deck1 (conj (pop deck1) c1 c2)
                          next-deck2 (pop deck2)]
                      ;(println (str "Player 1 wins round " round " of game " game))
                      (recur [p1 next-deck1]
                             [p2 next-deck2]
                             next-history
                             next-round
                             game))
                  2 (let [next-deck1 (pop deck1)
                          next-deck2 (conj (pop deck2) c2 c1)]
                      ;(println (str "Player 2 wins round " round " of game " game))
                      (recur [p1 next-deck1]
                             [p2 next-deck2]
                             next-history
                             (inc round)
                             game)))))
            (do
              (cond c1 (do (println (seq deck1)) 1)
                    c2 (do (println (seq deck2)) 2)))))))))

(defn part-2
  [raw-game]
  (let [[player-1 player-2] (parse-22 raw-game)
        num-cards (* 2 (count (second player-1)))]
    (recursive-combat player-1 player-2 1)))


(comment
  (count '(18 3 44 39 36 17 38 34 7 5 50 15 22 2 49 23 28 8 47 40 43 25 30 1 42 37 13 11 48 16 24 14 32 20 10 4 46 19 26 6 45 29 41 12 35 9 33 27 31 21))
  (reduce + (map * '(18 3 44 39 36 17 38 34 7 5 50 15 22 2 49 23 28 8 47 40 43 25 30 1 42 37 13 11 48 16 24 14 32 20 10 4 46 19 26 6 45 29 41 12 35 9 33 27 31 21)
                 (range 50 0 -1)))
  (part-1 raw-example)
  (part-1 raw-input)
  (part-2 raw-input)
  (part-2 raw-example)
  (part-2 "Player 1:\n43\n19\n\nPlayer 2:\n2\n29\n14")


  (parse-22 raw-example)
  (pop (conj (PersistentQueue/EMPTY) 1 2))

  (seq (conj (conj (PersistentQueue/EMPTY) 1) 2))
  (peek [9 2 6 3 1])
  (pop '(9 2 6 3 1))
  (seq (conj (vec '(2 6 3 1)) 9))
  (peek (apply conj (PersistentQueue/EMPTY) '(9 2 6 3 1)))

  )


