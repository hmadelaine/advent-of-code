(ns advent-of-code-2020.day-15
  (:require [utils.solve-utils :as su]
            [clojure.string :as str]))

(map su/->int (str/split "16,11,15,0,1,7" #","))

(defn parse-input
  [raw]
  (let [all-spoken (mapv su/->int (str/split raw #","))
        spoken (butlast all-spoken)]
    {:most-recently-spoken (into {} (map (fn [n i]
                                           [n (inc i)])
                                         spoken
                                         (range)))
     :last-spoken          (last all-spoken)
     :spoken               (vec spoken)
     :turn                 (count all-spoken)}))


(defn part-1
  [raw target]
  (let [{:keys [spoken last-spoken most-recently-spoken turn]} (parse-input raw)]
    (loop [spoken spoken
           last-spoken last-spoken
           most-recently-spoken most-recently-spoken
           turn turn]
      (if (< turn target)
        (let [turn-previous-spoken (most-recently-spoken last-spoken)]
          (recur (conj spoken last-spoken)
                 (if turn-previous-spoken (- turn turn-previous-spoken) 0)
                 (assoc most-recently-spoken last-spoken turn)
                 (inc turn)))
        last-spoken))))

(comment
  (parse-input "16,11,15,0,1,7")
  (parse-input "16,11,15,0,1,7")
  (part-1 "16,11,15,0,1,7" 30000000)

  (part-1)

  (keep-indexed (fn [i n]
                  ([i (o)]))   (range 10))
  )
