(ns advent-of-code-2020.day-10
  (:require [clojure.string :as str]
            [utils.solve-utils :as su]
            [ubergraph.core :as uber]
            [ubergraph.alg :as alg]
            [loom.alg :as alg']
            [loom.graph :as loom]))

(def raw-input "153\n69\n163\n123\n89\n4\n135\n9\n124\n74\n141\n132\n75\n3\n18\n134\n84\n15\n61\n91\n90\n98\n99\n51\n131\n166\n127\n77\n106\n50\n22\n70\n43\n28\n41\n160\n44\n117\n66\n60\n76\n17\n138\n105\n97\n161\n116\n49\n104\n169\n71\n100\n16\n54\n168\n42\n57\n103\n1\n32\n110\n48\n12\n143\n112\n82\n25\n81\n148\n133\n144\n118\n80\n63\n156\n88\n47\n115\n36\n2\n94\n128\n35\n62\n109\n29\n40\n19\n37\n122\n142\n167\n7\n147\n121\n159\n87\n83\n111\n162\n150\n8\n149")

(defn parse-10
  [raw]
  (into (sorted-set) (su/parse-nums raw)))

(def raw-example "16\n10\n15\n5\n1\n11\n7\n19\n6\n12\n4")
(def raw-example-2 "28\n33\n18\n42\n31\n14\n46\n20\n48\n47\n24\n23\n49\n45\n19\n38\n39\n11\n1\n32\n25\n35\n8\n17\n7\n9\n4\n2\n34\n10\n3")

(def example (parse-10 raw-example))
(def example-2 (parse-10 raw-example-2))

(def input (parse-10 raw-input))

((defn part-1
   [input]
   (let [answers (let [m (apply max input)
                       input (conj input (+ 3 m))]
                   (loop [j 0
                          js input
                          answer {1 [] 3 []}]
                     (if-not (seq js)
                       answer
                       (let [[delta adapter] (some #(let [delta (>= 3 (- % j))]
                                                      (when delta
                                                        [(- % j) %])) js)]
                         (recur adapter (disj js adapter) (update answer delta conj adapter))))))
         _ (prn answers)
         ones (count (answers 1))
         threes (count (answers 3))]
     (apply * [ones threes]))) input)

(comment
  (part-1 example)

  (def graph-example {0  [1]
                      1  [4]
                      4  [5 6 7]
                      5  [6 7]
                      6  [7]
                      7  [10]
                      10 [11 12]
                      11 [12]
                      12 [15]
                      15 [16]
                      16 [19]
                      19 [22]})
  (uber/viz-graph graph-example)

  (into {} (map (fn [[n nb]]
                  [n [nb]]) (partition 2 1 '(0 1 4 6 7 10 12 15 16 19 22))))

  (defn path->neighbours
    [path]
    (into {} (map (fn [[n nb]]
                    [n #{nb}]) (partition 2 1 path))))

  (defn narrow-path
    [graph seen-neighbours]
    (reduce
      (fn [acc [v neighbours]]
        (if (> (count neighbours) 1)
          (let [reduced-neighburs (remove #((set (seen-neighbours v)) %) neighbours)]
            (reduced (assoc acc v reduced-neighburs)))
          (assoc acc v neighbours)))
      graph
      (into (sorted-map) graph)))

  (alg'/bf-path (loom/digraph graph-example) 0 22)
  (alg'/astar-path (loom/digraph graph-example) 0 22 nil)
  (alg'/bf-traverse (loom/digraph graph-example) 0)
  (alg'/longest-shortest-path (loom/digraph graph-example) 0)


  (let [graph graph-example
        path (alg'/bf-path (loom/digraph graph) 0 22)
        seen (path->neighbours path)
        graph' (narrow-path graph seen)
        path' (alg'/bf-path (loom/digraph graph') 0 22)
        seen' (merge-with into seen (path->neighbours path'))
        graph'' (narrow-path graph' seen')
        path'' (alg'/bf-path (loom/digraph graph'') 0 22)
        seen'' (merge-with into seen' (path->neighbours path''))
        graph''' (narrow-path graph'' seen'')
        path''' (alg'/bf-path (loom/digraph graph''') 0 22)

        ]
    #_[graph' graph'']
    [path' path'' path'' path'''])

  )
