(ns advent-of-code-2020.day-25
  "The handshake used by the card and the door involves an operation that transforms a subject number.
   To transform a subject number, start with the value 1. Then, a number of times called the loop size, perform the following steps:
   Set the value to itself multiplied by the subject number.
   Set the value to the remainder after dividing the value by 20201227.")

(defn steps
  [subject-number value]
  (rem (* value subject-number) 20201227))

(defn find-loop-size
  [subject-number public-key ]
  (loop [value 1
         loop-size 0]
    (let [value (steps subject-number value)]
      (if (= public-key value)
        loop-size
        (recur value (inc loop-size))))))

(defn transform
  "To transform a subject number, start with the value 1.
  Then, a number of times called the loop size, perform the following steps:
  Set the value to itself multiplied by the subject number.
   Set the value to the remainder after dividing the value by 20201227."
  [subject-number loop-size]
  (first (take 1 (drop (inc loop-size) (iterate (partial steps subject-number) 1)))))


(defn part-1
  [card-public-key door-public-key]
  (let [card-loop-size (find-loop-size 7 card-public-key)
        door-loop-size (find-loop-size 7 door-public-key)
        encryption-key-card (transform door-public-key card-loop-size)
        encryption-key-door (transform card-public-key door-loop-size)]
    [encryption-key-card encryption-key-door]))

(def card-public-key 1526110)
(def door-public-key 20175123)

(comment

  (part-1 card-public-key door-public-key)

  )