(ns advent-of-code-2021.day-10-syntax-scoring
  "The navigation subsystem syntax is made of several lines containing chunks.
   There are one or more chunks on each line,
   and chunks contain zero or more other chunks.
   Adjacent chunks are not separated by any delimiter;
   if one chunk stops, the next chunk (if any) can immediately start.
   Every chunk must open and close with one of four legal pairs of matching characters:
   If a chunk opens with (, it must close with ).
   If a chunk opens with [, it must close with ].
   If a chunk opens with {, it must close with }.
   If a chunk opens with <, it must close with >.

   Some lines are incomplete,
   but others are corrupted. Find and discard the corrupted lines first.
   A corrupted line is one where a chunk closes with the wrong character - that is,
   where the characters it opens and closes with do not form one of the four legal pairs listed above.

   ): 3 points.
   ]: 57 points.
   }: 1197 points.
   >: 25137 points."
  (:require [instaparse.core :as insta]
            [clojure.string :as str]
            [clojure.set :as set]
            [clojure.java.math :as math]))


(def parser (insta/parser (slurp "resources/2021-10.BNF")))


(def example "[({(<(())[]>[[{[]{<()<>>\n[(()[<>])]({[<{<<[]>>(\n{([(<{}[<>[]}>{[]{[(<()>\n(((({<>}<{<{<>}{[]{[]{}\n[[<[([]))<([[{}[[()]]]\n[{[{({}]{}}([{[{{{}}([]\n{<[[]]>}<{[{[{[]{()[[[]\n[<(<(<(<{}))><([]([]()\n<{([([[(<>()){}]>(<<{{\n<{([{{}}[<[[[<>{}]]]>[]]")


(def input "<[<<<<[{[<({{({}<>)<[]{}>}[<<>{}>{<>{}}]}{(({}<>){()<>}){{[]<>}{[]}}})>([{{(()[])[{}()]}}(<{{}<>}\n<<[<<{[<<<{{<(()[])>[({}{})]}}({{({}())[<>{}]}}{{(<><>)[<>()]}<<<>{}><<>()>>})>{[[<(<>[])[[]{}]>{{[]<>}<[]\n<[{<<(<([{<[<{()<>}<{}[]>>([<>()]{<>()})]{[([][])<{}<>>][<<><>>]}>}][[[<{{[]{}}[[][]]}([{}[]]<[]<>\n<{[(<<[({([{<[{}[]]{<>{}}>}[(({}()){[]<>])]]{{<<[]>([]<>)><<{}()>{<>[]}>}{({<><>}<<>{}>){(()[])<()<>>}}})\n[{<{[(({[[<<<<<>()>{(){}}>[{[]{}}(<><>)]>{[<[][]>(()<>)]<<[]()><()()>>}>])}(<{<[<<{}[]>>{[()[]]({}{})}]>{\n[<{<[[[<{[<<[{<>{}}<(){}>]{<{}{}><{}{}>}>{<(()<>){<><>}>}><[<{{}()}>{{<>()}{()<>}}]({<{}<>>}[{()<>}[()()]])\n(([(<({[<<({{<()()]{[]{}}}[{<>()}[[]()]]}({[<><>]}<[<>{}][()<>]>)){{[[[]<>]<<><>>](({}{}){{}()})}}>>]})>)<((\n(<<({((<<(<{([()]<()[]>)}[{(()<>)(()())}{{()<>}<()()>}]>)({[<(<>())((){})><{<><>}[{}<>]>][[[<>\n[(<([{[[[{{{([()()]([]()))[<[][]>[<><>]]}{(<[]>[(){}]){[<>{}]{{}()}}}>[<<[[][]][{}()]>[<<>{}>\n{<{[[[{<<([({[<><>][(){}]}({(){}}[{}<>]))<{[(){}][<>{}]}{(()()){{}[]}}>]{[((()[])<()[]>)<<(){}>{{\n[<[{{{({([({<<(){}>[{}<>]><{{}<>}[<><>]>}[[<(){}>([]{})]<{(){}}[{}[]]>])[<({<>{}}<{}<>>)({<><>})>]]){(<(({\n<<{[[{[[(<[{[[{}()][()[]]](<[]{}>(<>))}(([{}()])<[{}()](<>())))]><(<[[<>{}](<>())][[<>()][()]]>{<<<><>>[()[\n<<([<[{{[[({[{()}[{}<>]]({{}{}}{<><>})}<{{{}()}<()[]>}<<{}{}><[][]>>>)<{([[]<>]<{}()>)][([{}()\n[(<<[[{<<{<[[<()[]>({}<>)]<{[]<>}<{}<>>>]{<<()<>>[[]{}]>([{}()]{()()})}>[<{((){})[{}()]}[([]{})<()[\n[<<(<{[{{[<{(([])({}())){<<>>{()[]}}}{[(<><>)[[]()]]{<<><>><()<>>]}><((<<>()>{<>{}})){[{()()}][[()[]]([]{})\n((<[({<[(({[{(()())<(){}>}<[[]<>]{()()}>]({<()<>>(<>())})}<(<[[][]]{<>[]}><<[]<>>[{}[]]>)<{([]{})[{}()]}([[]\n[([{<[{(<<({[<{}()>([][])]{({}<>){{}[]}}}[<((){}){<>{}})[{(){}}]])({([{}<>]{<>{}})})>[<<{<{}<>><[\n[{{{[<({({{{[<{}()><[][]>]({{}{}}([]))}{<[<>{}]{{}{}>><{{}<>}>}}}[[[([{}()](()()))][({<>{}}{<>{}}){\n[<{[{{(<{[<[(<()()>{()[]}){{{}{}}(<><>)}]<<({}{}){{}()}>{([])(<><>)}>>[({{[]{}}})<{(<><>){(){}}}((()<\n<[<<[<(([<[{(<[][]>(<>[]}){{{}<>}[{}()]}}]<[[<{}{}>{()()}][[<>()]]]([<()>(()[])])>>][{[((<[\n[{({<[[{<<[<[(<>{})<[]{}>]<<[]{}>[<><>]>>{({()<>}[<><>])}]><{{{{[]()}{<>[]}}<((){})>}({<(){}>[\n{[<([[<(((<{<{[]<>}<[][]>>}>)))[<<{[([[]<>]([][]))(({}<>)<<><>>)]<{{{}{}}<<><>>}([()<>](()()>)>}>>]>{({<{\n{{{{(([<(<{({(()())([][])}{({}()){[]{}}})}({(((){})[{}[]])}{((<><>){<><>}>[<{}[]>{<>[]}]})><[{<(<>[\n<[<[<(<<<{(([[(){}]({}<>)]){<[()<>]{()()}>[[{}{}](<>)]})((((()<>)({}<>])[[{}<>][<><>]])(<<{}{}>(()()\n[[{(([<{<[[({{{}{}}<[]<>>}{([]){<>()}})[(<[]()>(<><>)){<<>()>{()[]}}]]]>(<[[{(()<>)[[]{}]}(([]())(()\n{<<(<<([(<(<({[]{}}{[]<>})>({([]<>)<[]{}>})){{([(){}]([]<>)){[[]()]}}}>{<{((()](<>()))}[(<{}<>>((){})\n[({{{{{(<{({[{<>())(()())][<[]<>><[][]>]}{<[<><>][()()]>[<()()>([]{})]})<[[([][])<[]()>]<([][])<()<>>>]\n[[[<([<[<(<{(([]<>)<{}<>>)<[{}[]]<()[]>>}{{([]()){()[]}}([{}{}](()()))}><{{<()<>><()()>}{<{}<>}{<>()}}}{{[[\n(<({({[[(<[{<{(){}}[{}{}]><{()()}>}[([<>()]({}<>)){<{}()>({}[])}]]>)[[(([(<>[])<()<>>]){[<\n({<[<{<<{{{({[()[]]})({[()()]{{}()}}(([]<>)<<>[]>))}}}>>(([({<[{[]}[[]()]][[<>[]]([]<>)]>({[<>()]{\n{{(<[{({<[{{[{{}()}(<>{})]{[()[]]([]{})}}{([[]()][[]])[[[]()][()[]]}}}[<[<{}()>[{}[]]]>({<[]()>([\n<(([(([[[[(([({}{}){()<>}]<{()[]}([]<>)]))]][{[({{<>()}<{}<>>})([[<>()]])](<[<<>()>]({()<>}{[]{}})>{[(()\n([<{[<<<(<[{(<{}<>>[<>()])[<(){}>]}[[[()[]][()<>]]<<[]{}><<>()>>]]<[<{{}<>}{()<>}>{{[]()}})(<<<><>>>{\n[{({{<(((({(<{(){}}>((())(()<>)))((<{}{}>((){}))<[[]]((){})>)})(<(([[]{}]<<><>>)<{()[]>(<>())>)>))))>}{{{({\n[{(<<{{(<[{({{{}{}}{<><>}})}(<{({}<>)[<>{}]}<[{}{}]{{}{}}>>)]>([[{(<[][])<[]<>>)<(()<>)[[][]]>}[({()<>\n<[([[((([{<[{(<>{}){<>()}}(({}[]])]([([]())<<>[]>])>[{<[{}<>](<>[])>}<[[(){}][{}()]]{{{}<>}[{}[]]}>]}])))][{(\n(<[[(({({{[<<[<>[]][<>{}]>[[<>{}]((){})]><<<{}<>>[<>{}]><{[]{}}[()<>]>>]([[[<>{}]{()()}]<<()<\n[{{{{<<{{{{<({<>()}((){}))(<[][]>{[][]})>{<(()<>)[()<>]><<{}{}>{{}[]}>}}<[[<{}{}>]{[<>()]<<><>>}]{<(()())<{}\n([(<<{[{(<{<(<{}{}><{}<>>)[(<>[]){(){}}]>({<[]()>(<>{})}<{[]{}}[(){}]>)}>([[<<[]()>(<>)>(<<>[\n<(<<{<{[{[<[{<()<>>([][])}]>]}]}><<<[([{(({}{})(()()))({()()}<<>>)}([(<>{})(<>[])][({}<>)[[]<>\n<([[((<[<[[((<{}<>><{}()>)<(<>{})<[]<>>>)[<{()}({}())>{[[]{}]{()[]}}]]<<[(()[])(<>())]{<[]<>>\n<<{{{<<({{<[<[<>()]{{}<>}>{<()<>>{[]{}}}]>}}{({[<[()[]][<>{}]>((()[])<{}()>)]}([{[[]{}]<[]<>>}{([]\n(({[(<({<{[[{<<><>>({}[])}(<<><>>[[]{}]))(<{<>{}}[[]{}]>[[<>{}]({}{})])][<[<()[]><()<>>]><[({}[])[<>()]]<([](\n([([[([(<{[<([<>{}][(){}]){<<>()><()<>>}>{[{()<>}[[][]]][[{}{}]<(){}>]}]([<[[]()]{{}[]}>[{<>[]}[[](\n({((<[(<(({{([{}[]](<>()))[({}())]}[{[<>{}]<<>()>}{[{}]}]}({{<[][])[{}[]]}}((<<>{}><<>{}>))))<[<{[(){}][{\n{([<{{(({[(<<{<>{}}[<><>]>{(<>[])({}())}>{((()[])[{}]){([][])((){})}})][<[<<[]()>{(){}}>]((<{}()>[{}()])<<{\n{{<<(<[<(((({{{}<>}{[]<>}}[(<><>){[]()}]))[<{([]<>)<[]()>}([[]{}]<()>)>{{[<>[]]({}<>)}{<<>[]>[<><>\n[<({{[{{[(<[[<[][]>{<>{}}][{{}[]}[(){}]]]>{{(<{}[]>(<>()))<((){})>}[{([])}]}){[<[{()<>}<[][]>][[\n({<{{{(({({<(<{}[]>[[]<>])>})}<{(([{<>{}}<<>()>](([]<>){()[]}))([({}{}]{{}{}}][{()[]}{()()}]\n((((<({[[<(<{(()[])((){})}>(([{}()]<<><>>)[{()[]]{{}}]))[[{(<>()){<><>}}{<<>[]>[[]<>]}][[{\n({<[<((<({<<((<><>)(<>[]))[{[]{}}{[]{}}]>{[[()()]{()()}]{[<>{}]}}><({[[][]][()<>]})>}([[[(\n({<(<<<<<(<[<[<>()](()>>][<<<>()>[{}[]]><(<><>)>]><({<<>{}>}[<<><>>{(){}}])[{{<>{}}<[]<>>}]>)(<{{{()()}\n[<<[{[<{([([{<()()>[{}[]]}<<()[]>{()()}>][<<{}>[[]<>]>{[()[]][[]()]}]){[((()<>)<{}[]>){[[]()][{\n[{[<{<{(({{<(({}[]))[[{}[]]{[][]}]>}}<([{(()[])}<[[]()]>]{<<<>()>[<>[]>>[[[]()][[][]]]})>){<<[<<{}()>(()\n<(({{<{{<[{(<[[]<>]><({}{}){[]{}}>){{[[]{}]({}<>)}<((){})<<><>>>}}{[(([]<>)[{}<>])({<><>})]<(<()<>>{(\n[[{[(<(<<[(<[[<>[]]((){})]{[<><>]<{}[]>}>)<[{{()[]>(<>{})}(({}[]){{}[]})]<<[{}{}]>{[{}{}]}>>][{([<()<>>({}\n[({<({[[<[{<<{{}[]}<(){}>><[[]()][{}{}]>><{([])([])}[({}<>){()()}]>}{[{<{}{}>[<>()]}]({({}[\n{{<[<[(([<{[{{[]{}}[[]{}]}]}>[[(<{[]()}(()())><{(){}}(<>())>)[<([]()){{}<>}>[<[]<>>{{}[]}]]](((({}\n{(<<[(({{[[{([[][]]{{}()})([()<>][{}{}])}]([[{[]<>}({}{}]][<{}[]>]](({{}[]}){<{}[]>((){})}))][<((\n{{{{({{({[{{([{}[]]<(){}>)<<[]()>([]<>)>>{[{{}()}]<[{}]<()()>>}}[[{<[][]>{[]{}}}({()[]}<[]()>)][<[{\n[{{[[({[[[[((<{}[]><{}[]>){[{}{}]{()[]}})({{(){}}([])}<<[]()>[{}{}]>)]}{{{({[]}({}))<((){}){{}<>}>}{{\n[<{([{(<{(((([()<>]<()<>>)[[<>()]([]{})])[(({}()))<<<>()><[]>>])[<{{<>{}}[()()]}(<[][]>{()()}\n({(({{<[{<{((<{}{}>{{}[]})<{{}()}{{}()}>)[[({}()){[]<>}]{<()>[{}<>]}]}><{[[(<>{})<[][]>]<{{}<>}{{\n(({{(({(<[[{[<<>{}>{[]()}][{[]{}]{(){}}]}(((()[]){{}{}})<(()())>)]([<[{}[]]>])][<{{[{}[]]<<><>>}{[[\n((<[[<<<[([<[<<><>>([][])](<[][]>{<><>})>((({}<>)((){})))>[[<[<><>]<{}<>>><({}[])([]{})>]])[{[{({}<>)[()[]\n[<((({{({(({<(()())<{}[]>>(<[]{}>({}{}))}))[<<[({}[])[{}[]]]<{{}{}}{[]}>>>]}[(<({<{}()>[<>()]}[<[]()>((){\n[[([(((<{[[[<[{}()}({}<>)>]]{<{{<><>}({}())}{{<><>}([]{})}>}]}[([{[{[][]}(()<>)]{{<>{}}[<>(\n<<([<{{<{<<(<([]<>)><(<><>){{}{}}>)<(<()<>>([]<>)){(<>{}><<>>}>>{(((()[]){{}[]})[[[][]][{}[]]])({([\n[<[{<[<<(([{({[]{}}([][]))<[()<>]{{}<>}>}]<<[<{}>[<>{}]][([]<>)(<>[])]>[{((){})(()())}<[{}()]{<>()}>]>){([\n[[[{[(({<<{{{((){})({})}{[<>()]([]<>)}}{[<<>{}>[{}[]]][[{}<>][()<>]]}}>{<{{([]{}){{}[]}}<{<>{\n{{{<{[[<[{[<[<<>()>{{}()}][[<>{}][[]<>]]>]<{<{(){}}({}{})><(<>())[{}()]>)<{[()()]}[<()<>>(<>)]>>}\n(([[<{(([<[((<(){}>[{}()]){([]{})[<>{}]})[[[<><>](<>[])]((()()))]][{{<()[]><<>{}>}}]>])<({[[({()<>}[<>{}])]]\n{{({[({[[<(((({}[])(()()))))(<<<{}()>[{}]>>(<[<>[]]{[]()}>{({}<>)([][])}))>]{({{<(<><>)>[{\n<<((({[([[{<<{<>{}}[[][]]><{()()}<{}<>>>>}[[[<<>()>([]())]({<><>}{{}[]})]<{<{}<>>[[]()]}{{(){}\n<[{(([<(<{{[[{{}()}([]<>)](<{}()>{[]{}})]}<[[<()[]>(()())]{([]{})[<>{}]}]<<{[][]}><<<>()>>>>\n<([(([{([([(<[{}[]][[]()]>(<{}[]>[<><>]))<<{{}()}<<>[]>>>])[[<[{<>()}[()()]]<[{}()]((){})>>]{[{(()[])<(\n((<([<[[{[{{[(<><>)<[]()>][(<><>)[<>{}]]}}<(([{}()]{<><>})<([])[()()]>)>]{(({[()[])[[]()]}[(<>)])[<{{}\n{<{<<{{[{<(([{(){}}<<>[]>]<{{}{}}{()[]}>)[<[[]{}><<><>>><[<>()]>])>{(<({{}{}}{<>{}})[<<>[]>]>)((<{\n[<<<(({{{[(<{([][])[{}<>]}<(()[])(()())>>[{{<>{}}({})}[<<>>[<>]]]){([{()()}[()[]]]{[()[]]<<>>\n<{{{<(<<{([[(<{}()>(<>{}))]](({[[][]]}({[]<>}{[]<>}))<([{}{}][[]])[<[]()><{}<>>]>))[<<<<()><[][]>>{(()())<[]\n{{[[{[[{{{[{([<>[]][<>[]>)[({}[]){{}{}}]}[(((){})[()])(({}[])[<>])]]}<[<{<[]{}>({}<>)}{[{}[]]((){}\n{([<([([{(((([()[]][[]<>])[<[]()><()>])({({}{})<[][]>}[([]<>){[]<>}])))<<{[[{}{}]][{(){}}{[]()}\n<<[(([{<((<[({<>{}})[(()){[][]}]]>))>}]<({((([{([][])({}<>)}<<()()>([]())>])){{{{[{}{}]<<>[]>}{(()[])\n[[{<([[{<{((({<>{}}[{}{}])([[]()]{{}[]}))({<[][]><<>{}>})){<(<<><>>({}<>))<([]){<>{}}>>(<<<\n([({<<{<(<((<(<>{})({})><[[][]](()())>)([[{}{}]([]())][<()[]>[{}[]]])){<[<{}{}]{<>()}][([]{}){<><>}]>}>)>{[[(\n{<(([<([(([(({[]<>}[{}<>])[(()<>){()[]}])<<{[][]}>({<>[]}{[]<>})>]{{{<(){}><()()>}({[]{}}{{}[]}))([([]())\n{[<([[{[({[<{{(){}}}{(()[])<()[]>}>]<<<{()[]}><{{}()}([]{})>>{{<<>()><<>[]>>{{<>[]}{<><>}}}>\n(<(<{[{[([[[([<><>])][({[]{}}({}[]))]]])[<[{({{}<>}([]()))(<{}><()()>)}(<<{}<>><<>()>)<<{}\n(<[(({{<[{({{<()[]>}{{<>}<<>[]]}}{[{<><>}[{}<>]]((<>))})([[[<>{}]]<([])>])}{<({<[]<>><<>()>}[({}())[<>\n{[({([(<{[[[<<<>[]>(()<>)>]<(([]{})[[][]])(<[]()><{}<>>)>](({{<><>}<()<>>}{[{}{}]([]<>)})[<{<>}[[]{}]")
(defn parse-input
  [input]
  (str/split-lines input))

(def scores-1
  {")" 3
   "]" 57
   "}" 1197
   ">" 25137})

(defn extract-illegal
  [error]
  (let [{:keys [column text]} error]
    (scores-1 (str (nth text (dec column))))))

(defn solve-line-1
  [line]
  (let [parsed (parser line :total true)]
    (when (= :instaparse/failure (get-in parsed [1 0]))
      (extract-illegal (parser line)))))

(defn solve-1
  [input]
  (->>
    (parse-input input)
    (keep solve-line-1)
    (reduce +)))

(def dico
  {"(" ")",
   "[" "]",
   "{" "}",
   "<" ">",
   ")" "(",
   "]" "[",
   "}" "{",
   ">" "<"})


(defn scores-2
  [acc c]
  (+ (* acc 5) ({")" 1
                 "]" 2
                 "}" 3
                 ">" 4} c)))

(defn transform-2
  [parsed]
  (insta/transform
    {:chunk              (fn [& c]
                           (if (every? #(or (string? %) (nil? %)) c)
                             nil
                             c))
     :instaparse/failure (fn [_] :error)
     :line               (fn line [& all]
                           (keep dico (reverse (flatten all))))}
    parsed))

(defn solve-line-2
  [line]
  (let [parsed (parser line :total true)]
    (when (not= :instaparse/failure (get-in parsed [1 0]))
      (reduce
        scores-2
        0
        (transform-2 parsed)))))

(defn find-middle
  [scores]
  (let [middle (dec (math/ceil (/ (count scores) 2)))]
    (nth (sort scores) middle)))

(defn solve-2
  [input]
  (let [scores (keep solve-line-2 (parse-input input))]
    (find-middle scores)))

(comment
  (assert (= 358737 (solve-1 input)))
  (assert (= 4329504793 (solve-2 input)))
  (solve-2 example)

  (solve-1 example)

  (parser "[(<>)]{}")

  (map parser (parse-input example))
  )