(ns advent-of-code-2021.day-18-snailfish
  "Every snailfish number is a pair :
   An ordered list of two elements. Each element of the pair can be either a regular number or another pair.
   To add two snailfish numbers, form a pair from the left and right parameters of the addition operator.
   For example, [1,2] + [[3,4],5] becomes [[1,2],[[3,4],5]].

   To reduce a snailfish number :
   you must repeatedly do the first action in this list that applies to the snailfish number:
   - If any pair is nested inside four pairs, the leftmost such pair explodes.
   - If any regular number is 10 or greater, the leftmost such regular number splits.
   During reduction, at most one action applies,
   After which the process returns to the top of the list of actions.
   For example, if split produces a pair that meets the explode criteria,
   that pair explodes before other splits occur.

   To explode a pair :
   The pair's left value is added to the first regular number to the left of the exploding pair (if any),
   And the pair's right value is added to the first regular number to the right of the exploding pair (if any).
   Exploding pairs will always consist of two regular numbers.
   Then, the entire exploding pair is replaced with the regular number 0."
  (:require [clojure.edn :as edn]
            [clojure.zip :as z]
            [clojure.zip :as zip]
            [clojure.java.math :as math]
            [clojure.math.combinatorics :as combo :refer [combinations]]))

(defn num?
  [loc]
  (when (number? (zip/node loc))
    loc))

(defn pair?
  [node]
  (and (vector? node)
       (every? number? node)))

(defn nested-pair?
  [loc]
  (let [node (zip/node loc)]
    (when (and (pair? node)
               (= 4 (count (zip/path loc))))
      loc)))

(defn find-first
  ([move pred zipper]
   (loop [current-location zipper]
     (if (and current-location
              (pred current-location))
       current-location
       (if-not (or (nil? current-location)
                   (zip/end? current-location))
         (recur (move current-location))))))
  ([pred zipper]
   (find-first zip/next pred zipper)))

(defn find-left
  [loc]
  (find-first zip/prev num? loc))

(defn find-right
  [loc]
  (find-first zip/next num? loc))

(defn find-explode
  [loc]
  (find-first nested-pair? loc))

(defn find-explode-reverse
  [loc]
  (find-first zip/prev nested-pair? (zip/prev (zip/prev loc))))

(def example [[[[[9, 8], 1], 2], 3], 4])
(def input "[[3, [5, [7, [3, 9]]]] [[[[7, 0], 0], [2, [2, 8]]], [[[7, 8], 1], 3]] [[[[2, 7], 0], 7], 4] [[2, 1], [9, 0]] [[[[7, 1], [3, 2]], [[9, 8], 5]], [2, 7]] [[[8, 9], [[8, 7], 0]], [[[8, 7], [6, 3]], [[1, 7], [8, 9]]]] [[8, 6], [[9, [1, 7]], [6, [3, 9]]]] [[2, [[5, 6], 6]], [[4, [5, 9]], [3, [4, 5]]]] [[[[2, 0], [1, 1]], [6, 6]], [[1, 9], [[2, 7], [6, 8]]]] [[[4, 6], [[6, 3], [3, 9]]], [[[2, 6], [6, 1]], [[9, 9], [1, 5]]]] [[[4, [3, 1]], 3], 6] [[0, [[5, 2], 8]], [1, [9, [4, 3]]]] [[[[8, 6], [2, 1]], [2, [8, 6]]], [[[7, 1], [3, 9]], 0]] [[[[4, 7], [2, 7]], [[8, 9], 2]], [[[2, 4], [7, 2]], [3, 7]]] [[5, [2, 2]], [[1, 6], [[9, 1], [5, 0]]]] [[5, [[1, 2], [6, 4]]], [6, 8]] [[[5, [1, 7]], 7], [7, [8, 1]]] [[1, 9], [[0, 3], [[6, 7], [2, 4]]]] [1, [7, [[0, 6], 0]]] [[[[5, 7], 9], [[3, 2], 7]], [[5, 1], [9, 9]]] [[[[0, 4], [9, 6]], [[8, 3], [7, 4]]], [7, [6, 2]]] [[[[1, 6], 0], [[8, 0], [3, 4]]], [[3, [0, 3]], 4]] [4, [[7, 8], [4, [9, 7]]]] [[[2, [3, 7]], 5], [0, [9, 9]]] [[[2, 0], [[5, 8], [7, 6]]], [[9, [6, 2]], [3, 2]]] [[[3, 1], 3], [[[3, 7], 6], [9, 8]]] [[7, [[2, 5], 5]], [5, [3, [4, 5]]]] [[[6, 7], 6], [2, [[9, 3], 9]]] [[[[5, 6], 7], [[3, 2], 5]], [[9, [4, 3]], [3, 8]]] [0, 7] [[[4, 6], [2, 9]], [[[7, 6], [5, 1]], 7]] [[0, 5], [[1, [4, 1]], [[7, 3], 9]]] [[[2, [3, 8]], 5], [[[5, 9], 8], [7, 0]]] [[[6, [8, 6]], [[3, 6], 7]], [[2, 1], [6, [7, 5]]]] [[2, [[6, 3], [8, 9]]], [[[5, 6], 4], [[7, 0], 1]]] [[[[7, 1], [5, 6]], 8], [[[8, 9], 4], [8, 3]]] [[[9, 2], [1, 0]], 0] [[5, [5, [8, 5]]], 4] [[3, [5, [4, 9]]], 3] [[8, [[7, 7], 6]], 5] [[4, [[5, 1], 1]], [1, [1, [9, 8]]]] [[[7, [3, 6]], [[2, 8], [4, 7]]], [[[8, 8], [4, 0]], [2, 4]]] [[[[3, 6], 3], [0, 9]], 2] [[2, 8], [[8, [8, 6]], [[1, 1], [4, 5]]]] [[2, [1, [1, 0]]], [[[6, 2], [7, 4]], [[7, 1], 6]]] [3, [8, [7, [8, 6]]]] [[1, 0], [[[0, 4], [0, 5]], [1, 5]]] [[[[5, 0], 4], [[7, 8], [8, 8]]], [[1, 7], 0]] [1, [[[4, 1], 7], [6, [9, 0]]]] [[[1, 8], 2], [[5, 5], [8, 5]]] [[4, [9, [0, 6]]], [[[8, 9], [4, 5]], 4]] [[[[5, 4], [1, 7]], [[3, 1], [7, 9]]], [[[0, 8], [4, 7]], [[5, 9], 6]]] [[[[8, 0], 9], 4], [[7, [1, 3]], 5]] [[[[5, 0], 6], [[6, 1], 8]], [[9, 1], 7]] [[9, [6, [8, 8]]], [7, [[7, 1], 6]]] [[[5, [1, 5]], [3, [4, 2]]], [[[5, 2], 7], [[6, 9], [2, 8]]]] [[[5, [5, 5]], [5, 7]], [4, [[2, 9], 7]]] [[[[0, 4], 0], [[0, 6], [3, 0]]], [0, [[8, 1], 2]]] [[[7, [4, 6]], [[7, 2], [4, 6]]], [[[9, 3], [4, 9]], 6]] [[6, 7], 7] [[[4, 1], [8, [1, 5]]], [[4, 6], 0]] [[[4, [5, 5]], 5], [[0, [2, 7]], [1, 1]]] [[[[0, 1], 3], [6, 7]], [4, 7]] [[4, [6, 4]], [[[9, 8], 1], [9, 3]]] [[[4, 9], 0], [[[7, 0], [0, 9]], [1, [1, 0]]]] [[[7, 9], [[9, 5], [6, 9]]], [[0, [3, 0]], [0, [5, 9]]]] [9, [[0, 0], [[1, 9], 9]]] [[[5, [0, 5]], [[9, 8], [9, 5]]], [[0, [2, 5]], 7]] [[[[5, 8], 6], 9], [[[2, 7], 7], [[7, 8], 5]]] [[8, [[4, 7], 6]], 2] [[[[7, 1], [9, 0]], [9, [1, 7]]], [[8, [6, 7]], [2, 5]]] [[4, [2, 9]], 8] [[[[7, 6], [5, 3]], [5, [9, 7]]], [[6, [8, 1]], [[6, 4], 9]]] [[7, [[7, 8], 4]], [[1, 3], [4, [9, 7]]]] [[[6, [6, 7]], [[2, 8], 3]], [7, [6, [0, 3]]]] [[9, 8], [[0, [4, 8]], [[9, 1], 1]]] [[[[4, 0], [5, 9]], 7], [6, [[5, 9], [9, 6]]]] [[8, 1], [1, [9, [8, 3]]]] [[[1, [5, 1]], [6, 7]], [[5, 9], [2, [6, 7]]]] [[[3, 7], [[7, 8], 1]], [[0, [6, 3]], [8, 0]]] [[5, [[9, 3], [1, 2]]], 7] [[[1, [9, 9]], 3], [[6, 4], [4, 1]]] [[6, [1, [3, 6]]], [2, 9]] [[2, [0, 2]], [5, [[9, 4], [5, 0]]]] [[4, [[3, 1], [7, 0]]], [[9, 1], [[5, 5], [6, 7]]]] [[3, [[7, 1], [3, 4]]], [7, [9, [9, 4]]]] [[9, 9], [[5, 4], [[9, 7], 4]]] [[[5, 1], 8], [[6, 7], 9]] [[[0, [9, 5]], [4, 3]], [3, 2]] [[[6, [4, 1]], [[8, 7], [5, 3]]], [[[1, 2], 5], [[9, 2], 5]]] [[[[7, 4], [9, 0]], [[1, 8], [2, 9]]], [[5, [1, 9]], [4, 0]]] [[[4, [3, 8]], [[3, 3], [2, 8]]], [[[1, 3], 9], [[8, 5], 6]]] [[[[6, 4], [7, 9]], [[7, 6], 8]], [7, [9, 8]]] [[7, [3, 5]], 7] [[[[5, 0], [2, 3]], [3, 7]], [[4, [6, 3]], [7, [4, 4]]]] [[6, [3, [7, 6]]], [[[5, 8], [8, 1]], [3, [1, 5]]]] [[8, [9, [5, 2]]], 2] [[1, [5, 4]], [[7, [8, 0]], 8]] [[[[2, 7], 4], 3], [[1, 4], [8, 4]]] [3, [9, 2]]]")

(def plus (fnil + 0))

(defn update-left
  [loc l]
  (if-let [left (find-left loc)]
    (find-explode (zip/edit left plus l))
    loc))

(defn update-right
  [loc r]
  (if-let [right (find-right (zip/next (zip/next (zip/next loc))))]
    (find-explode-reverse (zip/edit right plus r))
    loc))

(defn explodes
  [math]
  (if-let [nested-pair-loc (find-explode (zip/vector-zip math))]
    (let [[l r] (zip/node nested-pair-loc)]
      (-> nested-pair-loc
          (update-left l)
          (update-right r)
          (zip/replace 0)
          (zip/root)))
    math))

(defn find-split
  [loc]
  (find-first #(and (num? %) (<= 10 (zip/node %))) loc))

(defn splits
  [math]
  (if-let [to-split-loc (find-split (zip/vector-zip math))]
    (let [n (zip/node to-split-loc)
          [l r] ((juxt #(int (math/floor (/ % 2))) #(int (math/ceil (/ % 2)))) n)]
      (-> to-split-loc
          (zip/replace [l r])
          (zip/root)))
    math))

(def add vector)

(defn explodes-and-splits
  [math]
  (let [math' (explodes math)]
    (if (= math' math)
      (let [math'' (splits math')]
        math'')
      math')))

(defn reduce-snailfish-number
  [math]
  (loop [math math]
    (let [math' (explodes-and-splits math)]
      (if (= math' math)
        math
        (recur math')))))



(defn magnify
  [[l r]]
  (+ (* 3 l) (* 2 r)))


(defn magnitude
  [math]
  (loop [loc (zip/vector-zip math)]
    (if (zip/end? loc)
      (zip/root loc)
      (recur (zip/next
               (cond
                 (pair? (zip/node loc)) (zip/edit loc magnify)
                 :else loc))))))

(defn solve-1
  [numbers]
  (let [result (reduce (comp reduce-snailfish-number add) numbers)]
    (loop [result result]
      (let [result' (magnitude result)]
        (if (= result' result)
          result
          (recur result'))))))

(defn solve-2
  [numbers]
  (let [all-pairs (combinations numbers 2)]
    (apply max (map solve-1 all-pairs))))

(comment



  (assert (= 3574 (solve-1 [[[[6 6] [6 7]] [[9 5] [8 0]]] [[[7 8] [7 8]] [9 2]]])))

  ([[[[8, 7], [7, 7]], [[8, 6], [7, 7]]], [[[0, 7], [6, 6]], [8, 7]]])
  (solve-1 (edn/read-string input))

  (assert (=  4763 (solve-2 (edn/read-string input))))


  (= [[[[6, 7], [6, 7]], [[7, 7], [0, 7]]], [[[8, 7], [7, 7]], [[8, 8], [8, 0]]]]
     (reduce-snailfish-number (add [[[[4, 0], [5, 4]], [[7, 7], [6, 0]]], [[8, [7, 7]], [[7, 9], [5, 0]]]]
                                   [[2, [[0, 8], [3, 4]]], [[[6, 7], 1], [7, [1, 6]]]])))

  (=
    [[[[7, 0], [7, 7]], [[7, 7], [7, 8]]], [[[7, 7], [8, 8]], [[7, 7], [8, 7]]]]
    (reduce-snailfish-number (add [[[[6, 7], [6, 7]], [[7, 7], [0, 7]]], [[[8, 7], [7, 7]], [[8, 8], [8, 0]]]]
                                  [[[[2, 4], 7], [6, [0, 5]]], [[[6, 8], [2, 8]], [[2, 1], [4, 5]]]])))

  (= [[[[0, 7], 4], [[7, 8], [6, 0]]], [8, 1]] (explodes (splits (splits (explodes (explodes (add [[[[4, 3], 4], 4], [7, [[8, 4], 9]]] [1, 1])))))))
  (reduce-snailfish-number (add [[[[4, 3], 4], 4], [7, [[8, 4], 9]]] [1, 1]))

  (= [[[[0, 9], 2], 3], 4] (explodes [[[[[9, 8], 1], 2], 3], 4]))
  (= [7, [6, [5, [7, 0]]]] (explodes [7, [6, [5, [4, [3, 2]]]]]))
  (= [[6, [5, [7, 0]]], 3] (explodes [[6, [5, [4, [3, 2]]]], 1]))
  (= [[3, [2, [8, 0]]], [9, [5, [7, 0]]]] (explodes (explodes [[3, [2, [1, [7, 3]]]], [6, [5, [4, [3, 2]]]]])))

  (def add vector)
  (splits [1 12])

  (vector [1, 2] [[3, 4], 5])

  (splits (zip/vector-zip [1, 10]))

  (count (z/path (z/down (z/down (z/down (z/down (z/vector-zip [[[[[9, 8], 1], 2], 3], 4])))))))

  (z/right (z/down (z/right (z/down (z/right (z/down (z/right (z/down (z/vector-zip [7, [6, [5, [4, [3, 2]]]]])))))))))


  (z/right (z/down (z/right (z/down (z/right (z/down (z/down (z/vector-zip [[6, [5, [4, [3, 2]]]], 1]))))))))


  (z/right (z/down (z/vector-zip [[3, [2, [1, [7, 3]]]], [6, [5, [4, [3, 2]]]]])))

  )
