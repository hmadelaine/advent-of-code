(ns advent-of-code-2021.day-21-dirac-dice
  (:require [clojure.java.math :as math]))


(def input "Player 1 starting position: 10\nPlayer 2 starting position: 4")

(def example "Player 1 starting position: 4\nPlayer 2 starting position: 8")


(defn turn
  [player dice]
  )

(defn roll
  [from]
  (let [[f s t] (range from (+ 3 from))]
    [(+ f s t) (inc t)]))

(defn next-position
  [mark]
  (let [d (mod mark 10)]
    (if (zero? d)
      10
      d)))





(defn solve-1
  [position-1 position-2]
  (let [result (loop [position-1 position-1
                      score-1 0
                      position-2 position-2
                      score-2 0
                      dice 1]
                 (let [[forward dice] (roll dice)
                       position-1' (next-position (+ position-1 forward))
                       score-1' (+ score-1 position-1')]
                   (if (<= 1000 score-1')
                     [position-1' score-1' position-2 score-2 dice]
                     (let [[forward dice] (roll dice)
                           position-2' (next-position (+ position-2 forward))
                           score-2' (+ score-2 position-2')]
                       (if (<= 1000 score-2')
                         [position-1' score-1' position-2' score-2' dice]
                         (recur position-1' score-1' position-2' score-2' dice))))))
        [_ _ _ score-2 rolls] result]
    (* score-2 (dec rolls))))


(defn turn
  [player roll]
  (let [[position score] player
        position' (next-position (+ position roll))]
    [position' (+ score position')]))

(defn universe
  [player rolls]
  (map #(turn player %) rolls))

(defn explore
  [])

(comment

  (turn [10 0] 1)

  (
   (take 6 (partition 2 (partition 3 (range)))))



  (take 9 (map #(universe [10 0] %) (partition 3 6 (range 1 Long/MAX_VALUE))))



  (assert (= 908091 (solve-1 10 4)))

  (math/pow 90 3)
  )