(ns advent-of-code-2021.day-8-seven-segment-search
  (:require [com.rpl.specter :as sp]
            [clojure.string :as str]
            [clojure.set :as set]
            [clojure.data]))


(def digits {0 #{:a :b :c :e :f :g},
             1 #{:c :f},
             2 #{:a :c :d :e :g},
             3 #{:a :c :d :f :g},
             4 #{:b :c :d :f},
             5 #{:a :b :d :f :g},
             6 #{:a :b :d :e :f :g},
             7 #{:a :c :f},
             8 #{:a :b :c :d :e :f :g},
             9 #{:a :b :c :d :f :g}})

(def digits-lentgh {0 6,
                    1 2,
                    2 5,
                    3 5,
                    4 4,
                    5 5,
                    6 6,
                    7 3,
                    8 7,
                    9 6})

(def lentgh->digits {2 #{1},
                     3 #{7},
                     4 #{4}
                     5 #{2 3 5},
                     6 #{0 6 9},
                     7 #{8}})


(def example "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe\nedbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc\nfgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg\nfbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb\naecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea\nfgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb\ndbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe\nbdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef\negadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb\ngcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce")
(def input "bgafcde gfcd agc ebdgac adfceb bafeg efgca cgdfae cg ecadf | fabgced gc agc cdfg\ngbdacfe gcabfd cdb dcfba bfacg cgad fadeb feabcg cd gcbfed | bdagcef dcb cdag gbfca\ndgcbafe dbfca fbaed be cedb gefad dcfeab facdgb eba gbface | eb gadfcbe cfbad gfbeca\nebc cb aedbf agcef badecg gaebfc bcgf adbcfge ceabf daecgf | cb bce efdab ecbaf\nfedbc cebad gfcbd fec gcdfab ecbfga dacgbfe gfed fe gefbdc | bfecag cef egdf fgde\nbafedc baefg dbfga daegcb gae egbcfa eg cefab fgce decbafg | abdgf cgfe cedgba befga\ndcba fagbed cgbfed dgfbeac da dag acbgde fcaeg becgd acgde | agd bacd dga gbecad\nec aebgcfd fecd bagfec efagd edfgab cgdfea dgcab ecdga eac | ce ec ce gbacefd\naegdbfc fe dacbf aefbgd aecgb cdfe ebadcf ecbfa cbfdga aef | dfce fe fcde afebdgc\nbgaf gcbad cagdfb gb dfbeca degfcb bfadc cfgeadb bdg agcde | fdegacb acbefgd bdg gafb\ngdecba dcae ec dbfeag bgead dgbce gec cgefab dcbgefa cdbfg | afdbgce agbfde abcgefd ec\ngacbefd fgbedc egafdc adf fcbae afedb ad bfeadg adbg dfgeb | ad degcfa ad dfbge\nfadebc baef eadfcg ebgdc eda cfgabd afbdc ea edcab cdgbefa | aefb ead ea ebaf\nbadgecf caefd aebfdc fecbdg fadcg fed de ebad abgecf bceaf | ed gcfebd aebd gfdca\nbcdae aefcbg fgdea bf cdabegf adcebf fcbd fbead afb begadc | gbcafe faegbc fba eabdc\nbfcdeg ecd eabgc adeg ed dbaec bcfad ecabfg dbgcfea dbacge | de bdfcaeg ecgfbd bcdea\ndeacf bf eagfdb gfecadb facbeg bfa cbdage abfce fbcg gecab | aebcf ecfagb abegfd bgeac\neag dgbecf gdfae caed dfebgca ea ebcgfa dfbag gecfda dgcfe | ae ea deac fcdega\nedbga egdabc cgfdbe gabfe efcdgab gbdefa fb abdf fbg gecfa | cgaebd cbgdea acgfe begdca\ncgfedb egbdc agcfd cdagb eabgdf ab abce adb agcebfd dabegc | ba bda abd deagfb\nfc agfbce fdebg dgafeb ebfdc cbf cfedgba fdcg gcefdb eacdb | gbfaec fgcbed gbceafd bedagf\nbdcgefa acdfbe ed gbcea cfgbad ecbda edfcag afcbd ead bdef | dfbe de dea dea\ngfbeca gcefdb fegacd cgbda fbde ed bgedc begcf agdcefb ced | gbacd ced dec bcegfd\ndgbacf gfabec agd egdaf badcegf ad ecgaf defgb cade dfegca | cdae dga bgcaef ad\ndcf cfadb bcagd afebd fbgdec egfcbda fcea gfeabd fc ebdfac | gabdc ebfad aefc eacbdf\ncfgdba fcdab cgfbade adeb afdebc egcfdb de aedfc cfage edf | ed eadb dgebcf cefdabg\nbcae befga dgebfc fbagce cbefg agb fcedgab ba bdgacf dgfae | beca dcaegbf gab geafb\ndgafbec fedba cbega aebfdc eabfdg bdgfce edc fdac cd adceb | abedf dfeabg dc gdbefc\nadegfbc gbedcf cfabd gacedb abcfe fdbacg dgabc df dfga bfd | fdag dbf dafg fd\ndfbcag ebfcad ed agecdb bfacd egbfa badfe afbcegd aed cdef | afebd ecdf cfed eadfbgc\nfca dbcaf ebdcf fadbg cdbefa adec ac dacbgfe fbcdge cgefba | ac edca edac ca\nagdfce cedbfag agc dcbaeg gc bcdga bgdfa cfaebd ebdac cbge | ceadb becg ebadc gc\naefbc aecdg efgcad cdebag bcg agfdbc bceag bdge bg fecdgab | bgecda bcdaeg gb caebf\ncfgebd acedf ag gbad gcfbad acg bgecfa bdfcg gbadecf cdfga | dgba dgcbf gac bgfdc\ncdgbafe cdgbe gd agde bcaged dfebc geacb cdg bgeacf gbdcfa | efgcba edag gecfdba cgd\nbcg bcaf cegdaf dfacg dgacb fgcebda bgdcaf efbcgd bc gadbe | gcb bcg caebdfg cfab\neg cdgfae aecfd fgbaedc gbdfc facdeb cfegd ceag efg efdgab | fge acefbgd egf efg\nfcdg gefda cf edabc cedgfa cfa acefd cfebadg gdbafe bgecfa | fc fca cfabged fca\ned bcadegf egdfb fgcdb edbafg facbeg abegf deba fadgec dge | eadb ged eadb adfegb\nbgedc cf cefadg ecbgdf gbfae cbdf fcg ecbgad abfgced becfg | bdcf dbceg fgcbde cdbeg\ncg gcaf defcag cdefg gdafe dgfabe gec dabegfc bcegad ebdcf | feadg degcf gcaf dcgefa\nfdeac gdcebf daecbg bgfec fdeagbc gca gecfa aefcbg fbag ag | ga decfagb ag agfb\nbc fbdc bagecdf gfdcab acdgb dcaeg dbgfa cgbaef ebgfda gbc | bdfc cb cfbd fbcd\nefagc bafegd abecg eab eb fgbeca gbedcfa bfec fcgade cgbad | abfgdce degacf fcaebg cdabfeg\negbfcd afde ecgab fac fa edabcf cbdagfe afbec cfabgd fdebc | deaf feda cgbdfa fa\ngbafd defca ebcf eb cefbad dcebag dcgfae edfab deb bgedcfa | be deb gdbafce be\nafdgceb cf gbdeaf acdbfe abcef acf deafb fedgac cdbf cgeba | fac cfdb bdfcae fdcb\ncfdba ca afbgd eabc cda afbcdeg cfedb ecfgbd efcgad bcfeda | bfgad abec abce caeb\ncfebad debfacg acgfeb cab fedcb afcdb cdae febcdg ac dgbaf | bac ca faecbgd cgbfae\neg dfcegb dge abedcgf facdg bafdgc degacf dafeg agec adebf | egd fegda fedbcag eagdf\nage gcbad feacb cgbdfae ge gfbe agebc fcadeg cbafde cgebaf | fagecdb bfeg baecg fceabd\nbfcdge fgcbead ebdgac ba dagbfe gab dabc bcegd gface cebga | cbda gbedc egcbafd gab\ndbcegfa fadceg fcage bcage gb bdagfc abdec gbfe bgafce bcg | ebfdagc ebfg ebgf egfb\ngcdefa bagf dgebf bdefc bgcafde dbefga edabg gfe gf dcgeba | fg efg agfb gebfd\nfabdc gdeabf dag gbace adcgb gd eagfcb gaedbc egdfabc egdc | adgbc bcadf gd cedfabg\ndeagcf ce dfcaeb gfadceb bgade cde daegc cgdafb gadcf efcg | dbaecf fdecbga ce ecd\ndfcbag abceg db dgcefa dabec bcegdaf cdb bdef defac cfbdea | gdafec dcb febd bdef\nead fgceabd edgcaf da fgda fgcea ecdgb cgaed fdbace cbagef | ad eafcdb bfecagd ad\nab caefg agbcef efacb gecdfba ecfagd fbdec bdecga cba fgab | eafgc abfg cdagbfe dcageb\nafbcge ebafcgd dacegb cfbea afbg agc egcfa ag cgdfe bdceaf | adfbceg fagb gfba aecgbf\nacg cdegbf ecfadg cgdab fcbadg cdbgfae ca agedb cbgdf facb | cdeabfg ca afbc dagfec\ndca dbfae bagdcf aceg ac afdegc cegfd fcedgb cbdaegf cedfa | dac acge cedgf cfdbgae\ngdcafe ega cgbfde cgba afdbe bacegd ga begdc dcebagf edgab | abgc ga bacg bdacge\nbegfda gdafce fg gdbf gbaced agf decagbf aebdg gabfe cebfa | fag acdbeg agcdfe dgbefca\ngfcdbea fecab cbadge afgce gc fgdace fdcg cga eagdf eagbfd | efgacbd gc dgfc dacbeg\ngdbefc feadg dbagef afd abde gebfd cefga bdgafc da efdacbg | cbegfda adf bade cdgfbe\naefbdgc eabcfg cgefa degbf cedfg cd cadg cde fgecad fecadb | dbfegca egcdabf gcda dc\nbaed bcgda abdegc decag ega dgbfaec gacbfe ae cgfde dbacgf | aebd age ea bgdecfa\nabecg faegcd egfcd efcga cdaf baedgf efa fcbaedg fcgedb fa | afe af aef fa\nabgfce cbedgaf fbedc eacdfb dcf fd faegcd fecba dfba gdceb | faecdg gdceb dfc gcbde\nfgce fgadb egdabc edg defbg eg bfced fdbace bdfgeac gbcfde | gdcbfea deg afbedc dgcaeb\neg dgcfe efdgab bfdec aecg acbegfd bdcafg adcfg efg daefgc | fge aecg gfe ecagfdb\nacdfe bedcfg abgec cfeabd aedbfcg fecdag fg fcg afecg gdaf | cdaefg fdag cgf gadcfbe\naedc gbdac cbagfe bfdcg gdbfea acb cbgaed ac eadgb gecadfb | gbfdc gbdaec fbgeadc decabg\ngbc afgdb cfbdga badgef gadbce egabcdf geacf bcdf cb gabfc | fgaec fcagdeb gbcfa bc\ncfgabd cfdbe eafgdc badcfeg dgfab cbag eafbdg gc dfgbc cdg | fagecd cgd afdbge cg\neadcbf fcag gc gbc gadcefb gebad febdgc egcbaf ecbag bacef | dgbfec egadcbf gcfa bcegfda\nacbeg acgedb eg dgbfce cdagb cgdbaf gaed bafce cfebadg ecg | dbgac eabcf eabfdgc eagd\nbgefda eacg abdcfe eabcd abegcd ebg cbedg cbfdg dgfecab eg | ecag eg ge afebcd\ndgecbf bg dfcba abge gadebc edagc bcg ecdfga fbgecad bgcad | cgb gbea ebgcdf gbae\ndcabg gdfbca debc aed ed fabeg afdecg gbeda caegbd agbfcde | fcbdgea cedb dea efcdagb\nedbcafg cgaed gc gbdc acfebd cge gceabd abecd afegd bgafce | gc facegbd edbcaf cg\ngadbce gbdaf acebgf bd abcgfd edfga cdfb gbd cfgab dgafbce | gfdab fdegbac eabcfdg bgcdae\neacgf fb cegbdf dgbfca cfb dfba agfcb cgabd ebcdgfa gedacb | acbdg dbaf ceafg bf\ngebac egdacf fdceg daef af gfa dagfbc fcgbead afgce cebfgd | afg bcage gcdeabf cgfea\ncdabfg fgbec gcdafe ceafgb egc ec beac febdg bdegcfa fabcg | cafegdb beca ecgbfa bace\ngda feabdg ebgaf bgdfc bgfcae bfeagdc edab ad adgbf fceagd | gad ad afgbde dfaebg\nbfc bgfced edbfag fcea cf afgbdce cgbda abgef fcgba aebcfg | bcf efac eacf fc\nbdeag bgdcf ec ebgdfac gcbde gaec dec cafdeb afdebg cebdag | gcbfd agedb acfdbe ecd\nbefad gd edgbf bcgafd cebgfd abgcef gfd ecgd cgdfaeb ecfgb | cegd cdeg efadbgc egcdbaf\ngfdbae bc cegbfd gadcf cdaefgb gfbed fbcgd beagcd gbc becf | gedcfba edcagb fbcgdae cfbe\necf cbdaf bdfcaeg fagdec efba bcdfe bfecda gcedb ef cbadgf | ef fbedc dbceg edgfcab\ncabde ae ecfdba cbdfe faed dabcg cfbgaed gbfcea begdcf abe | adfe aeb cabgd ae\ngefcabd ecabd gebafc ag ecfgbd gacdbf dfcbg fdga cagbd acg | gebcadf gefbac dgaf dagf\nbgde efcgd cebgfa dbacgf cgfeb gdc fdaec dfbceg gd fegcadb | efbcadg cgd gbdcfa facebg\ngefcbd bgfdcea fg dcbag fbgcd fgeb defcag fcg becfad dfcbe | cefbda cfg fegbdc fg\nbcfed bcfdge aedbfg fad decbfa facge cdba efdca ad dbfagce | eabfgd fadce begfdca gecdbaf\nedfgbca dbega fdb gfecab fd ebcfda cfda feabd becfa edgbfc | df ebgad cabegfd adcf\ndaegbc fbacg edfg befdac efbcd edafbgc cdg bcgfd fdegcb gd | dg becdf dbeafcg gadefcb\negdacf gde ed dfebcag acgdfb cfadg agebdf cgeab gecda dfce | fedagc gabfecd eadgfbc cfgebda\ndfcea afbde fdgac gefbac eabcdf ace bced ec ebagfd adgbcfe | ce fcdga baecfg cea\nadebg gdfcae gb acegd abg begc bdaef bfgcda dgaefbc abgdec | bg cbafgd ecbg gba\nbfacde egfadcb cb fcdea eacbf cedb dcgeaf bgadfc abc ebgfa | adfebc dceb bdcafe efdac\nfaegbd dgfce dcgafe fgadecb bcgd bcfge bfaec gb cdbfeg fgb | aecfb fbg fcgade dafgbec\nfgacbe fc ebcadg agfdb fdbca cagbdfe cdfe abcde fcb bfacde | cabed cfde bfdac dgbaf\ndefabc gfcbe fea bdacfg adgecf fabce ae aebd bfecdga cdafb | fea ea bgfaced bead\ndfgae dcafe dce dgebfca abfcd ecab bfcdea gcfebd gafcbd ec | bcdagf cde gbedcfa bfdace\ncea gaedfb acdf ac ebacgd gefdabc befda cfbae egcfb efcabd | gcaebfd cbaef dfac ca\ngebac befadgc bedcgf cgb gc eagbdc cebaf cadg bgfaed aegdb | cbg cbg cbgadef acdg\ngfcdeb bfdegca beac befda bcf dcfga fbcda cb adecfb gbefda | fbc dbafc cbf cfb\ncbdfeag becagf acgeb be cgbda gfcdbe efba acfeg ecb eadgcf | aebf eb bagcfe eb\nfebg aecbdg fedacb ecg ge fbgaedc dfgce afcgd fcbed efcdgb | aedcbg baegcfd ecg bedcf\nbcfedg gdbcae febd fcbag fgbcd bd ecdgfa dbc fdcagbe gdcfe | efgbadc fcbedg abfecgd cgbedf\nfeagc cb dcgeabf fadbgc cgfdae fbdea feagcb bcafe cbeg bac | bgce bc cgadbf cba\nefagbd dageb dcgbeaf gc acg gdecab gecd fdbca gbcfea acbgd | eagbd egfabcd gfbace gca\nae cgae dea eacgfd fadgc gfebd afdcbe dfeag acbfgd dacbgef | dgbafc cbdfgae ae ead\negb gcfe abcfdeg begfcd abcdfg dabec eg bdfcg edgcb bedagf | bdecg beg gbdcef dbeac\nfcgae dfcebag aefdb geacbf cda dagbcf cd adcfe fedacg gdce | bcdafg adefb fdeba gecbfa\nafegd gadfb fcgadeb gbcade gdabef cefgad dfbe bdg fagbc bd | dabegc bfgca bd dgb\ndfceba faecd bd afbedgc ebad fcadb bafgc dcb efgdcb gfaedc | faedc bead egcdbaf bd\ncgb bdcfeg cg gdbeca cgef gdbcfea febdg fdcgb dafgeb bacfd | cgfe cgb gc gfec\ncadgfb cefdbag cg begacf dfgbea bgc adgbf bgcad fdgc eabcd | cg fgceab ebdacgf ecbdgaf\nbgdefc decfg cfedga cefgb bfdcea edgfacb bgcd fegba ceb cb | cfgbe dcaefg cbgfe dbcg\ngefbda acde gcbaf cd cfd bdcgfae daefg gdcbfe cgefad agdcf | gcebafd efgcda cfd aecd\ngedfc cead egbfda fgcdabe de fcgea gfdbc deg cagdef gebacf | deg bfgead gdfbc gde\nfbgae bfecg dbcaef gdcaeb cbfdge gbc decgbfa edbfc fdgc gc | gc gbcdef fdcg gfcbe\nfc caf cadbe dgabfc gebdfa fecg fagbe cebaf acbegdf cgabfe | egcf ceagbf gcfe ebgafc\nabfcd cgbfa bcgafd gadefc abfde cdf dc agfbced cbegfa gcbd | cd cfd fgeacd cdf\nbdgae gfbce cbagfd gedbcaf ebcgd gbcefa dc fcde cegdfb dbc | dcegb fgdcbea deabcfg bdc\nebcagf df bgcdefa fdceba cgdf dbacgf eadbg afd abgcf gabfd | ceagdbf dagcbf fd dcfbaeg\ndga cadbgef efbcgd bgdca eacd cedbga gdbec gdeabf fbacg da | dag adg edac edgcb\nfebcg gbadce aedbcf acbed cbgae cag gadb degbcfa ga fcgdae | bgad abdgfec acbed acg\ngdcaf dcbfag gdefa abcdg bcfg cf fedgacb dbaegc fcedba afc | cf afc gcfb eafdgbc\ngdcaef gcabfd bcefgda cde gcdfb gbecfd ec dfaeb cefbd egbc | egbfdc ce gbdcfa afbcgd\nfdea fga fa adcgbef abfcge gbeda fagdb abedgc cgbdf bdefag | fa gaecfb beadgc cbfgd\nba gcbafd decgba bfga fbadc bda dfbeacg ecfgbd cedfa fdbgc | gfab gbdfc adb ba\nbd ecdgab aedfg bde fbcae decfba cgafbe fdeab dbfc fdgceba | db dbe cdfb bdgfaec\nbd gcefbd ebcd dafbgc ebfgd decgaf edcfg fbd fbaeg gafcdbe | cbeagdf cebd cedb cedb\nedagf cdaefbg ead efgdb bgafed cfdag bfea dabgce dfbceg ae | gcafd feab dgfca feba\ngecfda bgcdf ba abd edgabf bcdeaf gedfa fdbga baeg abegcdf | ab bad bda fgebcda\negadb bec dbefga cbafg cageb ce ebdgca dceg gdebfca cfbead | ec ebc egdc ecb\ncbgfe fadcbg agde cbdge egdacb degcabf bed faebdc ed dcabg | eadg facdbe bcdeg debfac\nfabe fb bgf edcgaf bgacd bcfegd fcdegab adgef afbdg gbfade | bfg dbcgfae edacbgf bf\nbcdfe becgd cbafegd decag geb dgfb bfcade dgcefb bg gfbaec | bdgce gb dcbge gdfb\negcadf bfg fgbdac faedg ebaf gcedb begdf fb adfebg cafdgeb | faedbcg bafcgde bf becgd\nedafgb ecbgd gcdfaeb ef gbfadc cgfade gef fdegc fgcda efca | agfecdb dcabgef ef dgcaf\ncbdgea fegcad bagdc abdfgc age gebac afbcdge egbfc adeb ea | ae ega ecagbd gbacd\necdfb edafbc fe afdcb efc degcb aedf acbgfd ecgfab fecabdg | fe dbegcaf fe cedgfba\nedbfgc caeb ca cbdafg bcdge bgcdae edfga eacgd gca cgabfed | facebdg ca aedbgc ac\ngfeba aegc cbfega ea bea dfgbe edfacgb fadgbc gbafc aefcdb | gbecfda bcdfag gcea afcegdb\nfgcea dc egfdba cdafe gfdecb fbaedc ebdfa dacb bdafgec dec | ced bcfdaeg cefda fagcbed\ndbefa defacg agd dcbfg bdfagce ga efgbdc abgc fdgab bacfdg | cbga dga gedcfab fdabcg\neacbf eac dfagbce cfga fbgea ca bfced agebdc fbedga fcbega | dabfgce gfca ebfacg gbaecf\ncfagb cgfdea gdc agfcbe fdeacbg bcfdg dg badg fdgacb bcdef | cgd gfacb gd gdba\ncfga gbadef decabfg cbged baefcd ca cda dbagf fdbcag dbgca | afgebdc bdgeaf gafc cfag\ncgfbd cdafgb gbcead dbg afdb bgecf adfcg ecbgdfa bd gdacfe | fgacd cgdfa bdaf dbg\nabf fb febd dbafg febcga gbead egdacb fedgba fbagdce gdcaf | bf fb dbceafg fba\nfd dgfabe ecafdg deabcg cbefa fed dbgea bdefa bgdf dbfgeca | fde bdfg fdgb gfeabcd\nfgeadb abfed dgfbcea bafgd agcfd gfb bdfaec bg dgbe cegbaf | egdb fegadbc dcgaf bg\nfdabc acfbgde dg fcage cdfgab dfebag dga gdafc cdgb eafbcd | gfcad aegdcbf dag afbdc\ndbfe efdag fgdcae cgbfaed bgfead acebg fb bgf egabf abfcdg | ebdf gfb ebfd fgabed\nbafegd egcfadb fbc bc fcebgd becaf afbed agecf beacfd bdca | cb dbca cfb afgbcde\ngbefdc bfead gfcdab gbedca fc cabegdf bedfc fdc cefg cgbde | fdbceg cgfe fc bdcaefg\nfg dbfea fedagbc cageb aecfdg dfgb dbfaec gafbed begaf gaf | gfecad dgcfea dafeb gfbd\ndabfg eb egacdfb bfcdga faedc gdeb fbagec aeb bgefad dfeba | bafdcg gfbda gebd be\nadb dgeabc bfdg gaecfd afdgc cbdgfa bd dcgeafb dafbc bafce | bd fcagbde defagcb dbfg\nfge bagcf dfaebg fgebc eg cdeg fabcedg bedcf dfcegb acfbde | egcd ecgd cebafd cgdaefb\nbafdgc gecabf caegf ae beaf gfacb decgf cfedbag aec cdabeg | dcfge ae egcdafb eca\nfcgba ebgaf agefd agcdfb beac gdbcfe beg bcdfgae gacfbe eb | cfebgd abecdgf ceab abcfdge\nabcgfd bac feabd adcgeb fcgb bc debgcfa fbdac gceafd fagdc | fbcg gcfad afbed cfbda\nafdbegc dea abegd agecbd afbgdc dgbfe cabgd afcbed ceag ae | daecbfg cdbag acge ea\nebdcg cbfg cdgebf bge adbec eabgdf baecfdg fgadce gedfc bg | dcbae fbcg beg gfbc\ncfbga cefabg ebfca abeg ae bcedfga gbacfd eac bdefc dcafge | abge fedbc bacgf eac\neb aedcg dbfcge cegdbaf gfbe bed dagcbf dacebf dcfgb dbceg | gbef dbe fegb eb\ncgfe ced dgeba ce cegad abedcf cdaefg cfagbd cadfg gbcedaf | ecgad gcabefd dfbcgae ce\ndbagc afdc dgfaeb bcfage gbedfac cafbdg cdgeb ac cab gdafb | cab dafbcg dacf acdgfeb\ndgbae bfa eafgb bafegd fgaec bcfadg fb dbfe egbadc gbecadf | cdegbfa dagfcbe fdeb fab\nbe aecgdf gbdacef bagfc bdef beagf beg dbefag gedfa cbgade | be eb afegb beg\ncfdagb ac gcfdae cbad bfgda bcfedga begcf afc ebafdg cfbga | deagcf abdc ca cagfb\neg efdbga adbcfg ebfcga aefgd bdfga adcbgef defac gbed gae | edbg gedb bfdeag dbcgaf\nbacegf gbfca cfa ceab gafeb ca gcfdb gadfec dbafge gbdfaec | caf fbgedac fca afc\nfbagde cbeg afecg gdaecbf fedcga gab efacbg gb acbfg cfdba | gba ecbg agb abgcf\nabfg dafcgb gcafd bdcgaef ab cba ebdcf bgdaec cfaegd fcbda | acb ecadbg cgdabe fgab\nagcbd cegdba feagb fd daf gbfad bacdfg cgdfabe dcefab fgcd | df dagbc fgcd fd\ngdfeabc bcd debaf cb gedfab bcefd edfgc bdceaf aecb cdgbfa | ebac beac cbea abce\ncbgdef eg beg dbgfc bfgcead gbefd efcg edgbca eabfd acbfdg | gbe gbe cefg gbe\nfedg begcad gdb bafdge gd egbcaf gdbaf bfacd eacgdbf eafgb | cbefag baegf gefcdab bedgac\ngbce dcafgb agfdceb efdac acg agcfe aefgb befagd cegfab gc | fbedgac abdefg cga cag\ngabc bceadf agfcd agd ag fcdgbea gabdef gedfc dbcfa agbfdc | gacb gad bgceadf cbgafde\nafedc fabced gfcbed feb cegdbaf dbfa ebfac dacfeg fb gbace | feb afdb ebf acfde\ncbed edgcf ecabdfg ceg cfbgd abgfdc gebfdc ec efbagc gedfa | ec decb ecdb gec\negd gfbeac gedcb de dabgfe dcfbg cbefdag eadc dcegba acegb | ed adgfceb gde abcged\nefadcgb fbdce fagedc agebfd gbafe abedf da bgad ead ceagbf | efagb ecdbf ade aebcgfd\ncgeb bfaed cbedga cbaed bc bca afgcbd dgface bgfecda decga | fgadcb bca dabfe cab\ndgaec deagcf egf cdeabg abgdf gdfceab dfcbeg gfead efac ef | agebcd efdbcag aefc bgefcad\nfbeac gebcaf efa fgce bgfdea dgbcfae gcbdea adcbf ef egacb | gebfacd bfadcge ef eagbc\nacdbgfe adfg edfcbg efacb egacf gca dgaceb egdcf ga gefcda | fdga cfgae cga ga\ndfebcg gdaebf dagcf gbaecd dcgebfa bf cfbe bgf bgdce fdbgc | cgbefd bcfe gbf ecbf\ndcafeg ca acf fcdeb abcfdge facdgb bfagd dfaegb bagc dfbac | dfabgce dfaegc gbafde bacg\ncgefb cbe cfab afdbeg cb faebg gcfde cefbag ebadgc afdbgec | egbdcfa cbe cafb fecagbd")

(defn digit->kx
  [digit]
  (into (sorted-set) (map (comp keyword str) digit)))

(defn parse-line
  [line]
  (let [[raw-signals raw-four-digits] (str/split line #" \| ")]
    [(str/split raw-signals #" ")
     (str/split raw-four-digits #" ")]))

(defn parse-input
  [example]
  (->> (str/split-lines example)
       (map (fn [line] (str/split line #" \| ")))
       (map (fn [[raw-signals raw-four-digits]]
              [(str/split raw-signals #" ")
               (str/split raw-four-digits #" ")]))))

(defn solve-line
  [line]
  (let [[sequence digits] line]
    (->> digits
         (map count)
         (keep (set (vals (select-keys digits-lentgh [1 4 7 8])))))))


(defn numbers->cnt
  [numbers]
  (set (vals (select-keys digits-lentgh numbers))))

(defn catch-easy-ones
  [line]
  (let [[sequence digits] line
        knowns (->> sequence
                    (keep (fn [digit]
                            (when-let [cnt ((numbers->cnt [1 4 7 8]) (count digit))]
                              [(first (lentgh->digits cnt)) (digit->kx digit)])))
                    (into {}))
        unknowned (->> sequence
                       (keep (fn [digit]
                               (when-let [cnt ((numbers->cnt [0 2 3 5 6 9]) (count digit))]
                                 (digit->kx digit)))))]
    [knowns unknowned]))

(defn solve-1
  [input]
  (->>
    (parse-input input)
    (mapcat solve-line)
    (count)))



(defn juste-un-segment-de-moins?
  [segments candidat]
  (when (= [1 0 (dec (count segments))]
           (map count (clojure.data/diff segments candidat)))
    candidat))

(defn juste-un-segment-de-plus?
  [segments candidat]
  (when (= [1 0 (count segments)] (map count (clojure.data/diff candidat segments)))
    candidat))

(defn find-first
  [predicat segments candidats]
  (some (partial predicat segments) candidats))

(def recherche-un-segment-de-moins (partial find-first juste-un-segment-de-moins?))

(def recherche-un-segment-de-plus (partial find-first juste-un-segment-de-plus?))


(defn quatre+sept->neuf
  [{:keys [found u] :as acc}]
  (let [quatre+sept (set/union (found 4) (found 7))
        neuf (recherche-un-segment-de-plus quatre+sept u)]
    (-> acc
        (assoc-in [:found 9] neuf)
        (update-in [:u] #(disj % neuf)))))


(defn huit-six-intersec-un->six
  [{:keys [found u] :as acc}]
  (let [huit (found 8)
        six (->> u
                 (filter #(= 6 (count %)))
                 (keep #(when (seq (set/intersection (found 1) (set/difference huit %))) %))
                 first)]
    (-> acc
        (assoc-in [:found 6] six)
        (update-in [:u] #(disj % six)))))

(defn six-moins-un-segment->cinq
  [{:keys [found u] :as acc}]
  (let [cinq (recherche-un-segment-de-moins (found 6) u)]
    (-> acc
        (assoc-in [:found 5] cinq)
        (update-in [:u] #(disj % cinq)))))


(defn zero-reste-le-seul-nombre-avec-6-segments
  [{:keys [u] :as acc}]
  (let [zero (first (filter #(= 6 (count %)) u))]
    (-> acc
        (assoc-in [:found 0] zero)
        (update-in [:u] #(disj % zero)))))

(defn neuf-moins-un-segment->3
  [{:keys [found u] :as acc}]
  (let [trois (recherche-un-segment-de-moins (found 9) u)]
    (-> acc
        (assoc-in [:found 3] trois)
        (update-in [:u] #(disj % trois)))))


(defn dernier
  [{:keys [u] :as acc}]
  (-> acc
      (assoc-in [:found 2] (first u))
      (update-in [:u] #(disj % (first u)))))


(defn solve-line-2
  [line]
  (let [line (parse-line line)
        [k u] (catch-easy-ones line)
        acc (->> {:found k
                  :u     (into #{} u)}
                 (quatre+sept->neuf)
                 (huit-six-intersec-un->six)
                 (six-moins-un-segment->cinq)
                 (zero-reste-le-seul-nombre-avec-6-segments)
                 (neuf-moins-un-segment->3)
                 (dernier))
        seq->num (set/map-invert (:found acc))]
    (parse-long (str/join (map seq->num (map digit->kx (second line)))))))


(comment
  (solve-1 input)
  (solve-2 example)

  (clojure.data/diff #{:a :b :c} #{:a :b :f})

  (solve-line-2 "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf")

  (assert (= 994266 (reduce + (map solve-line-2 (str/split-lines input)))))

  (map #(map set %) (map second (parse-input example)))
  )