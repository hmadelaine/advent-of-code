(ns advent-of-code-2021.day-6-lanternfish
  (:require [clojure.string :as str]))


(def example "3,4,3,1,2")

(def input "2,5,3,4,4,5,3,2,3,3,2,2,4,2,5,4,1,1,4,4,5,1,2,1,5,2,1,5,1,1,1,2,4,3,3,1,4,2,3,4,5,1,2,5,1,2,2,5,2,4,4,1,4,5,4,2,1,5,5,3,2,1,3,2,1,4,2,5,5,5,2,3,3,5,1,1,5,3,4,2,1,4,4,5,4,5,3,1,4,5,1,5,3,5,4,4,4,1,4,2,2,2,5,4,3,1,4,4,3,4,2,1,1,5,3,3,2,5,3,1,2,2,4,1,4,1,5,1,1,2,5,2,2,5,2,4,4,3,4,1,3,3,5,4,5,4,5,5,5,5,5,4,4,5,3,4,3,3,1,1,5,2,4,5,5,1,5,2,4,5,4,2,4,4,4,2,2,2,2,2,3,5,3,1,1,2,1,1,5,1,4,3,4,2,5,3,4,4,3,5,5,5,4,1,3,4,4,2,2,1,4,1,2,1,2,1,5,5,3,4,1,3,2,1,4,5,1,5,5,1,2,3,4,2,1,4,1,4,2,3,3,2,4,1,4,1,4,4,1,5,3,1,5,2,1,1,2,3,3,2,4,1,2,1,5,1,1,2,1,2,1,2,4,5,3,5,5,1,3,4,1,1,3,3,2,2,4,3,1,1,2,4,1,1,1,5,4,2,4,3")


(defn parse-lanternfishes
  "Give the frequencies by age"
  [raw-lenterfishes]
  (frequencies (map parse-long (str/split raw-lenterfishes #","))))


(def example-lanternfishes-by-age (parse-lanternfishes example))
(def input-lanternfishes-by-age (parse-lanternfishes input))

(def plus (fnil + 0 0))

(defn generation
  "Decrement all ages
   If age is 0, transform to 6 and cumulate with 6 frequency."
  [lanternfishes-by-age]
  (reduce
    (fn [new-gen age]
      (let [age-zero (get lanternfishes-by-age 0)]
        (if (zero? age)
          (-> new-gen
              (update 6 plus age-zero)
              (assoc 8 age-zero))
          (if-let [value (get lanternfishes-by-age age)]
            (assoc new-gen (dec age) value)
            new-gen))))
    {}
    (range 8 -1 -1)))

(defn solve
  [n lanternfishes-by-ages]
  (->> lanternfishes-by-ages
       (iterate generation)
       (drop n)
       first
       (vals)
       (reduce +)))


(comment

  (solve 80 example-lanternfishes-by-age)
  (assert (= 1574445493136 (solve 256 input-lanternfishes-by-age)))

  (do
    (require '[criterium.core :as bench])
    (bench/quick-bench (solve 80 input-lanternfishes-by-age)))

  (part-2 input-lanternfishes)
  )
