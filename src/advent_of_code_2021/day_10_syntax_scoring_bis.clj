(ns advent-of-code-2021.day-10-syntax-scoring-bis
  (:require [clojure.string :as str]
            [advent-of-code-2021.day-10-syntax-scoring :as d10]))


(def dico-open
  {"(" ")"
   "[" "]"
   "{" "}"
   "<" ">"})

(defn complete?
  [queue x]
  (let [l (peek queue)]
    (= (dico-open l) x)))

(defn solve-line
  [line]
  (reduce
    (fn [queue c]
      (cond
        (complete? queue c) (pop queue)
        (dico-open c) (conj queue c)
        :else (reduced c)))
    []
    (map str line)))

(defn solve-1
  [input]
  (->>
    (d10/parse-input input)
    (map solve-line)
    (keep d10/scores-1)
    (reduce +)))

(defn solve-2
  [input]
  (d10/find-middle
    (into []
          (comp
            (map solve-line)
            (filter vector?)
            (map reverse)
            (map #(map d10/dico %))
            (map #(reduce d10/scores-2 0 %)))
          (d10/parse-input input))))

(comment
  (solve-1 example)
  (solve-2 example)
  (solve-2 d10/input)
  (assert (= 358737 (solve-1 d10/input)))
  (assert (= 4329504793 (solve-2 d10/input)))

  (solve-line "[({(<(())[]>[[{[]{<()<>>")
  (solve-line "{([(<{}[<>[]}>{[]{[(<()>")
  (peek [1 2 3])
  (pop [1 2 3])
  (conj [1 2] 3)
  )