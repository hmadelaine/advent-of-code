(ns advent-of-code-2021.day-12-passage-pathing
  (:require [clojure.string :as str]
            [ubergraph.core :as uber]
            [ubergraph.alg :as alg]
            [loom.alg :as alg']
            [loom.graph :as loom]
            [utils.solve-utils :as utils]))


(def example "start-A\nstart-b\nA-c\nA-b\nb-d\nA-end\nb-end")
(def input "vn-DD\nqm-DD\nMV-xy\nend-xy\nKG-end\nend-kw\nqm-xy\nstart-vn\nMV-vn\nvn-ko\nlj-KG\nDD-xy\nlj-kh\nlj-MV\nko-MV\nkw-qm\nqm-MV\nlj-kw\nVH-lj\nko-qm\nko-start\nMV-start\nDD-ko")


(defn small-cave?
  [cave]
  (and
    (not= "start" cave)
    (re-matches #"[a-z]+" cave)))

(defn big-cave?
  [cave]
  (re-matches #"[A-Z]+" cave))

(defn navigable
  [raw-graph]
  (mapcat
    (fn [edge]
      (let [[from to] edge]
        (cond
          (or (= "start" from)
              (= "end" to)) [edge]
          :else [[from to]
                 [to from]])))
    raw-graph))


(defn parse-raw-graph
  [input]
  (mapv #(str/split % #"-") (str/split-lines input)))

(defn parse-graph
  [input]
  (->> input
       (parse-raw-graph)
       (navigable)
       (reduce
         (fn [graph [from to]]
           (update graph from (fnil conj #{}) to))
         {})
       #_(reduce-kv
           (fn [graph from to]
             (if (and (small-cave? from)
                      (= 1 (count to))
                      (small-cave? (first to)))
               (dissoc graph from)
               (assoc graph from to)))
           {})))

(defn explore
  [graph node path]
  (if (= node "end")
    path
    (when-let [nodes (seq (clojure.set/difference (graph node) (set (filter small-cave? (set path)))))]
      (map #(explore graph % (conj path %)) nodes))))


(defn visit-small-caves-at-most-once
  [path]
  )


(defn a-single-small-cave-can-be-visited-at-most-twice
  "Return the possibles next caves
   parmi les les caves possibles on garde :
   - Toutes les big-caves
   - les small-caves qui
    y a t'il une small caves déjà visitées une fois.
    il faut retirer celle qui a déjà été visitée deux fois "
  [graph node path]
  (let [next-possible-nodes (graph node)
        next-big-caves (into #{} (filter big-cave?) next-possible-nodes)
        next-small-possible-nodes (filter small-cave? next-possible-nodes)
        num-times-small-cave-visited (frequencies (filter small-cave? path))
        small-cave-already-visited-twice? (some #{2} (vals num-times-small-cave-visited))]
    (reduce
      (fn [acc candidate]
        (let [already-visited? (num-times-small-cave-visited candidate)]
          (if (or (and already-visited?
                  (not small-cave-already-visited-twice?))
                (not already-visited?))
            (conj acc candidate)
            acc)))
      #{}
      (into next-big-caves  next-small-possible-nodes))))


(comment
  (a-single-small-cave-can-be-visited-at-most-twice
    {"lj"    #{"kh" "VH" "kw" "MV" "KG"},
     "kh"    #{"lj"},
     "VH"    #{"lj"},
     "vn"    #{"DD" "MV" "ko"},
     "DD"    #{"vn" "xy" "qm" "ko"},
     "xy"    #{"DD" "qm" "MV" "end"},
     "start" #{"vn" "MV" "ko"},
     "kw"    #{"lj" "qm" "end"},
     "qm"    #{"DD" "xy" "kw" "MV" "ko"},
     "MV"    #{"lj" "vn" "xy" "start" "qm" "ko"},
     "KG"    #{"lj" "end"},
     "ko"    #{"vn" "DD" "start" "qm" "MV"},
     "end"   #{"xy" "kw"}}
    "MV" ["start" "ko" "DD" "ko" "DD" "qm" "MV"])
  )

(defn explore-2
  "don't visit small caves more than once"
  [graph node path]
  (if (= node "end")
    path
    (when-let [nodes (set (a-single-small-cave-can-be-visited-at-most-twice graph node path))]
      (map #(explore-2 graph % (conj path %)) nodes))))


(defn solve-part-1
  [graph]
  (let [start "start"
        nodes (graph start)
        path [start]]
    (map #(explore graph % (conj path %)) nodes)))

(defn solve-part-2
  [graph]
  (let [start "start"
        nodes (graph start)
        path [start]]
    (map #(explore-2 graph % (conj path %)) nodes)))


(into {} (map (fn [[from tos]]
                [(keyword from) (into {} (map #(hash-map (keyword %) 1) tos))])
              (parse-graph example)))
(defn cnt
  [answer]
  (->> answer
       (clojure.walk/postwalk #(if (vector? %)
                                 {:solve %}
                                 %))
       (flatten)
       (keep :solve)
       (into #{})
       (count)))


(comment


  (cnt (solve-part-2 (parse-graph example)))
  (cnt (solve-part-2 (parse-graph input)))

  (parse-graph input)
  (alg/shortest-path (uber/graph (parse-graph input)) "start" "end")
  (solve-part-1 (parse-graph example))
  (assert (= 10 (cnt (solve-part-1 (parse-graph example)))))
  (assert (= 19 (cnt (solve-part-1 (parse-graph "dc-end\nHN-start\nstart-kj\ndc-start\ndc-HN\nLN-dc\nHN-end\nkj-sa\nkj-HN\nkj-dc")))))
  (assert (= 226 (cnt (solve-part-1 (parse-graph "fs-end\nhe-DX\nfs-he\nstart-DX\npj-DX\nend-zg\nzg-sl\nzg-pj\npj-he\nRW-he\nfs-DX\npj-RW\nzg-RW\nstart-pj\nhe-WI\nzg-he\npj-fs\nstart-RW")))))
  (uber/viz-graph (uber/graph (parse-graph input)))

  (assert (= 3292 (cnt (solve-part-1 (parse-graph input)))))

  )


