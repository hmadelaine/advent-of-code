(ns advent-of-code-2021.day-17-trick-shot
  "The probe's x,y position starts at 0,0.
   Then, it will follow some trajectory by moving in steps.
   On each step, these changes occur in the following order:

    -The probe's x position increases by its x velocity.
    -The probe's y position increases by its y velocity.
    -Due to drag, the probe's x velocity changes by 1 toward the value 0; that is, it decreases by 1 if it is greater than 0, increases by 1 if it is less than 0, or does not change if it is already 0.
    -Due to gravity, the probe's y velocity decreases by 1.
    -For the probe to successfully make it into the trench, the probe must be on some trajectory that causes it to be within a target area after any step. The submarine computer has already calculated this target area (your puzzle input).
    For example:
"
  (:require [cartesian.utils :as cart]
            [clojure.java.math :as math]))


(def input "target area: x=179..201, y=-109..-63")
(def example "target area: x=20..30, y=-10..-5")

(defn parse-input
  [input]
  (let [[_ min-x max-x max-y min-y] (re-matches #"target area: x=(\d+)\.\.(\d+), y=-(\d+)..-(\d+)" input)
        [min-x max-x max-y min-y] (map parse-long [min-x max-x max-y min-y])]
    (into {} (for [x (range min-x (inc max-x))
                   y (range min-y (inc max-y))]
               [[x (- y)] \T]))))



(comment
  (cart/sparse-draw (parse-input example))
  )


(defn next-velocity
  [[vx vy]]
  (let [vx' (cond
              (pos? vx) (dec vx)
              (neg? vx) (inc vx)
              (zero? vx) 0)
        vy' (dec vy)]
    [vx' vy']))


(defn step
  [[position velocity]]
  (let [[x y] position
        [vx vy] velocity]
    [[(+ x vx) (+ y vy)]
     (next-velocity [vx vy])]))

(defn shoot
  [targets velocity]
  (let [{:keys [max-x min-y]} (cart/dimensions targets)]
    (take 36 (take-while
               (fn [states]
                 (let [[location] states
                       [x y] location
                       not-found (not (targets location))]
                   (prn (and
                          not-found
                          (and (and (and
                                      (not (zero? x))
                                      (<= x max-x)))
                               (<= y min-y))))
                   (and
                     not-found
                     (or (and (and
                                (not (zero? x))
                                (<= x max-x)))
                         (<= y min-y)))))
               (drop 1 (iterate step [[0 0] velocity]))))))


(defn shoot'
  [targets velocity]
  (let [{:keys [max-x min-y]} (cart/dimensions targets)
        initial [0 0]]
    (loop [position initial
           velocity velocity
           path [initial]]
      (let [[position' velocity'] (step [position velocity])
            [x y] position
            [x' y'] position'
            path' (conj path position')]
        (cond
          (targets position') [:success path']
          (< max-x x') [:overshoot-x path']
          (< y' min-y) [:overshoot-y path']
          :else (recur position' velocity' path'))))))


(defn viz
  [input velocity]
  (let [targets (parse-input input)
        [result path] (shoot' targets velocity)
        max-y (apply max (map second path))
        trajectory (->> path
                        (map (fn [location]
                               [location \#]))
                        (into {}))]
    #_(println (cart/sparse-draw (merge targets trajectory)))
    (prn result max-y)))

(defn find-target
  [targets velocity]
  (loop [velocity velocity]
    (let [[result path] (shoot' targets velocity)
          [vx vy] velocity
          [x y] (last path)]
      (condp = result
        :success velocity
        :overshoot-x (recur [(dec vx) vy])
        :overshoot-y (recur [(inc vx) vy])))))

(defn maximize-y
  [targets velocity max-y]
  (let [[vx vy] velocity]
    (loop [velocity [vx (inc vy)]
           max-y max-y]
      (let [[vx vy] velocity
            [result path] (shoot' targets velocity)
            max-y' (apply max (map second path))]
        (cond
          (and (= :success result) (< max-y max-y')) (recur [vx (inc vy)] max-y')
          (and (= :success result) (< max-y' max-y)) velocity)))))

(defn solve-2
  []
  (let [targets (parse-input input)]
   (count (for [x (range -200 300)
                y (range -200 400)
                :let [[result _] (shoot' targets [x y])]
                :when (= :success result)]
            [x y]))))

(comment


  (let [targets (parse-input input)
        [result path] (shoot' targets [0 0])]
    [result path])



  (let [targets (parse-input input)
        velocity-x (find-target targets [0 0])
        _ (prn velocity-x)
        [result path] (shoot' targets velocity-x)
        max-y (apply max (map second path))]
    (maximize-y targets velocity-x max-y))
  )

(defn solve-1
  [input velocity]
  (let [targets (parse-input input)
        [result path] (shoot' targets velocity)
        max-y (apply max (map second path))]
    ))


(comment

  (viz input [19 108])

  (cart/sparse-draw (parse-input input))
  (shoot (parse-input example) [7 2])

  (shoot (parse-input example) [7 2])

  (shoot (parse-input example) [17 -4])
  (viz example [7 2])
  (viz example [6, 3])
  (println (viz example [6, 9]))
  )


