(ns advent-of-code-2021.day-24-arithmetic-logic-unit
  (:require [clojure.string :as str]
            [clojure.java.math :as math]
            [clojure.core.reducers :as r]))

(def input "inp w\nmul x 0\nadd x z\nmod x 26\ndiv z 1\nadd x 11\neql x w\neql x 0\nmul y 0\nadd y 25\nmul y x\nadd y 1\nmul z y\nmul y 0\nadd y w\nadd y 6\nmul y x\nadd z y\ninp w\nmul x 0\nadd x z\nmod x 26\ndiv z 1\nadd x 11\neql x w\neql x 0\nmul y 0\nadd y 25\nmul y x\nadd y 1\nmul z y\nmul y 0\nadd y w\nadd y 12\nmul y x\nadd z y\ninp w\nmul x 0\nadd x z\nmod x 26\ndiv z 1\nadd x 15\neql x w\neql x 0\nmul y 0\nadd y 25\nmul y x\nadd y 1\nmul z y\nmul y 0\nadd y w\nadd y 8\nmul y x\nadd z y\ninp w\nmul x 0\nadd x z\nmod x 26\ndiv z 26\nadd x -11\neql x w\neql x 0\nmul y 0\nadd y 25\nmul y x\nadd y 1\nmul z y\nmul y 0\nadd y w\nadd y 7\nmul y x\nadd z y\ninp w\nmul x 0\nadd x z\nmod x 26\ndiv z 1\nadd x 15\neql x w\neql x 0\nmul y 0\nadd y 25\nmul y x\nadd y 1\nmul z y\nmul y 0\nadd y w\nadd y 7\nmul y x\nadd z y\ninp w\nmul x 0\nadd x z\nmod x 26\ndiv z 1\nadd x 15\neql x w\neql x 0\nmul y 0\nadd y 25\nmul y x\nadd y 1\nmul z y\nmul y 0\nadd y w\nadd y 12\nmul y x\nadd z y\ninp w\nmul x 0\nadd x z\nmod x 26\ndiv z 1\nadd x 14\neql x w\neql x 0\nmul y 0\nadd y 25\nmul y x\nadd y 1\nmul z y\nmul y 0\nadd y w\nadd y 2\nmul y x\nadd z y\ninp w\nmul x 0\nadd x z\nmod x 26\ndiv z 26\nadd x -7\neql x w\neql x 0\nmul y 0\nadd y 25\nmul y x\nadd y 1\nmul z y\nmul y 0\nadd y w\nadd y 15\nmul y x\nadd z y\ninp w\nmul x 0\nadd x z\nmod x 26\ndiv z 1\nadd x 12\neql x w\neql x 0\nmul y 0\nadd y 25\nmul y x\nadd y 1\nmul z y\nmul y 0\nadd y w\nadd y 4\nmul y x\nadd z y\ninp w\nmul x 0\nadd x z\nmod x 26\ndiv z 26\nadd x -6\neql x w\neql x 0\nmul y 0\nadd y 25\nmul y x\nadd y 1\nmul z y\nmul y 0\nadd y w\nadd y 5\nmul y x\nadd z y\ninp w\nmul x 0\nadd x z\nmod x 26\ndiv z 26\nadd x -10\neql x w\neql x 0\nmul y 0\nadd y 25\nmul y x\nadd y 1\nmul z y\nmul y 0\nadd y w\nadd y 12\nmul y x\nadd z y\ninp w\nmul x 0\nadd x z\nmod x 26\ndiv z 26\nadd x -15\neql x w\neql x 0\nmul y 0\nadd y 25\nmul y x\nadd y 1\nmul z y\nmul y 0\nadd y w\nadd y 11\nmul y x\nadd z y\ninp w\nmul x 0\nadd x z\nmod x 26\ndiv z 26\nadd x -9\neql x w\neql x 0\nmul y 0\nadd y 25\nmul y x\nadd y 1\nmul z y\nmul y 0\nadd y w\nadd y 13\nmul y x\nadd z y\ninp w\nmul x 0\nadd x z\nmod x 26\ndiv z 26\nadd x 0\neql x w\neql x 0\nmul y 0\nadd y 25\nmul y x\nadd y 1\nmul z y\nmul y 0\nadd y w\nadd y 7\nmul y x\nadd z y")

(defn div [a b]
  (int (math/floor (/ a b))))

(defn b-value
  [s b]
  (if (keyword? b)
    (s b 0)
    b))

(defn times
  [a b]
  (if a
    (* a b)
    b))

(defn safe-mod
  [a b]
  (let [mod-nil (fnil mod 1)]
    (if (some neg? [a b])
      (do
        (prn "Négatif mod")
        a)
      (mod-nil a b)

      )))

(def ops {"inp" (fn [s v n] (assoc s v n))
          "add" (fn [s l r] (update s l (fnil + 0) (b-value s r)))
          "mul" (fn [s l r] (update s l times (b-value s r)))
          "div" (fn [s l r] (update s l (fnil div 0) (b-value s r)))
          "mod" (fn [s l r] (update s l mod (b-value s r)))
          "eql" (fn [s l r] (let [v (if (== (s l) (b-value s r)) 1 0)]
                              (assoc s l v)))})

(defn read-b
  [s]
  (when s
    (if (#{"w" "x" "y" "z"} s)
      (keyword s)
      (doto (parse-long s)
        (prn )))))

(defn parse-op
  [s]
  (let [[o a b] (str/split s #" ")]
    (cond-> [(keyword o) (ops o) (keyword a)]
            b (conj (read-b b)))))


(defn parse-ops
  [xs]
  (mapv parse-op (str/split-lines xs)))

(defn num->digits
  [number]
  (mapv (comp parse-long str) (str number)))

(defn compute
  ([state ops number]
   (loop [ops ops
          number (num->digits number)
          state state]
     (let [[op & ops] ops]
       (if op
         (let [[ins f a b] op]
           (if (= :inp ins)
             (recur ops (rest number) (f state a (first number)))
             (recur ops number (f state a b))))
         state))))
  ([ops number]
   (compute {:z 0, :x 0, :y 0, :w 0} ops number)))

(defn next-monad
  [monad]
  (loop [monad (dec monad)]
    (if (some zero? (num->digits monad))
      (recur (dec monad))
      monad)))

(defn explore
  [ops start]
  )

(defn solve-1
  [input]
  (let [ops (parse-ops input)]
    (loop [monad 99999987985956]
      (if (zero? (:z (compute ops monad)))
        monad
        (recur (next-monad monad))))))

(defn solve-1-par
  [input]
  (let [ops (parse-ops input)]
    (r/fold + (r/filter #(zero? (:z %)) (r/map #(compute {:z 0, :x 0, :y 0, :w 0} ops %) (iterate next-monad 90000000000000))))
    #_(loop [monad 99999987985956]
        (if (zero? (:z (compute ops monad)))
          monad
          (recur (next-monad monad))))))


(comment

  (let [variable "y"]
    (filter #(re-seq (re-pattern (str "[a-z]{3} " variable " .")) %) (str/split-lines input)))

  (solve-1 input)
  (solve-1-par input)

  (take 19 (iterate next-monad 99999987985956))
  (compute "inp w\nadd z w\nmod z 2\ndiv w 2\nadd y w\nmod y 2\ndiv w 2\nadd x w\nmod x 2\ndiv w 2\nmod w 2"
           1)

  (mapv (comp parse-long str) (str 1024))
  (reduce
    (fn [s [ins op a b]]
      (op s))
    1
    (parse-ops "inp x\nmul x -1")

    )

  (mod 2 26)

  (compute (parse-ops "inp w\nmul x 0\nadd x z\nmod x 26\ndiv z 1\nadd x 11\neql x w\neql x 0\nmul y 0\nadd y 25\nmul y x\nadd y 1\nmul z y\nmul y 0\nadd y w\nadd y 6\nmul y x\nadd z y") 7)
  (let [ops (parse-ops "inp w\nmul x 0\nadd x z\nmod x 26\ndiv z 26\nadd x 0\neql x w\neql x 0\nmul y 0\nadd y 25\nmul y x\nadd y 1\nmul z y\nmul y 0\nadd y w\nadd y 7\nmul y x\nadd z y")]
    (map #(compute ops %) (range 1 10)))

  (let [[f & r :as all-ops] (mapv #(into [[:inp (fn [s v n] (assoc s v n)) :w]] %)
                      (mapv parse-ops (drop 1 (str/split input #"inp w\n"))))]
    (map #(compute {:z 26, :x 0, :y 0, :w 0} (nth all-ops 13) %) (range 1 10))
    #_(reduce
      (fn [acc op]
        (compute acc op 10))
      {:z 1}
      r))

  (map #(compute {:z 0, :x 0, :y 0, :w 0} (parse-ops "inp w\nmul x 0\nadd x z\nmod x 26\ndiv z 1\nadd x 11\neql x w\neql x 0")
            %
            ) (range 1 10))
  )
(compute {:z 0, :x 0, :y 0, :w 0} (parse-ops "inp w\neql x w\neql x 0")
         9)
(compute {:z 0, :x 0, :y 0, :w 0}
         (parse-ops "add x z\nmod x 26\ndiv z 1\nadd x 11\neql x w\neql x 0\nmul y 0\nadd y 25\nmul y x\nadd y 1\nmul z y\nmul y 0\nadd y w\nadd y 6\nmul y x\nadd z y")
         1)

(compute {:z 6, :x 1, :y 6, :w 0}
         (parse-ops "inp w\nmul x 0\nadd x z\nmod x 26\ndiv z 1\nadd x 11\neql x w\neql x 0\nmul y 0\nadd y 25\nmul y x\nadd y 1\nmul z y\nmul y 0\nadd y w\nadd y 12\nmul y x\nadd z y")
         1)

(parse-ops input)
(cursive/diff "inp w\nmul x 0\nadd x z\nmod x 26\ndiv z 1\nadd x 11\neql x w\neql x 0\nmul y 0\nadd y 25\nmul y x\nadd y 1\nmul z y\nmul y 0\nadd y w\nadd y 12\nmul y x\nadd z y"
              "inp w\nmul x 0\nadd x z\nmod x 26\ndiv z 1\nadd x 11\neql x w\neql x 0\nmul y 0\nadd y 25\nmul y x\nadd y 1\nmul z y\nmul y 0\nadd y w\nadd y 12\nmul y x\nadd z y")