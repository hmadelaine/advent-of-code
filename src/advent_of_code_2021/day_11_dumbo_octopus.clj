(ns advent-of-code-2021.day-11-dumbo-octopus
  "The energy level of each octopus is a value between 0 and 9.
   Here, the top-left octopus has an energy level of 5, the bottom-right one has an energy level of 6, and so on.

   You can model the energy levels and flashes of light in steps.

   During a single step, the following occurs:
   - First, the energy level of each octopus increases by 1.
   - Then, any octopus with an energy level greater than 9 flashes.
   - This increases the energy level of all adjacent octopuses by 1,
     including octopuses that are diagonally adjacent.
   - If this causes an octopus to have an energy level greater than 9, it also flashes.

   This process continues as long as new octopuses keep having their energy level increased beyond 9.
   An octopus can only flash at most once per step.
   Finally, any octopus that flashed during this step has its energy level set to 0, as it used all of its energy to flash.
   Adjacent flashes can cause an octopus to flash on a step even if it begins that step with very little energy."
  (:require [cartesian.utils :as cart]
            [com.rpl.specter :as sp]
            [clojure.data]))



(def example "5483143223\n2745854711\n5264556173\n6141336146\n6357385478\n4167524645\n2176841721\n6882881134\n4846848554\n5283751526")
(def input "8448854321\n4447645251\n6542573645\n4725275268\n6442514153\n4515734868\n5513676158\n3257376185\n2172424467\n6775163586")

(defn parse-cavern
  [input]
  (cart/parse->coords (comp parse-long str) input))


(defn raises-all-octopuses-energy
  [cavern]
  (sp/transform [sp/MAP-VALS] inc cavern))


(defn pad-2
  [n]
  (format " %02d" n))

(defn draw
  [cavern]
  (println (cart/coords->raw pad-2 cavern))
  (println "--------------------"))

(defn find-flashable-octopuses
  [cavern]
  (keep
    (fn [[coord level]]
      (when (<= 10 level) coord))
    cavern))

(defn zero-energy-flashed-octopuses
  [flashed-octopuses cavern]
  (reduce (fn [cavern coord] (assoc cavern coord 0)) cavern flashed-octopuses))

(defn adjacent-octopuses
  "All non flashed octopuses that are diagonally adjacent"
  [octopus-coord cavern]
  (filter
    #(pos? (cavern %))
    (cart/all-adjacents cavern octopus-coord)))

(defn flashes
  "Find all flashable octopuses.
  Set their energy level to 0.
  Find all ajacent octopuses
  and increase their energy level by how many times thet benefit from the flashes"
  [cavern]
  (let [flashable (find-flashable-octopuses cavern)
        cavern (zero-energy-flashed-octopuses flashable cavern)
        increases (frequencies (mapcat #(adjacent-octopuses % cavern) flashable))]
    (reduce (fn [cavern [coord up]] (update cavern coord + up)) cavern increases)))

(defn step
  "During a single step, the following occurs:
   - First, the energy level of each octopus increases by 1.
   - Then, any octopus with an energy level greater than 9 flashes.
   - This increases the energy level of all adjacent octopuses by 1,
     including octopuses that are diagonally adjacent.
   - If this causes an octopus to have an energy level greater than 9, it also flashes.
   This process continues as long as new octopuses keep having their energy level increased beyond 9."
  [cavern]
  (let [cavern (raises-all-octopuses-energy cavern)]
    (loop [cavern cavern]
      (let [next-cavern (flashes cavern)]
        (if (= cavern next-cavern)
          next-cavern
          (recur (flashes cavern)))))))

(defn solve-1
  [input]
  (->> input
       (parse-cavern)
       (iterate step)
       (drop 1)
       (take 100)
       (map #(count (sp/select [sp/MAP-VALS zero?] %)))
       (reduce +)))

(defn solve-2
  [input]
  (let [cavern (parse-cavern input)]
    (loop [cavern cavern
           index 0]
      (if (every? zero? (vals cavern))
        index
        (recur (step cavern) (inc index))))))

(comment
  (solve-2 example)
  (assert (= 1546 (solve-1 input)))
  (assert (= 471 (solve-2 input)))

  )