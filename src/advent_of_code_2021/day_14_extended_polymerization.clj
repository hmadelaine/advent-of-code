(ns advent-of-code-2021.day-14-extended-polymerization
  (:require [clojure.string :as str]
            [utils.solve-utils :as utils]))

(def example "NNCB")
(def example-rules "CH -> B\nHH -> N\nCB -> H\nNH -> C\nHB -> C\nHC -> B\nHN -> C\nNN -> C\nBH -> H\nNC -> B\nNB -> B\nBN -> B\nBB -> N\nBC -> B\nCC -> N\nCN -> C")

(def input "OFSNKKHCBSNKBKFFCVNB")
(def input-rules "KC -> F\nCO -> S\nFH -> K\nVP -> P\nKF -> S\nSV -> O\nCB -> H\nPN -> F\nNC -> N\nBC -> F\nNP -> O\nSK -> F\nHS -> C\nSN -> V\nOP -> F\nON -> N\nFK -> N\nSH -> B\nHN -> N\nBO -> V\nVK -> H\nSC -> K\nKP -> O\nVO -> V\nHC -> P\nBK -> B\nVH -> N\nPV -> O\nHB -> H\nVS -> F\nKK -> B\nHH -> B\nCF -> F\nPH -> C\nNS -> V\nSO -> P\nNV -> K\nBP -> N\nSF -> V\nSS -> K\nFP -> N\nPC -> S\nOH -> B\nCH -> H\nVV -> S\nVN -> O\nOB -> K\nPF -> H\nCS -> C\nPP -> O\nNF -> H\nSP -> P\nOS -> V\nBB -> P\nNO -> F\nVB -> V\nHK -> C\nNK -> O\nHP -> B\nHV -> V\nBF -> V\nKO -> F\nBV -> H\nKV -> B\nOF -> V\nNB -> F\nVF -> C\nPB -> B\nFF -> H\nCP -> C\nKH -> H\nNH -> P\nPS -> P\nPK -> P\nCC -> K\nBS -> V\nSB -> K\nOO -> B\nOK -> F\nBH -> B\nCV -> F\nFN -> V\nCN -> P\nKB -> B\nFO -> H\nPO -> S\nHO -> H\nCK -> B\nKN -> C\nFS -> K\nOC -> P\nFV -> N\nOV -> K\nBN -> H\nHF -> V\nVC -> S\nFB -> S\nNN -> P\nFC -> B\nKS -> N")

(defn parse-rules
  [raw-rules]
  (into {}
        (map
          (fn [raw-rule]
            (str/split raw-rule #" -> "))
          (str/split-lines raw-rules))))


(defn parse-polymer
  [input]
  (map str input))

(defn polymerization
  [rules]
  (fn [polymer]
    (reduce
      (fn [acc [l r]]
        (if-let [element (rules (str l r))]
          (into acc [l element])
          (conj acc l)))
      []
      (partition-all 2 1 polymer))))


(defn parse-rules-2
  [raw-rules]
  (reduce-kv
    (fn [acc k v]
      (let [[l r] k]
        (assoc acc k [(str l v) (str v r)])))
    {}
    (parse-rules raw-rules)))


(defn parse-polymer-2
  [input]
  (frequencies (map str/join (partition 2 1 (parse-polymer input)))))

(defn polymerization-2
  [rules]
  (fn [polymer]
    (reduce-kv
      (fn [acc k v]
        (let [new-gen (rules k)]
          (reduce
            (fn [acc k]
              (update acc k (fnil + 0) v))
            acc
            new-gen)))
      {}
      polymer)))

(defn solve-2
  "Reason in pair.
   Only count the first letter of a pair and add one for the last letter."
  [n input-rules-2 input-2]
  (let [rules (parse-rules-2 input-rules-2)
        polymer (parse-polymer-2 input-2)
        solver (polymerization-2 rules)
        result (first (take 1 (drop n (iterate solver polymer))))
        result' (reduce-kv
                (fn [acc pair cnt]
                  (let [f (first pair)]
                    (update acc f (fnil + 0) cnt)))
                {}
                result
                )]
    (inc (- (last (apply max-key second result'))
        (second (apply min-key second result'))))))

(comment

  (solve-2 10 example-rules example)
  (solve-2 40 example-rules example)

  (solve-2 10 input-rules input)
  (solve-2 40 input-rules input)


  )
