(ns advent-of-code-2018.test-day-2
  (:require [clojure.test :refer :all]
            [advent-of-code-2018.day-2 :refer :all]))

(deftest check-example
  (is (= 12 (checksum-part-1 ["abcdef"
                              "bababc"
                              "abbcde"
                              "abcccd"
                              "aabcdd"
                              "abcdee"
                              "ababab"]))))
(deftest check-same-characters
  (is (= [nil \b \c \d nil \f]
         (same-characters ["abcdef" "abcdff" "cbcdff"]))))

(deftest check-part-1
  (is (= 7163 (solve-part-1 raw-input))))


(deftest chack-part-2
  (is (= "ighfbyijnoumxjlxevacpwqtr" (solve-part-2 input))))
