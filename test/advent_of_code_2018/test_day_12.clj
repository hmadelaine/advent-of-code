(ns advent-of-code-2018.test-day-12
  (:require [clojure.test :refer :all]
            [advent-of-code-2018.day-12 :refer :all]))


(deftest check-part-1
  (is (= 325 (solve-part-1 raw-example))))
