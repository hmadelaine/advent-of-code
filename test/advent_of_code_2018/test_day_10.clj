(ns advent-of-code-2018.test-day-10
  (:require [clojure.test :refer :all]
            [advent-of-code-2018.day-10 :refer :all]))



(deftest check-part-1
  (is (= " ####   #####      ###  #####   #    #  #    #   ####   ######\n#    #  #    #      #   #    #  ##   #  #   #   #    #  #     \n#       #    #      #   #    #  ##   #  #  #    #       #     \n#       #    #      #   #    #  # #  #  # #     #       #     \n#       #####       #   #####   # #  #  ##      #       ##### \n#       #           #   #  #    #  # #  ##      #       #     \n#       #           #   #   #   #  # #  # #     #       #     \n#       #       #   #   #   #   #   ##  #  #    #       #     \n#    #  #       #   #   #    #  #   ##  #   #   #    #  #     \n ####   #        ###    #    #  #    #  #    #   ####   #     "
         (solve-part-1 raw-input))))

(deftest check-part-2
  (is (= 10345 (solve-part-2 raw-input))))