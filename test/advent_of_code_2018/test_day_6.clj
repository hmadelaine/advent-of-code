(ns advent-of-code-2018.test-day-6
  (:require [clojure.test :refer :all]
            [advent-of-code-2018.day-6 :refer :all]))

(deftest check-part-1
  (is (= 17 (peek (solve-part-1 raw-example)))))



