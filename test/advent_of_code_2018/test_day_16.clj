(ns advent-of-code-2018.test-day-16
  (:require [clojure.test :refer :all]
            [advent-of-code-2018.day-16 :refer :all]))


(deftest check-part-2
  (is (= 706 (solve-part-2 raw-input raw-test-program))))
