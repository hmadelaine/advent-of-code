(ns advent-of-code-2018.test-day-4
  (:require [clojure.test :refer :all]
            [advent-of-code-2018.day-4 :refer :all]))

(deftest check-part-1
  (is (= 119835 (solve-part-1 raw-input))))

(deftest check-part-2
  (is (= 12725 (solve-part-2 raw-input))))
