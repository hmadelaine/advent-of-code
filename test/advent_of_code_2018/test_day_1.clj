(ns advent-of-code-2018.test-day-1
  (:require [clojure.test :refer :all]
            [advent-of-code-2018.day-1 :refer :all]))


(deftest test-day-1-part-1
  (is (= 497 (solve-part-1 input))))


(deftest test-find-first-duplicate
  (testing "Examples of part 2 "
    (is (= 10 (find-first-duplicate-frequency [+3, +3, +4, -2, -4])))
    (is (= 5 (find-first-duplicate-frequency [-6, +3, +8, +5, -6])))
    (is (= 14 (find-first-duplicate-frequency [+7, +7, -2, -7, -4])))))

(deftest test-day-1-part-2
  (is (= 558 (solve-part-2 input))))
