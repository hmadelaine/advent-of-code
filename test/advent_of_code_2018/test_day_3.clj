(ns advent-of-code-2018.test-day-3
  (:require [clojure.test :refer :all]
            [advent-of-code-2018.day-3 :refer :all]))

(deftest check-part-1
  (is (= 118858 (solve-part-1 raw-input))))

(deftest check-part-2
  (is (= 1100 (solve-part-2 raw-input))))