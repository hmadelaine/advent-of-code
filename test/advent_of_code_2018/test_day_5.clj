(ns advent-of-code-2018.test-day-5
  (:require [clojure.test :refer :all]
            [advent-of-code-2018.day-5 :refer :all]))


(deftest check-examples
  (is (= "dabCBAcaDA" (apply str (collapse-all "dabAcCaCBAcCcaDA")))))

(deftest check-part-1
  (is (= 9386 (solve-part-1 raw-input))))

(deftest check-part-2
  (is (= 4876 (solve-part-2 raw-input))))
