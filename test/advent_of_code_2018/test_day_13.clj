(ns advent-of-code-2018.test-day-13
  (:require [clojure.test :refer :all]
            [advent-of-code-2018.day-13 :refer :all]))

(deftest check-part-1
  (is (= [[[7 3] 2]] (solve-part-1 raw-example)))
  (is (= [[[48 20] 2]] (solve-part-1 raw-input))))

(deftest check-part-2
  (is (= {:face :up, :location [6 4], :last-cross :right}
         (solve-part-2 raw-example-2)))
  (is (= {:face :right, :location [59 64], :last-cross :right}
         (solve-part-2 raw-input))))