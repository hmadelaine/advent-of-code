(ns advent-of-code-2018.test-day-18
  (:require [clojure.test :refer :all]
            [advent-of-code-2018.day-18 :refer :all]))


(deftest check-part-1
  (is (= 675100 (solve-part-1 raw-input))))
